package com.gmail.killer.mingming.mygdxtest.desktop;

import com.gmail.killer.mingming.mygdxtest.Platform;

public class PlatformDesktop implements Platform {
	
	private final static String SAVE_FILE_PATH = "Gaforce/MyTest/";

	@Override
	public void exitApplication() {
		Main.exit();
	}

	@Override
	public String getSaveFilePath() {
		return SAVE_FILE_PATH;
	}

}
