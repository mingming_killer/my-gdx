package com.gmail.killer.mingming.mygdxtest.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gmail.killer.mingming.mygdxtest.MyGame;

public class Main {
	
	private static LwjglApplication mMainInstance = null;
	
	public static void main (String[] args) {
		PlatformDesktop platform = new PlatformDesktop();
		MyGame.setPlatformAPI(platform);
		
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "gdx-2dtest";
		cfg.useGL20 = false;
		cfg.width = 800;
		cfg.height = 480;
		
		mMainInstance = new LwjglApplication(MyGame.getInstance(), cfg);
	}
	
	public static void exit() {
		if (null != mMainInstance) {
			mMainInstance.exit();
		}
	}
}
