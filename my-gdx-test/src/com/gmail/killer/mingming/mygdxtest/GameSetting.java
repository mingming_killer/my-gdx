package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.OrderedMap;
import com.gaforce.killer7.ui.SerializerFactory;

/**
 * 
 * <p>
 * Hold game setting value.
 * </p>
 * 
 * @author mingming
 *
 */
public class GameSetting implements Serializable {
	
	public final static String TAG = "CLF.GameSetting: ";
	
	// *********************************
	// serializable data
	// *********************************
	public final static String MUSIC = "music";
	public final static String MIX = "mix";
	public final static String GRID = "grid";
	public final static String LANG = "lang";
	
	
	private boolean mSwitchMusic;
	private boolean mSwitchMix;
	private boolean mSwitchGrid;
	
	private String mLang;
	
	// **********************************
	// **********************************
	// **********************************
	
	
	public GameSetting() {
		initializeToDefault();
	}
	
	private void initializeToDefault() {
		mSwitchMusic = true;
		mSwitchMix = true;
		mSwitchGrid = true;
		
		mLang = GameElementDef.LANG_EN;
	}
	
	public void switchMusic(boolean on) {
		mSwitchMusic = on;
	}
	
	public void switchMix(boolean on) {
		mSwitchMix = on;
	}
	
	public void switchGrid(boolean on) {
		mSwitchGrid = on;
	}
	
	public void changeLang(String lang) {
		mLang = lang;
	}
	
	public boolean getSwitchMusic() {
		return mSwitchMusic;
	}
	
	public boolean getSwitchMix() {
		return mSwitchMix;
	}
	
	public boolean getSwitchGrid() {
		return mSwitchGrid;
	}
	
	public String getLang() {
		return mLang;
	}
	
	@Override
	public void write(Json json) {
		if (null == json) {
			Utils.log(TAG + "Json object is null !");
			return;
		}
			
		try {
			// base type data use writeValue API will faster.
			//json.writeField(mCol, FIELD_INTEGER, COL, null);
			json.writeValue(MUSIC, mSwitchMusic, Boolean.class);
			json.writeValue(MIX, mSwitchMix, Boolean.class);
			json.writeValue(GRID, mSwitchGrid, Boolean.class);
			json.writeValue(LANG, mLang, String.class);
			
		} catch (Exception e) {
			Utils.log(TAG, e);
		}
	}

	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData) {
		if (null == json || null == jsonData) {
			return;
		}
		
		mSwitchMusic = SerializerFactory.getBoolean(jsonData, MUSIC, true);
		mSwitchMix = SerializerFactory.getBoolean(jsonData, MIX, true);
		mSwitchGrid = SerializerFactory.getBoolean(jsonData, GRID, true);
		mLang = SerializerFactory.getString(jsonData, LANG, GameElementDef.LANG_EN);
	}
	
}
