package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver.Resolution;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.gaforce.killer7.resolver.ScaleResolver;
import com.gaforce.killer7.resource.Cyclable;
import com.gaforce.killer7.resource.ResourceManager;
import com.gaforce.killer7.resource.StringValues;
import com.gaforce.killer7.resource.UITaskQueue;
import com.gaforce.killer7.ui.DialogFactory;
import com.gaforce.killer7.ui.Scene;
import com.gaforce.killer7.ui.ScreenMask;
import com.gaforce.killer7.ui.SerializerFactory;
import com.gaforce.killer7.utils.K7Utils;
import com.gmail.killer.mingming.mygdxtest.MyGame.AssetLoadListener;

/**
 * 
 * <p>
 * Hold the global resource.
 * Who want to access global resource should though it.
 * </p>
 * 
 * @author mingming
 *
 */
public class GlobalResource implements Cyclable, AssetErrorListener, 
	AssetLoadListener {
	
	public final static String TAG = "CLF.GlobalResource: ";
	public final static String NAME_SCREEN_MASK = "ScreenMask";
	
	private Stage mStage = null;
	private SpriteCache mSpriteCache = null;
	
	private Skin mDefaultSkin = null;
	private BitmapFont mSysFont = null;
	
	private AssetManager mAssetMgr = null;
	private ResolutionFileResolver mResolver = null;
	
	private StringValues mStringValues = null;
	private GameSetting mGameSetting = null;
	
	private ScreenMask mScreenMask = null;
	
	private UITaskQueue mUITask = null;
	
	private static GlobalResource mInstance = null;
	
	
	public static GlobalResource getInstance() {
		if (null == mInstance) {
			mInstance = new GlobalResource();
		}
		return mInstance;
	}
	
	private GlobalResource() {
		mUITask = new UITaskQueue();
		mSpriteCache = new SpriteCache(GameConfig.SPRITE_CACHE_SIZE, true);
	}
	
	public Stage getStage() {
		if (null == mStage) {
			mStage = new Stage(Gdx.graphics.getWidth(), 
					Gdx.graphics.getHeight(), true);
		}
		return mStage;
	}
	
	public SpriteCache getSpriteCache() {
		if (null == mSpriteCache) {
			mSpriteCache = new SpriteCache(GameConfig.SPRITE_CACHE_SIZE, true);
		}
		
		return mSpriteCache;
	}
	
	public Skin getSkin() {
		return mDefaultSkin;
	}
	
	public BitmapFont getSysFont() {
		return mSysFont;
	}
	
	public BitmapFont getFont() {
		return getFont(GameElementDef.FONT_DEF);
	}
	
	public BitmapFont getFont(String name) {
		if (null == mDefaultSkin) {
			return null;
		}
		
		try {
			return mDefaultSkin.getFont(name);
		} catch (Exception e) {
			Utils.log(TAG + "getFont: " + 
					"failed !!", e);
			return null;
		}
	}
	
	public ResolutionFileResolver getResolver() {
		return mResolver;
	}
	
	public GameSetting getGameSetting() {
		return mGameSetting;
	}
	
	public AssetManager getAssetMgr() {
		return mAssetMgr;
	}
	
	public StringValues getStringValues() {
		return mStringValues;
	}
	
	public String resolverPath(String path) {
		if (null != mResolver) {
			return mResolver.resolve(path).path();
		} else {
			return null;
		}
	}
	
	/**
	 * An safely wrapper of {@link AssetManager#get(String, Class)}.
	 * @param <T> AssetManager get parameter.
	 * @param assetMgr AssetManager object.
	 * @param fileName AssetManager get parameter.
	 * @param type AssetManager get parameter.
	 * @param autoLoad if set true, it will auto load when AssetManager don't 
	 * 	load it.
	 * @return success return resource, or null
	 */
	public <T> T getAssetSafely(String fileName, Class<T> type, 
			boolean autoLoad, boolean wait) {
		if (null == mAssetMgr) {
			return null;
		}
		
		try {
			// get directly first.
			return mAssetMgr.get(fileName, type);
			
		} catch (GdxRuntimeException runtimeException) {
			// the resource don't complete load in AssetManager.
			if (autoLoad) {
				// if auto load, complete load it.
				// note: don't call load() again, it will add references.
				//assetMgr.load(fileName, type);
				if (wait) {
					while(!mAssetMgr.update()) {;}
				} else {
					if (!mAssetMgr.update()) {
						return null;
					}
				}
				
				try {
					return mAssetMgr.get(fileName, type);
				} catch (Exception e) {
					Utils.log(TAG + "getAssetSafely: " + 
							"AssetManager get " + fileName + " failed", e);
					return null;
				}
				
			} else {
				return null;
			}
			
		} catch (Exception e) {	
			Utils.log(TAG + "getAssetSafely: " + 
					"AssetManager get " + fileName + " failed", e);
			return null;
		}
	}
	
	public ScreenMask getScreenMask() {
		return mScreenMask;
	}
	
	public ScreenMask screenMask(int x, int y, int w, int h, float alpha) {
		if (null == mScreenMask) {
			return null;
		}
		
		mScreenMask.x = x;
		mScreenMask.y = y;
		mScreenMask.width = w;
		mScreenMask.height = h;
		
		mScreenMask.setAlpha(alpha);
			
		return mScreenMask;
	}
	
	public void removeScreenMask() {
		if (null == mScreenMask) {
			return;
		}
		
		mScreenMask.remove();
	}
	
	public boolean saveGameSetting() {
		// not encrypt game setting file now.
		return SerializerFactory.writeSerializable(mGameSetting, 
				Gdx.files.external(Utils.resovlerSavePath(ResDef.GAME_SETTING_FILE)), 
				GameSetting.class);
	}
	
	public void changeLang(String lang) {
		if (null == lang) {
			return;
		}
		
		// change setting.
		if (null != mGameSetting) {
			if (mGameSetting.getLang().equals(lang)) {
				return;
			}
			mGameSetting.changeLang(lang);
		}
		
		// reload language strings.
		if (null != mStringValues) {
			mStringValues.destroy();
		}
		
		mStringValues = SerializerFactory.readSerializable(
				Gdx.files.internal(Utils.resovlerLangPath(lang, ResDef.LANG_UI)), 
				StringValues.class);
		if (null == mStringValues) {
			mStringValues = new StringValues();
		}
		
		// change scene.
		Utils.reloadLang();
	}
	
	public boolean checkAssetUpdate() {
		if (null != mAssetMgr) {
			return mAssetMgr.update();
		} else {
			return false;
		}
	}
	
	public <T> void loadAsset(String fileName, Class<T> type, 
			AssetLoaderParameters<T> parameter) {
		if (null != mAssetMgr) {
			try {
				if (!mAssetMgr.isLoaded(fileName, type)) {
					mAssetMgr.load(fileName, type, parameter);
				}
			} catch (Exception e) {
				Utils.log(TAG + "loadAsset: " + 
						"AssetManager load failed !!", e);
			}
		}
	}
	
	public <T> void unloadAsset(T asset) {
		if (null != mAssetMgr) {
			try {
				mAssetMgr.unload(mAssetMgr.getAssetFileName(asset));
			} catch (Exception e) {
				Utils.log(TAG + "unloadAsset: " + 
						"AssetManager unload failed !!", e);
			}
		}
	}
	
	//public void initialize(int w, int h) {
	public void preloadAsset() {		
		// create ResourceManager.
		ResourceManager resMgr = ResourceManager.getInstance();
		
		// load system default font (use libgdx internal bitmap font).
		mSysFont = new BitmapFont();
		
		// create AssetManager and configure it.
		// On yeah, the resolution is too many ~~ shut it >_<.
		/*Resolution[] resolutions = {
				new Resolution(240, 320, ResDef.RES_NORMAL), 
				new Resolution(320, 480, ResDef.RES_NORMAL), 
				new Resolution(480, 800, ResDef.RES_HD), 
				new Resolution(480, 854, ResDef.RES_HD), 
				new Resolution(540, 960, ResDef.RES_HD), 
				new Resolution(640, 960, ResDef.RES_HD), 
				new Resolution(600, 1024, ResDef.RES_XHD), 
				new Resolution(768, 1024, ResDef.RES_XHD), 
				new Resolution(800, 1280, ResDef.RES_XHD), 
		};*/
		
		// initialize SerializerFactory base element, 
		// in oder to serialize some no-UI object.
		SerializerFactory.initializeBaseElement();
		
		mResolver = SerializerFactory.readSerializable(
				Gdx.files.internal(ResDef.RESOLUTIONS), ResolutionFileResolver.class);
		if (null == mResolver) {
			Utils.log(TAG + "preloadAsset: " + 
					"read resolutions configure file error, use default setting.");
			
			Resolution[] resolutions = {
					new Resolution(480, 800, ResDef.RES_NORMAL),
			};
			mResolver = new ResolutionFileResolver(
					new InternalFileHandleResolver(), resolutions);
		}
		
		mAssetMgr = new AssetManager();
		mAssetMgr.setLoader(Texture.class, new TextureLoader(mResolver));
		mAssetMgr.setLoader(BitmapFont.class, new BitmapFontLoader(mResolver));
		mAssetMgr.setLoader(Skin.class, new SkinLoader(mResolver));
		mAssetMgr.setErrorListener(this);
		Texture.setAssetManager(mAssetMgr);
		
		// pre-load asset.
		mAssetMgr.load(ResDef.TEX_BK, Texture.class);
		mAssetMgr.load(ResDef.TEX_UI, Texture.class);
		
		// note: this no use resolved path.
		//String file = resovlerPath(ResDef.SKIN_DEF);
		SkinLoader.SkinParameter skinParam = new SkinLoader.SkinParameter(
				K7Utils.parseResCfg(ResDef.SKIN_DEF, ResDef.TEX_FORMAT));
		mAssetMgr.load(ResDef.SKIN_DEF, Skin.class, skinParam);
		
		// add cyclable to ResourceManager.
		// note: the add order determine load and release order 
		//resMgr.registerCyclable(ResourcePools.getInstance());
		resMgr.registerCyclable(getInstance());
		
		Utils.registerAssetLoadListener(this);
	}
	
	/*public void loadAssetDone(int w, int h) {
		initSkin();
		
		initSerializer();
		
		initScene(w, h);
		
		initOther();
		
		// set input receiver.
		Gdx.input.setInputProcessor(getStage());
	}*/
	
	@SuppressWarnings("unused")
	private void restoreUI(int w, int h) {
		initSkin();
		
		initSerializer();
		
		Scene.setDefaultSize(w, h);
		Scene.setDefaultSkin(mDefaultSkin);
	}
	
	private void initConfig() {
		// load game settings.
		// not encrypt game setting file now.
		mGameSetting = SerializerFactory.readSerializable(
				Gdx.files.external(Utils.resovlerSavePath(ResDef.GAME_SETTING_FILE)), 
				GameSetting.class);
		if (null == mGameSetting) {
			mGameSetting = new GameSetting();
			Utils.log(TAG + "initOther: " + 
					"read game setting file error, use default setting !");
		}
		
		
		// load current language strings.
		mStringValues = SerializerFactory.readSerializable(
				Gdx.files.internal(Utils.resovlerLangPath(mGameSetting.getLang(), ResDef.LANG_UI)), 
				StringValues.class);
		if (null == mStringValues) {
			mStringValues = new StringValues();
		}
		
		
		// load and resolver BitmapFont scale factor.
		//ScaleResolver scaleResolver = ScaleResolverFactory.serialize(
		//		Gdx.files.internal(ResDef.SCALE_FONT));
		ScaleResolver scaleResolver = SerializerFactory.readSerializable(
				Gdx.files.internal(ResDef.SCALE_FONT), ScaleResolver.class);
		if (null != scaleResolver) {
			float scale = scaleResolver.resolve();
			BitmapFont font = getFont();
			
			Utils.log(TAG + "initOther: " + 
					"font scale factor: " + scale);
			
			if (null != font) {
				// set font texture high quality for scale.
				font.getRegion().getTexture().setFilter(
						TextureFilter.Linear, TextureFilter.Linear);
				font.setScale(scale, scale);
			}
		}
	}
	
	private void initSkin() {
		// if you don't set the default font,
		// when you write 'xx' to create Label, it will occur error
		//if (null == LibgdxToolkit.defaultFont) {
			//LibgdxToolkit.defaultFont = new BitmapFont();
		//	LibgdxToolkit.defaultFont = ResourcePools.getInstance().getFont(
		//			ResourceParser.parsePath(ResourceDefined.FONT_DEF));
		//}
		
		/*if (null == mDefaultSkin) {
			try {
				//String file = ResourceParser.parsePath(ResDef.SKIN_DEF);
				//String cfgFile = ResourceParser.parseResCfg(file, ResDef.CFG_FORMAT);
				//mDefaultSkin = new Skin(Gdx.files.internal(cfgFile), Gdx.files.internal(file));
				mDefaultSkin = new Skin(
						mResolver.resolve(ResDef.SKIN_DEF), 
						mResolver.resolve(Utils.parseResCfg(
								ResDef.SKIN_DEF, ResDef.CFG_FORMAT)));
			} catch (Exception e) {
				e.printStackTrace();
				mDefaultSkin = null;
			}
		}*/
		
		if (null == mDefaultSkin) {
			mDefaultSkin = getAssetSafely(ResDef.SKIN_DEF, Skin.class, true, true);
		}
	}
	
	private void initScene(int w, int h) {
		// configure Scene.
		Scene.setDefaultSize(w, h);
		Scene.setDefaultSkin(mDefaultSkin);
		
		// and add to libgdx Stage order determine z-oder too. -_-||
		ResourceManager resMgr = ResourceManager.getInstance();
		
		// add background first.
		/*resMgr.registerCyclable((Scene) 
				SceneManager.getInstance().getState(StateDef.STATE_BACKGROUND_ID));
		resMgr.registerCyclable((Scene) 
				SceneManager.getInstance().getState(StateDef.STATE_MAIN_MENU_ID));
		resMgr.registerCyclable((Scene) 
				SceneManager.getInstance().getState(StateDef.STATE_OPTIONS_MENU_ID));
		resMgr.registerCyclable((Scene) 
				SceneManager.getInstance().getState(StateDef.STATE_SOLO_GAME_ID));*/
		
		resMgr.registerCyclable(Utils.getSceneMgr());
		
		// call ResourceManager create() to initialize scenes.
		resMgr.create();
		
		// add background to Stage first. (Note z-order !)
		Scene bk = Utils.getScene(SceneDef.SCENE_BACKGROUND_ID);
		if (null != bk) {
			bk.layout();
			bk.beginAct();
		}
		
		// run scene manager(state machine).
		Utils.runSceneMgr();
	}
		
	private void initSerializer() {		
		//SerializerFactory.initialize(mDefaultSkin, 
		//		GameElementDef.SKIN_STYLE_DEF, mAssetMgr, null);
		SerializerFactory.initializeUIElement(mDefaultSkin, 
				GameElementDef.SKIN_STYLE_DEF, mAssetMgr, mStringValues);
		
		// register custom Serializer.
		
	}
	
	private void initOther() {
		// initialize Dialog.
		DialogFactory.initialize(mDefaultSkin.getStyle(
				GameElementDef.SKIN_STYLE_DEF, WindowStyle.class));
		
		
		// initialize ScreenMask.
		Color color = new Color();
		Color.rgba8888ToColor(color, GameConfig.SCREEN_MASK_COLOR);
		mScreenMask = new ScreenMask(color, NAME_SCREEN_MASK);
		
		ResourceManager resMgr = ResourceManager.getInstance();
		resMgr.registerCyclable(mScreenMask);
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		if (null != mStage) {
			mStage.clear();
		}
	}

	@Override
	public void resume() {
		//restoreUI(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
	
	@Override
	public void destroy() {
		if (null != mStage) {
			mStage.dispose();
			mStage = null;
		}
		
		if (null != mSpriteCache) {
			mSpriteCache.dispose();
			mSpriteCache = null;
		}
		
		Scene.cleanDefaultSkin();
		
		DialogFactory.destroy();
		
		SerializerFactory.destroy();
		
		//if (null != mDefaultSkin) {
		//	mDefaultSkin.dispose();
		//	mDefaultSkin = null;
		//}
		mDefaultSkin = null;
		
		if (null != mAssetMgr) {
			mAssetMgr.dispose();
			mAssetMgr = null;
		}
		
		if (null != mSysFont) {
			mSysFont.dispose();
			mSysFont = null;
		}
		
		if (null != mStringValues) {
			mStringValues.destroy();
			mStringValues = null;
		}
		
		if (null != mUITask) {
			mUITask.destory();
			mUITask = null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void error(String fileName, Class type, Throwable throwable) {
		// TODO Auto-generated method stub
		Utils.log(TAG + "error: " +
				"couldn't load asset '" + fileName + "'", 
				(Exception)throwable);
	}

	@Override
	public void assetLoadDone() {
		initConfig();
		
		initSkin();
		
		initSerializer();
		
		initScene(Utils.getWidth(), Utils.getHeight());
		
		initOther();
		
		// set input receiver.
		Gdx.input.setInputProcessor(getStage());
		
		Utils.unregisterAssetLoadListener(this);
	}
	
	@Override
	public void waitLoadScreen(SpriteBatch batch) {
		Utils.defaultDrawLoadScreen(batch);
	}
	
}
