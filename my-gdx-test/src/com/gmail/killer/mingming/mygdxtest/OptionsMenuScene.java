package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.gaforce.killer7.statemachine.Event;
import com.gaforce.killer7.statemachine.SignalEvent;
import com.gaforce.killer7.statemachine.StateMachine;
import com.gaforce.killer7.statemachine.Transition;
import com.gaforce.killer7.ui.ImageButton;
import com.gaforce.killer7.ui.Scene;
import com.gaforce.killer7.ui.ToggleButton;

/**
 * 
 * <p>
 * Option menu.
 * </p>
 * 
 * @author mingming
 *
 */
public class OptionsMenuScene extends Scene 
	implements ClickListener {
	
	public final static String TAG = "CLF.OptionsMenuScene: ";
	
	private final static String NAME_TOGGLE_MUSIC = "OptToggleMusic";
	private final static String NAME_TOGGLE_MIX = "OptToggleMix";
	private final static String NAME_TOGGLE_GRID = "OptToggleGrid";
	private final static String NAME_BTN_BACK = "OptBtnBack";
	
	private ToggleButton mToggleMusic;
	private ToggleButton mToggleMix;
	private ToggleButton mToggleGrid;
	private ImageButton mBtnBack;
	
	
	public OptionsMenuScene(Stage stage, StateMachine stateMachine, final String name) {
		super(stage, stateMachine, 
				Gdx.files.internal(Utils.resolverPath(ResDef.UI_FILE_SCENE_OPTIONS_MENU)), 
				name, Utils.getMainGame());
	}
	
	@Override
	public void click(Actor actor, float x, float y) {
		if (actor.equals(mToggleMusic)) {
			onToggleMusicClick();
		} else if (actor.equals(mToggleMix)) {
			onToggleMixClick();
		} else if (actor.equals(mToggleGrid)) {
			onToggleGridClick();
		} else if (actor.equals(mBtnBack)) {
			onBtnBackClick();
		} else {
			// noting
		}
	}
	
	@Override
	protected void onEnter(Event event, Transition transition) {
		initElement();
		
		// before enter animation, we sync game setting show. 
		syncGameSetting();
		
		super.onEnter(event, transition);
	}
	
	@Override
    protected void onLeave(Event event, Transition transition) {
		// when leave options we write the game setting to file.
		Utils.saveGameSetting();
		
		super.onLeave(event, transition);
	}
	
	@Override
	protected void saveScene() {
		super.saveScene();
		
		Utils.saveGameSetting();
	}
	
	@Override
	protected void restoreScene() {
		super.restoreScene();
		
		initElement();
		
		syncGameSetting();
	}
	
	@Override
	protected void cleanScene() {
		super.cleanScene();
		
		mToggleMusic = null;
		mToggleMix = null;
		mToggleGrid = null;
		mBtnBack = null;
	}
	
	private void initElement() {
		mToggleMusic = (ToggleButton) mActors.get(NAME_TOGGLE_MUSIC);
		mToggleMix = (ToggleButton) mActors.get(NAME_TOGGLE_MIX);
		mToggleGrid = (ToggleButton) mActors.get(NAME_TOGGLE_GRID);
		mBtnBack = (ImageButton) mActors.get(NAME_BTN_BACK);
		
		if (null != mBtnBack) {
			mBtnBack.setClickListener(this);
		}
		
		if (null != mToggleMusic) {
			mToggleMusic.setClickListener(this);
		}
		
		if (null != mToggleMix) {
			mToggleMix.setClickListener(this);
		}
		
		if (null != mToggleGrid) {
			mToggleGrid.setClickListener(this);
		}
	}
	
	private void syncGameSetting() {
		if (null != mToggleMusic) {
			//mToggleMusic.toggle(Utils.getSwitchMusic());
			String lang = Utils.getLang();
			if (GameElementDef.LANG_EN.equals(lang)) {
				mToggleMusic.setToggle(true);
			} else {
				mToggleMusic.setToggle(false);
			}
		}
		
		if (null != mToggleMix) {
			mToggleMix.setToggle(Utils.getSwitchMix());
		}
		
		if (null != mToggleGrid) {
			mToggleGrid.setToggle(Utils.getSwitchGrid());
		}
	}
	
	private void onToggleMusicClick() {
		//Utils.log(TAG + "Music: " + mToggleMusic.isToggle());
		if (null != mToggleMusic) {
			//Utils.switchMusic(mToggleMusic.isToggle());
			
			String lang = GameElementDef.LANG_EN;
			if (mToggleMusic.isToggle()) {
				lang = GameElementDef.LANG_EN;
			} else {
				lang = GameElementDef.LANG_CN;
			}
			
			if (!Utils.getLang().equals(lang)) {
				//Utils.changeLang(lang);
				//Utils.reloadByChangeLanguage();
				Utils.changeLang(lang);
			}
		}
	}
	
	private void onToggleMixClick() {
		//Utils.log(TAG + "Mix: " + mToggleMix.isToggle());
		if (null != mToggleMix) {
			Utils.switchMix(mToggleMix.isToggle());
		}
	}
	
	private void onToggleGridClick() {
		//Utils.log(TAG + "Grid: " + mToggleGrid.isToggle());
		if (null != mToggleGrid) {
			Utils.switchGrid(mToggleGrid.isToggle());
		}
	}
	
	private void onBtnBackClick() {
		SignalEvent event = new SignalEvent(Utils.getSceneMgr(), 
				SceneDef.SIGNAL_OPTIONS_TO_MAIN.hashCode());
		Utils.sendEvent(event);
	}
	
}
