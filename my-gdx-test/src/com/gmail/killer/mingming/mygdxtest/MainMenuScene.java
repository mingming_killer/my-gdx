package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.gaforce.killer7.actions.ZoomTo;
import com.gaforce.killer7.interpolators.SystemInterpolator;
import com.gaforce.killer7.statemachine.Event;
import com.gaforce.killer7.statemachine.SignalEvent;
import com.gaforce.killer7.statemachine.StateMachine;
import com.gaforce.killer7.statemachine.Transition;
import com.gaforce.killer7.ui.Dialog;
import com.gaforce.killer7.ui.Dialog.DialogListener;
import com.gaforce.killer7.ui.DialogFactory;
import com.gaforce.killer7.ui.Scene;
import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * Game main menu.
 * </p>
 * 
 * @author mingming
 *
 */
public class MainMenuScene extends Scene 
	implements ClickListener, DialogListener {
	
	//private final static String NAME_LOGO = "MainImgLogo"; 
	private final static String NAME_BTN_SOLO = "MainBtnSolo";
	private final static String NAME_BTN_OPTIONS = "MainBtnOptions";
	private final static String NAME_BTN_EXIT = "MainBtnExit";
	
	private TextButton mBtnSolo = null;
	private TextButton mBtnOptions = null;
	private TextButton mBtnExit = null;
	
	private Dialog mDialog;
	
	
	public MainMenuScene (Stage stage, StateMachine stateMachine, final String name) {
		super(stage, stateMachine, 
				Gdx.files.internal(Utils.resolverPath(ResDef.UI_FILE_SCENE_MAIN_MENU)), 
				name, Utils.getMainGame());
	}
	
	@Override
	public void click(Actor actor, float x, float y) {
		if (actor.equals(mBtnSolo)) {
			onBtnSoloGameClick();
		} else if (actor.equals(mBtnOptions)) {
			onBtnOptionsClick();
		} else if (actor.equals(mBtnExit)) { 
			onBtnExitClick();
		} else {
			// noting
		}
	}
	
	@Override
	protected void onEnter(Event event, Transition transition) {
		initElement();
		
		super.onEnter(event, transition);
	}
	
	@Override
	protected void restoreScene() {
		super.restoreScene();
		
		initElement();
	}
	
	@Override
	protected void cleanScene() {
		super.cleanScene();
		
		mBtnSolo = null;
		mBtnOptions = null;
		mBtnExit = null;
	}
	
	@Override
	public void finishAct() {
		super.finishAct();
		
		if (null != mDialog) {
			Utils.removeScreenMask();
			mDialog.dismiss();
			mDialog = null;
		}
	}
	
	@Override
	public void reloadLang() {
		if (null != mBtnSolo) {
			mBtnSolo.setText(Utils.getString(StringDef.STR_MAIN_SOLO));
		}
		
		if (null != mBtnOptions) {
			mBtnOptions.setText(Utils.getString(StringDef.STR_MAIN_OPTIONS));
		}
		
		if (null != mBtnExit) {
			mBtnExit.setText(Utils.getString(StringDef.STR_MAIN_EXIT));
		}
	}
	
	private void initElement() {
		mBtnSolo = (TextButton) mActors.get(NAME_BTN_SOLO);
		mBtnOptions = (TextButton) mActors.get(NAME_BTN_OPTIONS);
		mBtnExit = (TextButton) mActors.get(NAME_BTN_EXIT);
		
		if (null != mBtnSolo) {
			mBtnSolo.setClickListener(this);
		}
		
		if (null != mBtnOptions) {
			mBtnOptions.setClickListener(this);
		}
		
		if (null != mBtnExit) {
			mBtnExit.setClickListener(this);
		}
	}
	
	private void onBtnSoloGameClick () {
		//SignalEvent event = new SignalEvent(Utils.getSceneMgr(), 
		//		SceneDef.SIGNAL_MAIN_TO_SOLO_READY.hashCode());
		//Utils.sendEvent(event);
	}
	
	private void onBtnOptionsClick () {
		SignalEvent event = new SignalEvent(Utils.getSceneMgr(), 
				SceneDef.SIGNAL_MAIN_TO_OPTIONS.hashCode());
		Utils.sendEvent(event);
	}
	
	private void onBtnExitClick () {
		//ColorLinesFireGame.getInstance().sendEmptyMessage(
		//		UserMsgDef.MSG_EXIT_APP);
		
		int width = (int) (Utils.getWidth() * GameConfig.DLG_WIDTH);
		int height = (int) (Utils.getHeight() * GameConfig.DLG_HEIGTH);
		int x = (Utils.getWidth() - width) / 2;
		int y = (Utils.getHeight() - height) / 2;
		
		int animationX = Utils.getWidth() / 2 - 1;
		int animationY = Utils.getHeight() / 2 + 1;
		int animationW = 1;
		int animationH = 1;
		
		AnimationAction action = ZoomTo.$(x, y, width, height, GameConfig.DLG_DURATION);
		action.setInterpolator(SystemInterpolator.$(SystemInterpolator.ElasticOut));
		
		mDialog = DialogFactory.dialog(
				"", Utils.getString(StringDef.STR_TIP_EXIT),  
				Utils.getString(StringDef.STR_YES),  
				Utils.getString(StringDef.STR_NO), 
				animationX, animationY, animationW, animationH, this, action);
		if (null != mDialog) {
			try {
				// add screen mask to stage.
				K7Utils.addActorSafely(Utils.getStage().getRoot(), 
						Utils.screenMask(0, 0, Utils.getWidth(), Utils.getHeight(), 
								GameConfig.SCREEN_MASK_ALPHA));
				
				// add dialog to stage.
				K7Utils.addActorSafely(Utils.getStage().getRoot(), 
						mDialog.getWindow());
			} catch (Exception e) {
				Utils.log("CFL.MainMenuScene: onBtnExitClick: ", e);
			}
		}
	}

	@Override
	public void onDlgSelected(int ret) {
		// remove screen mask.
		Utils.removeScreenMask();
		
		mDialog = null;
		
		// exit game.
		if (Dialog.ID_POSITIVE == ret) {
			Utils.sendEmptyMessage(UserMsgDef.MSG_EXIT_APP);
		}
	}
	
}
