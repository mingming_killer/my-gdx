package com.gmail.killer.mingming.mygdxtest;

/**
 * 
 * <p>
 * Resource defined.
 * </p>
 * 
 * @author mingming
 *
 */
public class ResDef {
	
	public final static String CFG_FORMAT = ".json";
	public final static String TEX_FORMAT = ".png";
	public final static String DATA_PREFIX = "data";
	
	// HD resource is default, and it folder is not exist.
	public final static String RES_HD = "default";
	public final static String RES_NORMAL = "normal";
	public final static String RES_XHD = "xhd";
	
	public final static String SKIN_DEF = "data/ui/skin_def.json";
	//public final static String FONT_DEF = "font/fzmiaowu";
	public final static String SCALE_FONT = "data/ui/scale_font.json";
	public final static String RESOLUTIONS = "data/ui/resolutions.json";
	
	public final static String LANG_PATH = "data/lang/";
	public final static String LANG_UI = "str_ui.json";
	
	// texture file.
	public final static String TEX_BK = "data/texture/bk.png";
	public final static String TEX_UI = "data/texture/ui.png";
	public final static String TEX_BALL = "data/texture/ball.png";
	public final static String TEX_ITEM = "data/texture/item.png";
	
	// scene ui file.
	public final static String UI_FILE_SCENE_MAIN_MENU = "data/ui/scene_main.json";
	public final static String UI_FILE_SCENE_OPTIONS_MENU = "data/ui/scene_options.json";
	public final static String UI_FILE_SCENE_SOLO_GAME = "data/ui/scene_solo_game.json";
	public final static String UI_FILE_SCENE_SOLO_READY = "data/ui/scene_solo_ready.json";
	public final static String UI_FILE_POPUP_MENU_SOLO_PAUSE = "data/ui/popmenu_solo_pause.json";
	
	// save file.
	public final static String GAME_SAVE_FILE = "game_save.json";
	public final static String AUTO_SAVE_FILE = "auto_save.json";
	
	public final static String GAME_SETTING_FILE = "game_setting.json";
	
}
