package com.gmail.killer.mingming.mygdxtest;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.gmail.killer.mingming.mygdxtest.MyGame;
import com.gmail.killer.mingming.mygdxtest.MyGame.AssetLoadListener;
import com.gaforce.killer7.message.Handler;
import com.gaforce.killer7.message.Message;
import com.gaforce.killer7.statemachine.Event;
import com.gaforce.killer7.statemachine.StateMachine;
import com.gaforce.killer7.ui.Scene;
import com.gaforce.killer7.ui.ScreenMask;
import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * Some useful smart tools.
 * </p>
 * 
 * @author mingming
 *
 */
public class Utils {
	
	public final static String TAG = "CLF.Utils: ";
	
	public final static void log(String message) {
		Gdx.app.log(K7Utils.TAG, message);
	}
	
	public final static void log(String message, Exception exception) {
		Gdx.app.log(K7Utils.TAG, message, exception);
	}
	
	public final static int getWidth() {
		try {
			return (int) GlobalResource.getInstance().getStage().width();
		} catch (Exception e) {
			log(TAG + "getWidth: ", e);
			return 0;
		}
	}
	
	public final static int getHeight() {
		try {
			return (int) GlobalResource.getInstance().getStage().height();
		} catch (Exception e) {
			log(TAG + "getHeight: ", e);
			return 0;
		}
	}
	
	private static int loadCount = 0;
	public static void defaultDrawLoadScreen(SpriteBatch batch) { 
		if (null == batch) {
			Utils.log(TAG + "drawDefaultLoadProgress: " + 
					"spriteBatch is null !");
			return;
		}
		
		loadCount++;
		if (loadCount >= 10) {
			loadCount = 0;
		}
		
		String show = "Load  ";
    	BitmapFont font = Utils.getSysFont();
    	float h = font.getBounds(show).height;
		
        batch.begin();
        {
        	if (null != font) {
        		for (int i = 0; i < loadCount; i++) {
        			show += ".";
        		}
        		font.draw(batch, show, 0, 2 + h);
        	}
        }
        batch.end();
	}
	
	public final static void drawBackground(SpriteBatch batch, float parentAlpha) {
		if (null == batch) {
			Utils.log(TAG + "drawBackground: " + 
					"spriteBatch is null !");
			return;
		}
		
		Stage stage = getStage();
		if (null == stage) {
			Utils.log(TAG + "drawBackground: " + 
					"can't get Stage!");
			return;
		}
		
		List<Actor> actors = stage.getActors();
		if (null == actors) {
			Utils.log(TAG + "drawBackground: " + 
					"can't get actors!");
			return;
		}
		
		Background bk = null;
		for (Actor actor : actors) {
			if (actor instanceof Background) {
				bk = (Background) actor;
				break;
			}
		}
		
		if (null == bk) {
			Utils.log(TAG + "drawBackground: " + 
					"can't get background!");
			return;
		}
		
		batch.begin();
		bk.draw(batch, parentAlpha);
		batch.end();
	}
	
	public final static String resovlerSavePath(String path) {
		if (null == path) {
			return null;
		}
		
		return (MyGame.getPlatformAPI().getSaveFilePath() + path);
	}
	
	public final static String resovlerLangPath(String lang, String element) {
		if (null == lang || null == element) {
			return null;
		}
		
		String path = ResDef.LANG_PATH;
		
		if (GameElementDef.LANG_CN.equals(lang) || 
				GameElementDef.LANG_EN.equals(lang)) {
			path += lang;
		} else {
			// default language is EN.
			path += GameElementDef.LANG_EN;
		}
		
		return path + "/" + element;
	}
	
	//***********************************************
	// encapsulation of GlobalHolder interfaces.
	//***********************************************
	
	public final static Stage getStage() {
		return GlobalResource.getInstance().getStage();
	}
	
	public final static SpriteCache getSpriteCache() {
		return GlobalResource.getInstance().getSpriteCache();
	}
	
	public final static Skin getSkin() {
		return GlobalResource.getInstance().getSkin();
	}
	
	public final static BitmapFont getSysFont() {
		return GlobalResource.getInstance().getSysFont();
	}
	
	public final static BitmapFont getFont() {
		return GlobalResource.getInstance().getFont();
	}
	
	public final static BitmapFont getFont(String name) {
		return GlobalResource.getInstance().getFont(name);
	}
	
	public final static ResolutionFileResolver getResolver() {
		return GlobalResource.getInstance().getResolver();
	}
	
	public final static AssetManager getAssetMgr() {
		return GlobalResource.getInstance().getAssetMgr();
	}
	
	public final static String resolverPath(String path) {
		return GlobalResource.getInstance().resolverPath(path);
	}
	
	public final static <T> T getAsset(String fileName, Class<T> type) {
		return GlobalResource.getInstance().getAssetSafely(
				fileName, type, true, true);
	}
	
	public final static void preloadAsset() {
		GlobalResource.getInstance().preloadAsset();
	}
	
	public final static <T> void loadAsset(String fileName, Class<T> type, 
			AssetLoaderParameters<T> parameter) {
		GlobalResource.getInstance().loadAsset(fileName, type, parameter);
	}
	
	public final static <T> void unloadAsset(T asset) {
		GlobalResource.getInstance().unloadAsset(asset);
	}
	
	public final static boolean checkAssetUpdate() {
		return GlobalResource.getInstance().checkAssetUpdate();
	}
	
	public final static ScreenMask screenMask(int x, int y, int w, int h, float alpha) {
		return GlobalResource.getInstance().screenMask(x, y, w, h, alpha);
	}
	
	public final static void removeScreenMask() {
		GlobalResource.getInstance().removeScreenMask();
	}
	
	public final static void changeLang(String lang) {
		GlobalResource.getInstance().changeLang(lang);
	}
	
	public final static String getString(String key) {
		return GlobalResource.getInstance().getStringValues().getString(key);
	}
	
	public final static String getString(String key, String def) {
		return GlobalResource.getInstance().getStringValues().getString(key, def);
	}
	
	public final static GameSetting getGameSetting() {
		return GlobalResource.getInstance().getGameSetting();
	}
	
	public final static boolean getSwitchMusic() {
		return GlobalResource.getInstance().getGameSetting().getSwitchMusic();
	}
	
	public final static boolean getSwitchMix() {
		return GlobalResource.getInstance().getGameSetting().getSwitchMix();
	}
	
	public final static boolean getSwitchGrid() {
		return GlobalResource.getInstance().getGameSetting().getSwitchGrid();
	}
	
	public final static String getLang() {
		return GlobalResource.getInstance().getGameSetting().getLang();
	}
	
	public final static void switchMusic(boolean on) {
		GlobalResource.getInstance().getGameSetting().switchMusic(on);
	}
	
	public final static void switchMix(boolean on) {
		GlobalResource.getInstance().getGameSetting().switchMix(on);
	}
	
	public final static void switchGrid(boolean on) {
		GlobalResource.getInstance().getGameSetting().switchGrid(on);
	}
	
	public final static void switchLang(String lang) {
		GlobalResource.getInstance().getGameSetting().changeLang(lang);
	}
	
	public final static boolean saveGameSetting() {
		return GlobalResource.getInstance().saveGameSetting();
	}
	
	//***********************************************
	//***********************************************
	//***********************************************
	
	
	//***********************************************
	// encapsulation of SceneManager interfaces.
	//***********************************************
	
	public final static SceneManager getSceneMgr() {
		return SceneManager.getInstance();
	}
	
	public final static void runSceneMgr() {
		SceneManager.getInstance().run();
	}
	
	public final static void postEvent(Event event) {
		SceneManager.getInstance().postEvent(event);
	}
	
	public final static void sendEvent(Event event) {
		SceneManager.getInstance().sendEvent(event);
	}
	
	public final static void adjustSceneSize(int width, int height) {
		SceneManager.getInstance().adjustSceneSize(width, height);
	}
	
	public final static void reloadLang() {
		SceneManager.getInstance().reloadLang();
	}
	
	public final static StateMachine getStateMachine() {
		return SceneManager.getInstance().getStateMachine();
	}
	
	public final static Scene getScene(final String name) {
		return SceneManager.getInstance().getScene(name);
	}
	
	public final static Scene getScene(final int id) {
		return SceneManager.getInstance().getScene(id);
	}
	
	//***********************************************
	//***********************************************
	//***********************************************
	
	
	//***********************************************
	// encapsulation of MyGame interfaces.
	//***********************************************
	
	public final static MyGame getMainGame() {
		return MyGame.getInstance();
	}
	
	public final static void registerUIHandler(Handler handler) {
		MyGame.getInstance().registerUIHandler(handler);
	}
	
	public final static void unRegisterUIHandler(Handler handler) {
		MyGame.getInstance().unregisterUIHandler(handler);
	}
	
	public final static void registerHandlerCallback(Handler.Callback callback) {
		MyGame.getInstance().registerHandlerCallback(callback);
	}
	
	public final static void unregisterHandlerCallback(Handler.Callback callback) {
		MyGame.getInstance().unregisterHandlerCallback(callback);
	}
	
	public final static void registerAssetLoadListener(AssetLoadListener listener) {
		MyGame.getInstance().registerAssetLoadListener(listener);
	}
	
	public final static void unregisterAssetLoadListener(AssetLoadListener listener) {
		MyGame.getInstance().unregisterAssetLoadListener(listener);
	}
	
	public final static boolean sendMessage(Message msg) {
		return MyGame.getInstance().sendMessage(msg);
	}
	
	public final static boolean sendEmptyMessage(int what) {
		return MyGame.getInstance().sendEmptyMessage(what);
	}
	
	public final static Platform getPlatformAPI() {
		return MyGame.getPlatformAPI();
	}
	
	public final static void setPlatformAPI (Platform platform) {
		MyGame.setPlatformAPI(platform);
	}
	
	//***********************************************
	//***********************************************
	//***********************************************
	
}
