package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.Table;
import com.gaforce.killer7.main.MainLoop;
import com.gaforce.killer7.message.Message;
import com.gaforce.killer7.resource.ResourceManager;
import com.gaforce.killer7.resource.UITaskQueue;
import com.gaforce.killer7.resource.UITaskQueue.UITask;
import com.gaforce.killer7.resource.UITaskQueue.UITaskListener;
import com.gaforce.killer7.resource.UITaskQueue.UITaskUnit;

/**
 * 
 * <p>
 * ColorLinesFire Game main.
 * </p>
 * 
 * @author mingming
 *
 */
public class MyGame extends MainLoop 
	implements UITaskListener {
	
	public final static String TAG = "CLF.MainGame: ";
	
	private static Platform mPlatformAPI = null;
	private static MyGame mInstance = null;
	
	private boolean mNeedAdjustSize = false;
	private UITaskUnit mResumeTask = null;
	
	private AssetLoadListener mLoadListener = null;
	private UITaskQueue mUITaskQueue = null;
	
	
	public interface AssetLoadListener {
		public void assetLoadDone();
		
		public void waitLoadScreen(SpriteBatch batch);
	}
	
	/**
	 * Get singleton object instance.
	 * @return singleton instance.
	 */
	public static MyGame getInstance() {
		if (null == mInstance) {
			mInstance = new MyGame();
		}
		return mInstance;
	}
	
	private MyGame() {
		super();
    }
	
	public static Platform getPlatformAPI() {
		return mPlatformAPI;
	}
	
	public static void setPlatformAPI (Platform platform) {
		mPlatformAPI = platform;
	}
	
	public void registerAssetLoadListener(AssetLoadListener listener) {
		mLoadListener = listener;
	}
	
	public boolean commitUITask(UITaskUnit unit) {
		if (null == mUITaskQueue) {
			return false;
		}
		
		return mUITaskQueue.commitTask(unit);
	}
	
	public void unregisterAssetLoadListener(AssetLoadListener listener) {
		// we don't allow unregister other register listener. 
		if (null == mLoadListener || !mLoadListener.equals(listener)) {
			return;
		}
		
		mLoadListener = null;
	}
	
	@Override
	public void create() {
		super.create();	
		Utils.log(TAG + "========> create() called");
		
		mNeedAdjustSize = false;
		mLoadListener = null;
		
		mUITaskQueue = new UITaskQueue();
		mResumeTask = new UITaskUnit(new ResumeTask(), null, this);
		
		// initialize global resource.
		Utils.preloadAsset();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		Utils.log(TAG + "========> resize() called: w:" + width + ", h:" + height);
	    
		// if not resource not load complete, 
		// we must wait resource load complete.
	    //Scene.setDefaultSize(width, height);
		if (Utils.checkAssetUpdate()) {
			mNeedAdjustSize = false;
			Utils.adjustSceneSize(width, height);
		} else {
			mNeedAdjustSize = true;
		}
	    
	    Utils.getStage().setViewport(width, height, true);
	}
	
	@Override
	public void render() {
		super.render();
		
		// clear background
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		// draw game
		draw(Utils.getStage());
	}

	@Override
	public void pause() {
		super.pause();
		
		Utils.log(TAG + "========> pause() called");
		
		ResourceManager.getInstance().pause();
	}

	@Override
	public void resume() {
		super.resume();
		
		Utils.log(TAG + "========> resume() called");
		
		//ResourceManager.getInstance().resume();
		mUITaskQueue.commitTask(mResumeTask);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		Utils.log(TAG + "========> dispose() called");
		
		mLoadListener = null;
		
		if (null != mUITaskQueue) {
			mUITaskQueue.destory();
			mUITaskQueue = null;
		}
		
		ResourceManager.getInstance().destroy();
		
		// let system gc
		System.gc();
	}
	
	@Override
	protected boolean defaultMessageHandler(Message msg) {
		boolean interrupted = false;
		
		switch (msg.what) {
        case UserMsgDef.MSG_EXIT_APP:
        	Platform platform = MyGame.getPlatformAPI();
    		if (null != platform) {
    			platform.exitApplication();
    		}
    		
    		interrupted = true;
        	break;
        	
        default:
        	break;
        }
		
		return interrupted;
	}
	
	private void draw(Stage stage) {
		SpriteBatch batch = stage.getSpriteBatch();
		
		//if (!drawLoadScreen(batch)) {
		//	drawGame(stage);
		//}
		
		if (checkAsset(batch)) {
			if (checkUITask(batch)) {
				drawGame(stage);
			}
		}
		
		drawFPS(batch);
	}
	
	private void drawGame(Stage stage) {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		
		// draw table-layout debug rectangle.
        Table.drawDebug(stage);
	}
	
	private boolean checkAsset(SpriteBatch batch) {
		// if AssetManager complete load, than we will show game screen.
		if (Utils.checkAssetUpdate()) {			
			if (null != mLoadListener) {
				mLoadListener.assetLoadDone();
				
				if (mNeedAdjustSize) {
					mNeedAdjustSize = false;
					Utils.adjustSceneSize(Utils.getWidth(), Utils.getHeight());
				}
			}
			
			return true;
		}
		
		// or we will show loading screen.	
		if (null != mLoadListener) {
			mLoadListener.waitLoadScreen(batch);
		}
        
        return false;
	}
	
	private boolean checkUITask(SpriteBatch batch) {
		if (null == mUITaskQueue) {
			return true;
		}
		
		return mUITaskQueue.checkTask(batch);
	}
	
	private void drawFPS(SpriteBatch batch) {
		BitmapFont font = Utils.getSysFont();
		
        batch.begin();
        {	
        	if (null != font) {
        		font.draw(batch, 
        				String.format("FPS: %d", Gdx.graphics.getFramesPerSecond()), 
        				0, Utils.getHeight() - 5);
        	}
        }
        batch.end();
	}
	
	
	private class ResumeTask implements UITask {

		@Override
		public boolean doUITask(Object params) {
			ResourceManager.getInstance().resume();
			return true;
		}
		
	}

	@Override
	public void uiTaskComplete(UITask task) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void waitUITaskScreen(SpriteBatch batch) {
		Utils.drawBackground(batch, 1.0f);
		
		Utils.defaultDrawLoadScreen(batch);
	}
	
}
