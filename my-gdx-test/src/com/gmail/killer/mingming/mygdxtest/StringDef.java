package com.gmail.killer.mingming.mygdxtest;

/**
 * 
 * <p>
 * String keys defined.
 * </p>
 * 
 * @author mingming
 *
 */
public class StringDef {
	
	public final static String STR_MAIN_SOLO = "str_main_solo";
	public final static String STR_MAIN_OPTIONS = "str_main_options";
	public final static String STR_MAIN_EXIT = "str_main_exit";
	  
	public final static String STR_SOLO_READY_CONTINUE = "str_solo_ready_continue";	
	public final static String STR_SOLO_READY_RESTART = "str_solo_ready_restart";
	  
	public final static String STR_SOLO_PAUSE_RESUME = "str_solo_pause_resume";
	public final static String STR_SOLO_PAUSE_RESTART = "str_solo_pause_restart";
	public final static String STR_SOLO_PAUSE_SAVE_EXIT = "str_solo_pause_save_exit";
	public final static String STR_SOLO_PAUSE_EXIT = "str_solo_pause_exit";
	
	public final static String STR_TIP_EXIT = "str_tip_exit";
	
	public final static String STR_YES = "str_yes";
	public final static String STR_NO = "str_no";
	
}
