package com.gmail.killer.mingming.mygdxtest;

/**
 * 
 * <p>
 * Implement it to provide platform dependent API.
 * like close application, pop up dialog and so on. 
 * </p>
 * 
 * @author mingming
 */
public interface Platform {
	public void exitApplication();
	
	public String getSaveFilePath();
}
