package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gaforce.killer7.statemachine.Event;
import com.gaforce.killer7.statemachine.Transition;
import com.gaforce.killer7.ui.Scene;

/**
 * 
 * <p>
 * Background scene ... ...
 * </p>
 * 
 * @author mingming
 *
 */
public class BackgroundScene extends Scene {
	
	private static final String NAME_BK = "Background";
	private Background mBackground;
	
	
	public BackgroundScene(Stage stage, String name) {
		super(stage, name, Utils.getMainGame());
	}
	
	/*@Override
	public void create() {
		if (null == mBackground) {
			mBackground = new Background(NAME_BK);
			
			// now the background show fill whole screen.
			mBackground.setFillParent(true);
		}
	}

	@Override
	public void pause() {
		finishAct();
		mBackground = null;
	}

	@Override
	public void resume() {
		if (null == mBackground) {
			mBackground = new Background(NAME_BK);
			
			// now the background show fill whole screen.
			mBackground.setFillParent(true);
		}
		
		layout();
		
		beginAct();
	}
	
	@Override
	public void destroy() {
		pause();
	}*/

	@Override
	public boolean beginAct() {
		if (null == mStage) {
			return false;
		}
		
		if (null != mBackground && null == mStage.findActor(NAME_BK)) {
			mStage.addActor(mBackground);
		}
		
		return true;
	}

	@Override
	public void finishAct() {
		if (null == mStage) {
			return;
		}
		
		if (null != mBackground) {
			mStage.removeActor(mBackground);
		}
	}
	
	@Override
	protected void onEnter(Event event, Transition transition) {
		// do noting.
    }
	
	@Override
    protected void onLeave(Event event, Transition transition) {
		// do noting.
    }
	
	@Override
	protected boolean deployScene() {
		if (null == mBackground) {
			mBackground = new Background(NAME_BK);
			
			// now the background show fill whole screen.
			mBackground.setFillParent(true);
		}
		
		return true;
	}
	
	@Override
	protected void restoreScene() {
		deployScene();
		
		layout();
		
		beginAct();
	}
	
	@Override
	protected void cleanScene() {
		mBackground = null;
	}

}
