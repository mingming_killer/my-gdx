package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.gaforce.killer7.ui.SerializerElementDef;
import com.gaforce.killer7.ui.SerializerFactory;
import com.gaforce.killer7.utils.K7Utils;

//public class Background extends Actor {
public class Background extends Widget {
	private TextureRegion mRegion;
	
	public Background() {
		super();
		init();
	}
	
	public Background(String name) {
		super(name);
		init();
	}
	
	private void init() {
		try {
			//Texture texture = ResourcePools.getInstance().getTexture(
			//		ResourceParser.parsePath(ResDef.TEX_BK), true);
			Texture texture = Utils.getAsset(ResDef.TEX_BK, Texture.class);
			
			//ObjectMap<Object, ?> params = null;
			ObjectMap<String, Object> params = null;
			
			// get parameter from configure file.
			//String cfgFile = ResourceParser.parseResCfg(
			//		ResourceParser.parsePath(ResDef.TEX_BK), 
			//		ResDef.CFG_FORMAT);
			String cfgFile = K7Utils.parseResCfg(
					GlobalResource.getInstance().resolverPath(ResDef.TEX_BK), 
					ResDef.CFG_FORMAT);
			
			params = SerializerFactory.readObject(Gdx.files.internal(cfgFile));
			if (null == params) {
				return;
			}
			
			Array<Float> pos = SerializerFactory.getFloatArray(
					params, SerializerElementDef.POSITION);
			
			float tmp = 0;
			tmp = pos.get(0);
			int x = (int)tmp;
			tmp = pos.get(1);
			int y = (int)tmp;
			tmp = pos.get(2);
			int w = (int)tmp;
			tmp = pos.get(3);
			int h = (int)tmp;
			mRegion = new TextureRegion(texture, x, y, w, h);
			
			width = pos.get(2);
			height = pos.get(3);
		
		} catch(Exception e) {
			e.printStackTrace();
			mRegion = null;
		}
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		if (null != mRegion) {
			batch.draw(mRegion, x, y, width, height);
		}
	}
	
	@Override
	public boolean touchDown(float x, float y, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void touchUp(float x, float y, int pointer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void touchDragged(float x, float y, int pointer) {
		// TODO Auto-generated method stub

	}

	@Override
	public Actor hit(float x, float y) {
		// TODO Auto-generated method stub
		return null;
	}

}
