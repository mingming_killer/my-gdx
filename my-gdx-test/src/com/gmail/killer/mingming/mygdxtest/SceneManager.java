package com.gmail.killer.mingming.mygdxtest;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gaforce.killer7.message.Handler;
import com.gaforce.killer7.message.Message;
import com.gaforce.killer7.resource.Cyclable;
import com.gaforce.killer7.resource.ResourceManager;
import com.gaforce.killer7.statemachine.Event;
import com.gaforce.killer7.statemachine.SignalTransition;
import com.gaforce.killer7.statemachine.State;
import com.gaforce.killer7.statemachine.StateMachine;
import com.gaforce.killer7.ui.Scene;

/**
 * 
 * <p>
 * Manage {@link State} and {@link StateMachine}.
 * create all game {@link State} and {@link Transition}.
 * </p>
 * 
 * @author mingming
 * 
 */
public class SceneManager implements 
	Handler.Callback, Cyclable {
	
	private Scene[] mScenes = null;
	private StateMachine mStateMachine = null;
	
	private static SceneManager mInstance = null;
	
	
	/**
	 * In fact I just want to manage the static resource of this class  
	 * by {@link ResourceManager}, so I must have a object of this class. 
	 * And this class all public methods is static. -_-||
	 * 
	 * @return this class static instance.
	 */
	public static SceneManager getInstance() {
		if (null == mInstance) {
			mInstance = new SceneManager();
		}
		
		mInstance.checkStateMachine();
		
		return mInstance;
	}
	
	// we forbid create new instance from outside.
	private SceneManager() {
		
	}
	
	/**
	 * Let {@link StateMachine} run.
	 */
	public void run() {
		mStateMachine.start();
	}
	
	/**
	 * Just a wrap of {@link StateMachine#postEvent(Event)}.
	 * 
	 * @param event see {@link StateMachine#postEvent(Event)}.
	 */
	public void postEvent(Event event) {
		mStateMachine.postEvent(event);
	}
	
	/**
	 * Just a wrap of {@link StateMachine#postEvent(Event)}.
	 * 
	 * @param event see {@link StateMachine#postEvent(Event)}.
	 */
	public void sendEvent(Event event) {
		mStateMachine.sendEvent(event);
	}
	
	/**
	 * When game size is change we adjust all scenes size.
	 * 
	 * @param w: width
	 * @param h: height
	 */
	public void adjustSceneSize(int width, int height) {
		Scene.setDefaultSize(width, height);
		
		if (null != mScenes) {
			for (int i = 0; i < mScenes.length; i++) {
				mScenes[i].adjustSize(width, height);
			}
		}
	}
	
	/**
	 * Reload string by current language.
	 */
	public void reloadLang() {
		if (null != mScenes) {
			for (int i = 0; i < mScenes.length; i++) {
				mScenes[i].reloadLang();
			}
		}
	}
	
	/**
	 * Get this class static object.
	 * 
	 * @return StateMachine instance.
	 */
	public StateMachine getStateMachine() {
		return mStateMachine;
	}
	
	/**
	 * Get a specified {@link State} in {@link StateMachine} by name.
	 * 
	 * @param name: scene name.
	 * @return state object, or if don't find in state machine is null.
	 */
	public Scene getScene(final String name) {
		if (null == name || null == mScenes) {
			return null;
		}
		
		for (Scene scene : mScenes) {
			if (name.equals(scene.getName())) {
				return scene;
			}
		}
		
		return null;
	}
	
	/**
	 * Get a specified {@link State} in {@link StateMachine} by id.
	 * 
	 * @param id: scene index.
	 * @return state object, or if index is over range is null.
	 */
	public Scene getScene(final int id) {
		if (null == mScenes || 
				id < 0 || id >= mScenes.length) {
			return null;
		}
		
		return mScenes[id];
	}
	
	@Override
	public boolean handleMessage(Message msg) {
		if (null == msg) {
			return false;
		}
		
		boolean interrupted = false;
		
		switch (msg.what) {
		case UserMsgDef.MSG_STATEMACHINE_EVENT:
			mStateMachine.onEvent();
			// we don't want other handler to process state machine message.
			interrupted = true;
			break;
			
		default:
			break;
		}
		
		return interrupted;
	}
	
	@Override
	public void create() {
		if (null != mScenes) {
			for (int i = 0; i < mScenes.length; i++) {
				mScenes[i].create();
			}
		}
	}

	@Override
	public void pause() {
		if (null != mScenes) {
			for (int i = 0; i < mScenes.length; i++) {
				mScenes[i].pause();
			}
		}
	}

	@Override
	public void resume() {
		if (null != mScenes) {
			for (int i = 0; i < mScenes.length; i++) {
				mScenes[i].resume();
			}
		}
	}
	
	@Override
	public void destroy() {
		// unregister state machine message handler.
		Utils.unregisterHandlerCallback(getInstance());
		
		if (null != mScenes) {
			for (int i = 0; i < mScenes.length; i++) {
				mScenes[i].destroy();
				mScenes[i] = null; 
			}
			mScenes = null;
		}
		
		if (null != mStateMachine) {
			mStateMachine = null;
		}
	}
	
	private void checkStateMachine() {
		if (null == mStateMachine) {
			initStateMachine();
		}
	}
	
	private void initStateMachine() {
		// create state machine.
		mStateMachine = new StateMachine(Utils.getMainGame(), 
				UserMsgDef.MSG_STATEMACHINE_EVENT);
		
		// register state machine message handler.
		Utils.registerHandlerCallback(getInstance());
		
		// create scenes.
		createScenes();
	}
	
	private void createScenes() {
		if (null != mScenes) {
			return;
		}
		
		Stage stage = Utils.getStage();
		//SignalTransition sigTrans = null;
		mScenes = new Scene[SceneDef.STATE_TOTAL];
		
		// create scenes
		// just create background scene, not add it to state machine.
		// and to scenes array order determine it's pause and resume too.
		// so add background first.
		mScenes[SceneDef.SCENE_BACKGROUND_ID] = new BackgroundScene(stage, 
				SceneDef.SCENE_BACKGROUND);
		
		mScenes[SceneDef.SCENE_MAIN_MENU_ID] = new MainMenuScene(stage, 
				mStateMachine, SceneDef.SCENE_MAIN_MENU);
		
		mScenes[SceneDef.SCENE_OPTIONS_MENU_ID] = new OptionsMenuScene(stage, 
				mStateMachine, SceneDef.SCENE_OPTIONS_MENU);
		
		// create transitions
		// main menu
		/*sigTrans =*/new SignalTransition(getInstance(), 
				SceneDef.SIGNAL_MAIN_TO_OPTIONS.hashCode(), 
				mScenes[SceneDef.SCENE_MAIN_MENU_ID], 
				mScenes[SceneDef.SCENE_OPTIONS_MENU_ID], 
				SceneDef.SIGNAL_MAIN_TO_OPTIONS);
		//sigTrans.subscribe((Scene)mScenes[StateID.STATE_MAIN_MENU_ID], null);
		
		// options menu
		new SignalTransition(getInstance(), 
				SceneDef.SIGNAL_OPTIONS_TO_MAIN.hashCode(), 
				mScenes[SceneDef.SCENE_OPTIONS_MENU_ID], 
				mScenes[SceneDef.SCENE_MAIN_MENU_ID], 
				SceneDef.SIGNAL_OPTIONS_TO_MAIN);
		
		// set initial state.
		mStateMachine.setInitialState(mScenes[SceneDef.SCENE_MAIN_MENU_ID]);
		//mStateMachine.setInitialState(mScenes[StateDefined.STATE_SOLO_GAME_ID]);
	}
	
}
