package com.gmail.killer.mingming.mygdxtest;

/**
 * 
 * <p>
 * Defined Game Element.
 * The .json file specified field defined.
 * The element defined in .json must in this class.
 * </p>
 * 
 * @author mingming
 *
 */
public class GameElementDef {
	
	// style defined	
	public final static String SKIN_STYLE_DEF = "default";
	public final static String FONT_DEF = "default-font";
	
	public final static String STYLE_ITEM_BAR = "item-bar";
	public final static String STYLE_BTN_BACK = "image-back";
	
	
	// element of ball: 1 ~ 7 (special item ball:all)
	public final static String BALL = "ball";
	
	public final static String BALL_1_RECT = "ball_1_rect";
	public final static String BALL_2_RECT = "ball_2_rect";
	public final static String BALL_3_RECT = "ball_3_rect";
	public final static String BALL_4_RECT = "ball_4_rect";
	public final static String BALL_5_RECT = "ball_5_rect";
	public final static String BALL_6_RECT = "ball_6_rect";
	public final static String BALL_7_RECT = "ball_7_rect";
	public final static String BALL_ALL_RECT = "ball_all_rect";
	public final static String BALL_GRID_RECT = "ball_grid_rect";
	
	public final static String BALL_NORMAL_RECT = "ball_normal_rect";
	public final static String BALL_APPEAR_RECT = "ball_appear_rect";
	public final static String BALL_SELECTED_RECT = "ball_selected_rect";
	public final static String BALL_MOVING_RECT = "moving";
	public final static String BALL_DESTROY_RECT = "ball_destroy_rect";
	
	
	// element of item
	public final static String ITEM_BOMB_RECT = "item_bomb_rect";
	public final static String ITEM_STOP_RECT = "item_stop_rect";
	public final static String ITEM_ALL_RECT = "item_all_rect";
	public final static String ITEM_SPEED_RECT = "item_speed_rect";
	
	public final static String ITEM_LV_RECT = "item_lv_rect";
	public final static String ITEM_LV_NONE_RECT = "item_lv_none_rect";
	
	public final static String ITEM_NORMAL_RECT = "normal";
	public final static String ITEM_APPEAR_RECT = "item_appear_rect";
	public final static String ITEM_UPGRADE_RECT = "item_upgrade_rect";;
	public final static String ITEM_SELECTED_RECT = "item_selected_rect";
	public final static String ITEM_DESTROY_RECT = "destroy";
	
	
	// supports language
	public final static String LANG_EN = "en";
	public final static String LANG_CN = "zh-rCN";
	
}
