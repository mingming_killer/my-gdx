package com.gmail.killer.mingming.mygdxtest;

/**
 * 
 * <p>
 * Game configure.
 * </p>
 * 
 * @author mingming
 *
 */
public class GameConfig {
	
	//***************************************************************
	// game rule configure.
	//***************************************************************
	
	// total of table row and column.
	public final static int ROW_TOTAL = 9;
	public final static int COL_TOTAL = 9; //8;
	
	// number of next step create balls.
	public final static int NEXT_COUNT = 3;
	
	// minimum destroyable number.
	public final static int DESTROY_BASE = 5;
	
	//***************************************************************
	//***************************************************************
	//***************************************************************
	
	
	
	//***************************************************************
	// game score configure.
	//***************************************************************
	
	// base score: score of destroy one ball.
	public final static int BASE_SCORE = 10;
	
	// destroy number of enter bonus. 
	public final static int DESTROY_ENTER_LV1 = 1;
	public final static int DESTROY_ENTER_LV2 = 1;
	public final static int DESTROY_ENTER_LV3 = 1;
	
	// destroy bonus factor.
	public final static float DESTROY_BONUS_LV1 = 0.4f;  // 40%
	public final static float DESTROY_BONUS_LV2 = 0.7f;  // 70%
	public final static float DESTROY_BONUS_LV3 = 1.0f;  // 100%
	
	// combat bonus factor.
	public final static float COMBAT_BONUS = 0.5f;  // 50%
	
	//***************************************************************
	//***************************************************************
	//***************************************************************
	
	
	
	//***************************************************************
	// item configure. 
	//***************************************************************
	
	public final static int ITEM_MAX = 3;
	public final static int ITEM_GAGUE = 280;
	public final static int ITEM_BONUS = 200;
	
	public final static float ITEM_LV_WIDTH = 0.25f;
	public final static float ITEM_LV_HEIGHT = 0.25f;
	
	//***************************************************************
	//***************************************************************
	//***************************************************************
	
	
	
	//***************************************************************
	// game actions(animation) configure. 
	//***************************************************************
	
	// ball moving speed factor (grid/second), 
	// the final speed depend table grid width.
	public final static int BALL_MOVE_SPEED = 6;
	
	// ball frame animation duration(second), 
	// like appear, destroy animation is frame animation.
	public final static float BALL_FRAME_ANIMATION_DURATION = 0.1f;
	
	// item frame animation duration(second).
	public final static float ITEM_FRAME_ANIMATION_DURATION = 0.1f;
	
	public final static float ITEM_EFFECT_SCALE_FACTOR = 2.0f;
	
	public final static float ITEM_MOVING_DURATION = 0.3f;
	public final static float ITEM_DESTROY_DURATION = 0.5f;
	
	
	// flash score label appear and disappear animation duration.
	public final static float FLASH_SCORE_IN_DURATION = 0.8f;
	public final static float FLASH_SCORE_OUT_DURATION = 0.4f;
	
	//***************************************************************
	//***************************************************************
	//***************************************************************
	
	
	
	//***************************************************************
	// other configure. 
	//***************************************************************
	
	public final static float DLG_WIDTH = 0.6f;
	public final static float DLG_HEIGTH = 0.8f;
	public final static float DLG_DURATION = 0.8f;
	
	public final static float POPUP_MENU_WIDTH = 0.6f;
	public final static float POPUP_MENU_HEIGTH = 0.8f;
	public final static float POPUP_MENU_DURATION = 0.8f;
	
	public final static int PROGRESS_MIN = 0;
	public final static int PROGRESS_MAX = 100;
	
	public final static float SCREEN_MASK_ALPHA = 0.7f;
	public final static int SCREEN_MASK_COLOR = 0x000000ff;  // format: RGBA
	
	public final static int SPRITE_CACHE_SIZE = 128;
	
	//***************************************************************
	//***************************************************************
	//***************************************************************
	
}
