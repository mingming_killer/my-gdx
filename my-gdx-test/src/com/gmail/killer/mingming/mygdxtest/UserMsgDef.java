package com.gmail.killer.mingming.mygdxtest;

import com.gaforce.killer7.message.MsgDef;

/**
 * 
 * <p>
 * Defined all message which use by {@link Handler}.
 * </p>
 * 
 * @author mingming
 */
public class UserMsgDef {
	
	private final static int BASE = MsgDef.MSG_USER;
	
	/**
	 * @param null
	 */
    public final static int MSG_STATEMACHINE_EVENT = BASE + 1;
    
    /**
     * @param null
     */
    public final static int MSG_EXIT_APP = BASE + 2;
    
    /**
     * @param obj (ArrayList<Vector2>) list of add balls
     */
    public final static int MSG_BALL_ADD = BASE + 3;
    
    /**
     * @param obj (ArrayList<Integer>) list of remove balls
     */
    public final static int MSG_BALL_REMOVE = BASE + 4;
    
    /**
     * @param arg1 (int) start position
     * @param arg2 (int) target position
     */
    public final static int MSG_BALL_MOVING = BASE + 5;
    
    /**
     * @param obj (Ball) ball who action moving.
     */
    public final static int MSG_BALL_MOVING_FINISH = BASE + 6;
    
    /**
     * @param arg1 (int) old selected ball, if -1 means is self.
     * @param arg2 (int) current selected ball
     */
    public final static int MSG_BALL_SELECTED = BASE + 7;
	
}
