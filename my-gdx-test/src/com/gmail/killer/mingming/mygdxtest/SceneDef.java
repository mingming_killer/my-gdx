package com.gmail.killer.mingming.mygdxtest;

/**
 * 
 * <p>
 * Defined all {@link State} which use by 
 * {@link StateMachine}.
 * </p>
 * 
 * @author mingming
 */
public class SceneDef {
	
	// the state array index.
	public final static int SCENE_BACKGROUND_ID = 0;
	public final static int SCENE_MAIN_MENU_ID = 1;
	public final static int SCENE_OPTIONS_MENU_ID = 2;
	
	// this total must match the state array index.
	public final static int STATE_TOTAL = 3;
	
	
	// the State name also Scene name.
	public final static String SCENE_BACKGROUND = "SceneBackground";
	public final static String SCENE_MAIN_MENU = "SceneMainMenu";
	public final static String SCENE_OPTIONS_MENU = "SceneOptionsMenu";
	
	
	// transition signal defined
	public final static String SIGNAL_MAIN_TO_OPTIONS = "SignalMainToOptions";
	
	public final static String SIGNAL_OPTIONS_TO_MAIN = "SignalOptionsToMain";
	
}
