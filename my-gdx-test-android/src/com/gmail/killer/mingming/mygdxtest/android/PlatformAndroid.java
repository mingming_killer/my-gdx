package com.gmail.killer.mingming.mygdxtest.android;

import com.gmail.killer.mingming.mygdxtest.Platform;

import android.app.Activity;

public class PlatformAndroid implements Platform {
	
	private final static String SAVE_FILE_PATH = "Gaforce/MyTest/";
	
	private Activity mMainActivity = null;
	
	
	public PlatformAndroid (Activity context) {
		mMainActivity = context;
	}

	public void exitApplication() {
		if (null != mMainActivity) {
			mMainActivity.finish();
		}
	}

	public String getSaveFilePath() {
		return SAVE_FILE_PATH;
	}

}
