package com.gmail.killer.mingming.mygdxtest.android;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.gmail.killer.mingming.mygdxtest.MyGame;
import com.gmail.killer.mingming.mygdxtest.Platform;
import com.gmail.killer.mingming.mygdxtest.Utils;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;

public class MainActivity extends AndroidApplication {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Platform platform = new PlatformAndroid(this);
        MyGame.setPlatformAPI(platform);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        
        initialize(MyGame.getInstance(), cfg);
    }
    
    @Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		// just for debug
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		
		Utils.log("Configuration: " + dm.widthPixels + ", " + dm.heightPixels);
    }
}