package com.gaforce.killer7.interpolators;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.utils.Pool;
import com.gaforce.killer7.utils.K7Utils;

/**
 * libgdx internal interpolations.
 * 
 * @author mingming
 *
 */
public class SystemInterpolator implements Interpolator {
	
	// libgdx internal interpolations.
	public final static String Bounce = "bounce";
	public final static String BounceIn = "bounceIn";
	public final static String BounceOut = "bounceOut";
	public final static String Circle = "circle";
	public final static String CircleIn = "circleIn";
	public final static String CircleOut = "circleOut";
	public final static String Elastic = "elastic";
	public final static String ElasticIn = "elasticIn";
	public final static String ElasticOut = "elasticOut";
	public final static String Exp10 = "exp10";
	public final static String Exp10In = "exp10In";
	public final static String Exp10Out = "exp10Out";
	public final static String Exp5 = "exp5";
	public final static String Exp5In = "exp5In";
	public final static String Exp5Out = "exp5Out";
	public final static String Fade = "fade";
	public final static String Linear = "linear";
	public final static String Pow2 = "pow2";
	public final static String Pow2In = "pow2In";
	public final static String Pow2Out = "pow2Out";
	public final static String Pow3 = "pow3";
	public final static String Pow3In = "pow3In";
	public final static String Pow3Out = "pow2Out";
	public final static String Pow4 = "pow4";
	public final static String Pow4In = "pow4In";
	public final static String Pow4Out = "pow4Out";
	public final static String Pow5 = "pow5";
	public final static String Pow5In = "pow5In";
	public final static String Pow5Out = "pow5Out";
	public final static String Sine = "sine";
	public final static String SineIn = "sineIn";
	public final static String SineOut = "sineOut";
	public final static String Swing = "swing";
	public final static String SwingIn = "swingIn";
	public final static String SwingOut = "swingOut";
	
	private final static Interpolation DEFAULT = Interpolation.linear;
	private final static String DEFAULT_FACTOR = Linear;
	
	private String mFactor;
	private Interpolation mInterpolation;
	
	
	private static final Pool<SystemInterpolator> mPool = new Pool<SystemInterpolator>(4, 100) {
		@Override
		protected SystemInterpolator newObject() {
			return new SystemInterpolator();
		}
	};

	// hide constructor
	private SystemInterpolator() {
		
	}

	/** 
	 * Gets a new {@link SystemInterpolator} from a maintained 
	 * pool of {@link Interpolator}s.
	 * 
	 * @param factor use the libgdx default interpolations.
	 * @return the obtained {@link SystemInterpolator} 
	 */
	public static SystemInterpolator $(String factor) {
		SystemInterpolator inter = mPool.obtain();
		inter.mFactor = factor;
		inter.mInterpolation = getInterpolation(factor);
		return inter;
	}

	/** 
	 * Gets a new {@link SystemInterpolator} from a maintained 
	 * pool of {@link Interpolator}s.
	 * <p>
	 * The initial factor is set to <code>{@value SystemInterpolator#DEFAULT_FACTOR}</code>.
	 * 
	 * @return the obtained {@link SystemInterpolator} 
	 */
	public static SystemInterpolator $() {
		return $(DEFAULT_FACTOR);
	}

	@Override
	public void finished() {
		mPool.free(this);
	}

	public float getInterpolation(float input) {		
		return mInterpolation.apply(input);
	}

	@Override
	public Interpolator copy() {
		return $(mFactor);
	}
	
	// get libgdx system internal interpolation.
	private static Interpolation getInterpolation(String factor) {
		try {
			return (Interpolation)Interpolation.class.getField(factor).get(null);
		} catch (Exception e) {
			K7Utils.log("K7.SystemInterpolator: getInterpolation: " + 
					"you set interpolation is not existed !!", e);
			return DEFAULT;
		}
	}

}
