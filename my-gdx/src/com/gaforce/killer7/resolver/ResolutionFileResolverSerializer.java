package com.gaforce.killer7.resolver;

import java.util.ArrayList;

import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver.Resolution;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Json.Serializer;
import com.gaforce.killer7.ui.SerializerElementDef;
import com.gaforce.killer7.ui.SerializerFactory;
import com.gaforce.killer7.utils.K7Utils;

public class ResolutionFileResolverSerializer {
	
	public final static String TAG = "K7.ResolutionFileResolverSerializer: ";
	
	public static Serializer<ResolutionFileResolver> getResolutionFileResolverSerializer() {
		return new Serializer<ResolutionFileResolver>() {
			
			@SuppressWarnings("rawtypes")
			@Override
			public void write(Json json, ResolutionFileResolver object, Class type) {
				// we don't support write to this serializable object.
				return;
			}
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public ResolutionFileResolver read(Json json, Object jsonData, Class knownType) {
				if (jsonData == null) {
					return null;
				}
				
				ResolutionFileResolver self = null;
				ObjectMap<String, Object> params = null;
				ArrayList<Resolution> resArray = null;
				Resolution[] res = null;
				
				try {
					params = (ObjectMap) jsonData;
					
					// configure json parameters.
					// create all ScaleFactor who declare in configure file
					resArray = serializeResolutions(params);
					
					if (null != resArray) {						
						res = (Resolution[])resArray.toArray(new Resolution[0]);
						resArray.clear();
						resArray = null;
						
						// now just support resolver internal file.
						self = new ResolutionFileResolver(
								new InternalFileHandleResolver(), res);
					}
					
				} catch (Exception e) {
					K7Utils.log(TAG, e);
					return null;
				}
				
				return self;
			}
			
		};
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Resolution> serializeResolutions(ObjectMap<String, Object> data) {
		if (null == data) {
			return null;
		}
		
		ArrayList<Resolution> resolutions = new ArrayList<Resolution> ();
		
		Array<Object> array = SerializerFactory.getObjectArray(
				data, SerializerElementDef.RESOLUTION_RESOLVER);
		if (null == array) {
			return null;
		}
		
		// serialize Resolution form configure file.
		Resolution resolution = null;
		for (Object obj : array) {
			// allow strings in JSON for comments
			if (!(obj instanceof ObjectMap)) { 
				continue;
			}
			
			// instance ScaleFactor in the json file.
			ObjectMap<String, Object> params = (ObjectMap<String, Object>) obj;
			resolution = serializeResolution(params);
			
			if (null != resolution) {
				resolutions.add(resolution);
			}
		}
		
		return resolutions;
	}
	
	public static Resolution serializeResolution(ObjectMap<String, Object> data) {
		try {
			int width = SerializerFactory.getInteger(data, SerializerElementDef.WIDTH, 0);
			int height = SerializerFactory.getInteger(data, SerializerElementDef.HEIGTH, 0);
			String suffix = SerializerFactory.getString(data, SerializerElementDef.SUFFIX, "");
			
			return new Resolution(width, height, suffix);
			
		} catch (Exception e) {
			K7Utils.log(TAG, e);
			return null;
		}
	}

}
