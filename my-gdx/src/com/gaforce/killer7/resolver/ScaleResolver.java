package com.gaforce.killer7.resolver;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.OrderedMap;
import com.gaforce.killer7.ui.SerializerElementDef;
import com.gaforce.killer7.ui.SerializerFactory;
import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * Resolver how scale factor is.
 * </p>
 * 
 * @author mingming
 *
 */
public class ScaleResolver implements Serializable {
	
	public final static String TAG = "K7.ScaleResolver: ";
	
	private final static float DEFAULT_FACTOR = 1.0f;
	
	
	public static class ScaleFactor {
		int mWidth;
		int mHeight;
		float mScaleFactor;
		
		public ScaleFactor(int width, int height, float scale) {
			mWidth = width;
			mHeight = height;
			mScaleFactor = scale;
		}
	}
	
	
	protected ScaleFactor[] mDescriptors;
	
	
	public ScaleResolver() {
		mDescriptors = null;
	}
	
	public ScaleResolver(ScaleFactor... descriptors) {
		mDescriptors = descriptors;
	}
	
	public float resolve() {
		if (null == mDescriptors) {
			return DEFAULT_FACTOR;
		}
		
		ScaleFactor bestDesc = choose(mDescriptors);
		return bestDesc.mScaleFactor;
	}
	
	protected void setDescriptors(ScaleFactor... descriptors) { 
		mDescriptors = descriptors;
	}
	
	public static ScaleFactor choose(ScaleFactor... descriptors) {
		int width = 0;
		if (Gdx.graphics.getWidth() > Gdx.graphics.getHeight()) {
			width = Gdx.graphics.getHeight();
		} else {
			width = Gdx.graphics.getWidth();
		}

		ScaleFactor bestDesc = null;
		
		// Find lowest.
		int best = Integer.MAX_VALUE;
		for (int i = 0, n = descriptors.length; i < n; i++) {
			if (descriptors[i].mWidth < best) {
				best = descriptors[i].mWidth;
				bestDesc = descriptors[i];
			}
		}
		
		// Find higher, but not over the screen res.
		best = Integer.MAX_VALUE;
		for (int i = 0, n = descriptors.length; i < n; i++) {
			if (descriptors[i].mWidth <= width) {
				best = descriptors[i].mWidth;
				bestDesc = descriptors[i];
			}
		}
		
		return bestDesc;
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<ScaleFactor> serializeScaleFactors(ObjectMap<String, Object> data) {
		if (null == data) {
			return null;
		}
		
		ArrayList<ScaleFactor> scaleFactors = new ArrayList<ScaleFactor> ();
		
		Array<Object> array = SerializerFactory.getObjectArray(
				data, SerializerElementDef.SCALE_RESOLVER);
		if (null == array) {
			return null;
		}
		
		// serialize ScaleFactor form configure file.
		ScaleFactor scaleFactor = null;
		for (Object obj : array) {
			// allow strings in JSON for comments
			if (!(obj instanceof ObjectMap)) { 
				continue;
			}
			
			// instance ScaleFactor in the JSON file.
			ObjectMap<String, Object> params = (ObjectMap<String, Object>) obj;
			scaleFactor = serializeScaleFactor(params);
			
			if (null != scaleFactor) {
				scaleFactors.add(scaleFactor);
			}
		}
		
		return scaleFactors;
	}
	
	public static ScaleFactor serializeScaleFactor(ObjectMap<String, Object> data) {
		try {
			int width = SerializerFactory.getInteger(data, SerializerElementDef.WIDTH, 0);
			int height = SerializerFactory.getInteger(data, SerializerElementDef.HEIGTH, 0);
			float factor = SerializerFactory.getFloat(data, SerializerElementDef.SCALE_FACTOR, 1.0f);
			
			return new ScaleFactor(width, height, factor);
			
		} catch (Exception e) {
			K7Utils.log(TAG, e);
			return null;
		}
	}

	@Override
	public void write(Json json) {
		// we don't support write to this serializable object.
		return;
	}

	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData) {
		if (null == jsonData) {
			return;
		}
		
		ArrayList<ScaleFactor> scaleFactors = null;
		
		try {
			// create all ScaleFactor who declare in configure file
			scaleFactors = serializeScaleFactors(jsonData);
			
			if (null != scaleFactors) {
				//resolver = new ScaleResolver(
				//		(ScaleFactor[])scaleFactors.toArray(new ScaleFactor[0]));
				//scaleFactors.clear();
				//scaleFactors = null;
				
				mDescriptors = (ScaleFactor[])scaleFactors.toArray(new ScaleFactor[0]);
				scaleFactors.clear();
				scaleFactors = null;
			}
			
		} catch (Exception e) {
			K7Utils.log(TAG, e);
		}
	}

}
