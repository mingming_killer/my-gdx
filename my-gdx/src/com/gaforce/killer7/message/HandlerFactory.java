package com.gaforce.killer7.message;

import com.gaforce.killer7.message.Handler.Callback;
import com.gaforce.killer7.message.Handler.UIThread;
import com.gaforce.killer7.utils.K7Utils;

/**
 * <p>
 * HandlerFactory ... ...
 * <p>
 * 
 * @author mingming
 *
 */
public class HandlerFactory {
	
	private final static String DEFAULT_NAME = "MessageHandlerThread";
	
	private static HandlerThread mHandlerThread = null;
	
	
	public static void initialize(String name) {
		if (null == name) {
			name = DEFAULT_NAME;
		}
		
		if (null != mHandlerThread) {
			mHandlerThread.quit();
		}
		
		mHandlerThread = new HandlerThread(name);
		mHandlerThread.start();
	}
	
	public static void destroy() {
		if (null == mHandlerThread) {
			mHandlerThread.quit();
		}
		
		mHandlerThread = null;
	}
	
	public static Handler getHandler(Callback callback) {
		if (null == mHandlerThread) {
			K7Utils.log("K7.HandlerFactory: getHandler: " + 
					"handler not create: handler thread not existed, call initialze() first !!");
			return null;
		}
		
		return new Handler(mHandlerThread.getLooper(), callback);
	}
	
	public static Handler getHandler(Callback callback, UIThread uiThread) {
		if (null == mHandlerThread) {
			K7Utils.log("K7.HandlerFactory: getHandler: " +  
					"handler not create: handler thread not existed, call initialze() first !!");
			return null;
		}
		
		if (null == uiThread) {
			return null;
		} else {
			return new Handler(mHandlerThread.getLooper(), callback, uiThread);
		}
	}
	
}
