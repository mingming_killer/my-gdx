package com.gaforce.killer7.message;

public class MsgDef {
	
	public final static int MSG_BASE = 0x0000;
	
	// user define message start here.
	public final static int MSG_USER = MSG_BASE + 0x0200;
	
	// (512 - 1) framework message.
	public final static int MSG_FRAMEWORK = MSG_BASE + 1;
	
	public final static int MSG_FW_ANIMATION_FINISH = MSG_FRAMEWORK + 1;

}
