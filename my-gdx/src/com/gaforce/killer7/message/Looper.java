package com.gaforce.killer7.message;

import com.badlogic.gdx.Gdx;
import com.gaforce.killer7.utils.K7Utils;

/**
  * Class used to run a message loop for a thread.  Threads by default do
  * not have a message loop associated with them; to create one, call
  * {@link #prepare} in the thread that is to run the loop, and then
  * {@link #loop} to have it process messages until the loop is stopped.
  * 
  * <p>Most interaction with a message loop is through the
  * {@link Handler} class.
  * 
  * <p>This is a typical example of the implementation of a Looper thread,
  * using the separation of {@link #prepare} and {@link #loop} to create an
  * initial Handler to communicate with the Looper.
  * 
  * <pre>
  *  class LooperThread extends Thread {
  *      public Handler mHandler;
  *      
  *      public void run() {
  *          Looper.prepare();
  *          
  *          mHandler = new Handler() {
  *              public void handleMessage(Message msg) {
  *                  // process incoming messages here
  *              }
  *          };
  *          
  *          Looper.loop();
  *      }
  *  }</pre>
  *  
  *  base on framework/base/core/java/android/os/Looper.java
  *  
  *  @author mingming
  */
public class Looper {
	
	/** android os/Process.supportsProcesses(): 
	 * Determine whether the current environment supports multiple processes. 
	 * we now just set it to true */ 
	private final static boolean SUPPORT_PROCESS = true;

    // sThreadLocal.get() will return null unless you've called prepare().
    @SuppressWarnings("rawtypes")
	private static final ThreadLocal sThreadLocal = new ThreadLocal();

    final MessageQueue mQueue;
    volatile boolean mRun;
    Thread mThread;
    private static Looper mMainLooper = null;
    
     /** Initialize the current thread as a looper.
      * This gives you a chance to create handlers that then reference
      * this looper, before actually starting the loop. Be sure to call
      * {@link #loop()} after calling this method, and end it by calling
      * {@link #quit()}.
      */
    @SuppressWarnings("unchecked")
	public static final void prepare() {
        if (sThreadLocal.get() != null) {
            throw new RuntimeException("Only one Looper may be created per thread");
        }
        sThreadLocal.set(new Looper());
    }
    
    /** Initialize the current thread as a looper, marking it as an application's main 
     *  looper. The main looper for your application is created by the Android environment,
     *  so you should never need to call this function yourself.
     * {@link #prepare()}
     */
     
    public static final void prepareMainLooper() {
        prepare();
        setMainLooper(myLooper());
        if (SUPPORT_PROCESS) {
            myLooper().mQueue.mQuitAllowed = false;
        }
    }

    private synchronized static void setMainLooper (Looper looper) {
        mMainLooper = looper;
    }
    
    /** Returns the application's main looper, 
     * which lives in the main thread of the application.
     */
    public synchronized static final Looper getMainLooper () {
        return mMainLooper;
    }

    /**
     *  Run the message queue in this thread. Be sure to call
     * {@link #quit()} to end the loop.
     */
    public static final void loop() {
        Looper me = myLooper();
        MessageQueue queue = me.mQueue;
        while (true) {
            Message msg = queue.next(); // might block
            //if (!me.mRun) {
            //    break;
            //}
            if (msg != null) {
                if (msg.target == null) {
                    // No target is a magic identifier for the quit message.
                    return;
                }
                if (Config.DEBUG) {
                	Gdx.app.log(K7Utils.TAG, "Looper: loop: " + 
                			">>>>> Dispatching to " + msg.target + " "
                			+ msg.callback + ": " + msg.what
                    );
                }
                
                if (msg.target.isInUIThread()) {
                	synchronized(msg.target) {
                		msg.target.postUIMessage(msg);
                	}
                } else {
                	msg.target.dispatchMessage(msg);
                }
                
                if (Config.DEBUG) {
                	Gdx.app.log(K7Utils.TAG, "Looper: loop: " + 
                			"<<<<< Finished to    " + msg.target + " "
                			+ msg.callback
                	);
                }
                msg.recycle();
            }
        }
    }

    /**
     * Return the Looper object associated with the current thread.  Returns
     * null if the calling thread is not associated with a Looper.
     */
    public static final Looper myLooper() {
        return (Looper)sThreadLocal.get();
    }
    
    /**
     * Return the {@link MessageQueue} object associated with the current
     * thread.  This must be called from a thread running a Looper, or a
     * NullPointerException will be thrown.
     */
    public static final MessageQueue myQueue() {
        return myLooper().mQueue;
    }

    private Looper() {
        mQueue = new MessageQueue();
        mRun = true;
        mThread = Thread.currentThread();
    }

    public void quit () {
        Message msg = Message.obtain();
        // NOTE: By enqueueing directly into the message queue, the
        // message is left with a null target.  This is how we know it is
        // a quit message.
        mQueue.enqueueMessage(msg, 0);
    }

    /**
     * Return the Thread associated with this Looper.
     */
    public Thread getThread() {
        return mThread;
    }
    
    public void dump (String prefix) {
        Gdx.app.log(K7Utils.TAG, prefix + this);
        Gdx.app.log(K7Utils.TAG, prefix + "mRun=" + mRun);
        Gdx.app.log(K7Utils.TAG, prefix + "mThread=" + mThread);
        Gdx.app.log(K7Utils.TAG, prefix + "mQueue=" + ((mQueue != null) ? mQueue : "(null"));
        if (mQueue != null) {
            synchronized (mQueue) {
                Message msg = mQueue.mMessages;
                int n = 0;
                while (msg != null) {
                	Gdx.app.log(K7Utils.TAG, prefix + "  Message " + n + ": " + msg);    
                    n++;
                    msg = msg.next;
                }
                Gdx.app.log(K7Utils.TAG, prefix + "(Total messages: " + n + ")");
            }
        }
    }

    public String toString() {
        return "Looper{"
            + Integer.toHexString(System.identityHashCode(this))
            + "}";
    }

    @SuppressWarnings("serial")
	static class HandlerException extends Exception {

        HandlerException(Message message, Throwable cause) {
            super(createMessage(cause), cause);
        }

        static String createMessage(Throwable cause) {
            String causeMsg = cause.getMessage();
            if (causeMsg == null) {
                causeMsg = cause.toString();
            }
            return causeMsg;
        }
    }
}

