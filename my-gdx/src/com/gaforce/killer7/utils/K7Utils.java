package com.gaforce.killer7.utils;

import java.io.File;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * 
 * <p>
 * Some useful smart tools.
 * </p>
 * 
 * @author mingming
 *
 */
public class K7Utils {
	
	//public final static int ASSET_LOAD_DEFAULT = 100;
	public final static String TAG = "K7";
	public final static String TAG_SELF = "K7.Utils: ";
	
	private final static float EQ_FLOAT = 0.0000000001f;
	
	
	/**
	 * Wrapper of {@link Gdx.app#log(String)}. 
	 * Unified the log output filter.
	 * 
	 * @param message: log message.
	 */
	public final static void log(String message) {
		Gdx.app.log(TAG, message);
	}
	
	/**
	 * Wrapper of {@link Gdx.app#log(String, Exception)}. 
	 * Unified the log output filter.
	 * 
	 * @param message: log message.
	 * @param exception: object of {@link Exception}.
	 */
	public final static void log(String message, Exception exception) {
		Gdx.app.log(TAG, message, exception);
	}
	
	/**
	 * Copy {@link Rectangle} data to another.
	 * 
	 * @param src: source rect.
	 * @param dst: destination rect.
	 */
	public final static void copyRectangle(Rectangle src, Rectangle dst) {
		if (null == src || null == dst) {
			log(TAG_SELF + "rectCopy: " + "parameter nul !");
			return;
		}
		
		dst.x = src.x;
		dst.y = src.y;
		dst.width = src.width;
		dst.height = src.height;
	}
	
	/**
	 * Add src {@link Rectangle} to dst. 
	 * 
	 * @param src: src of {@link Rectangle}.
	 * @param dst: dst of {@link Rectangle}.
	 */
	public final static void addRectangle(Rectangle src, Rectangle dst) {
		if (null == src || null == dst) {
			log(TAG_SELF + "rectCopy: " + "parameter nul !");
			return;
		}
		
		dst.x += src.x;
		dst.y += src.y;
		dst.width += src.width;
		dst.height += src.height;
	}
	
	/**
	 * Subtract src {@link Rectangle} on dst.
	 * 
	 * @param src: src of {@link Rectangle}.
	 * @param dst: dst of {@link Rectangle}.
	 */
	public final static void subRectangle(Rectangle src, Rectangle dst) {
		if (null == src || null == dst) {
			log(TAG_SELF + "rectCopy: " + "parameter nul !");
			return;
		}
		
		dst.x -= src.x;
		dst.y -= src.y;
		dst.width -= src.width;
		dst.height -= src.height;
	}
	
	/**
	 * Compare two float whether equal.
	 * 
	 * @param f1: float1
	 * @param f2: float2
	 * @return equal for true or false.
	 */
	public final static boolean floatEqual(float f1, float f2) {
		if (Math.abs(f1 - f2) <= EQ_FLOAT) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Parse resource configure file.
	 * 
	 * @param resPath: resource file path.
	 * @param format: configure file type (current it's "json").
	 * @return specified resource configure file path, if error it will
	 * 	return original resource file path. 
	 */
	public final static String parseResCfg(String resPath, String format) {
		if (null == resPath || null == format) {
			return resPath;
		}
		
		int index = resPath.lastIndexOf('.');
		if (-1 == index) {
			return resPath;
		}
		
		String path;
		try {
			path = resPath.replaceFirst(
					resPath.substring(index), 
					format);
		} catch (Exception e) {
			log(TAG_SELF, e);
			path = resPath;
		}
		
		return path;
	}
	
	/**
	 * An safely wrapper of {@link AssetManager#get(String, Class)}.
	 * 
	 * @param <T>: AssetManager get parameter.
	 * @param assetMgr: AssetManager object.
	 * @param fileName: AssetManager get parameter.
	 * @param type: AssetManager get parameter.
	 * @param autoLoad: if set true, it will auto load when AssetManager don't 
	 * 	load it.
	 * @return success return resource, or null
	 */
	public final static <T> T getAssetSafely(AssetManager assetMgr, 
			String fileName, Class<T> type, boolean autoLoad, boolean wait) {
		if (null == assetMgr) {
			return null;
		}
		
		try {
			// get directly first.
			return assetMgr.get(fileName, type);
			
		} catch (GdxRuntimeException runtimeException) {
			// the resource don't complete load in AssetManager.
			if (autoLoad) {
				// if auto load, complete load it.
				// note: don't call load() again, it will add references.
				//assetMgr.load(fileName, type);
				if (wait) {
					while(!assetMgr.update()) {;}
				} else {
					if (!assetMgr.update()) {
						return null;
					}
				}
				
				try {
					return assetMgr.get(fileName, type);
				} catch (Exception e) {
					log(TAG_SELF, e);
					return null;
				}
				
			} else {
				return null;
			}
			
		} catch (Exception e) {	
			log(TAG_SELF, e);
			return null;
		}
	}
	
	/**
	 * An safely wrapper of {@link Group#addActor(Actor)}.
	 * It will check actor whether already add to group.
	 * 
	 * @param group: object of {@link Group}.
	 * @param actor: object of {@link Actor}.
	 */
	public final static void addActorSafely(Group group, Actor actor) {
		if (null == group || null == actor) {
			return;
		}
		
		if (null == actor.name || null == group.findActor(actor.name)) {
			group.addActor(actor);
		}
	}
	
	/**
	 * Bring {@link Actor} to {@link Stage} children list last position.
	 * This position is render last, so it will show the top of the game screen.
	 * Notice: this function will change actor children position recursive.
	 * 
	 * @param stage: {@link Stage} who contain all {@link Actor}.
	 * @param actor: {@link Actor} which want to raise.
	 * @return true means success, or false.
	 */
	public final static boolean raiseActorToTop(Stage stage, Actor actor) {
		if (null == actor || null == stage) {
			return false;
		}
		
		Group parent = actor.parent;
		if (null == parent) {
			return false;
		}
		
		if (!parent.equals(stage.getRoot())) {
			raiseActorToTop(stage, parent);
		}
		
		actor.remove();
		addActorSafely(parent, actor);
		
		return true;
	}
	
	/**
	 * Check {@link Actor} whether in {@link Stage} children list last position.
	 * Notice: this function will check actor children position recursive.
	 * 
	 * @param stage: {@link Stage} who contain all {@link Actor}.
	 * @param actor: {@link Actor} which want to raise.
	 * @return true means actor already top, or false.
	 */
	public static boolean checkActorIsTop(Stage stage, Actor actor) {
		if (null == actor || null == stage) {
			return false;
		}
		
		Group parent = actor.parent;
		if (null == parent) {
			return false;
		}
		
		while (!parent.equals(stage.getRoot())) {
			if (!checkActorIsTop(stage, parent)) {
				return false;
			}
		}
		
		List<Actor> actors = parent.getActors();
		if (actors.size() > 0) {
			Actor last = actors.get(actors.size() - 1);
			if (last.equals(actor)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Check specified file, if don't exist will create it (include parent directory).
	 * 
	 * @param file: object of{@link FileHandle}.
	 * @return true means success, or false.
	 */
	public static boolean createFileAuto(FileHandle file) {
		if (null == file) {
			return false;
		}
		
		if (FileType.External != file.type()) {
			return false;
		}
		
		if (file.exists()) {
			return true;
		}
		
		File naviteFile = file.file();
        File parent = naviteFile.getParentFile();
        
        if (null != parent && !parent.exists()) { 
        	try {
        		if (parent.mkdirs()) {
        			log(TAG_SELF + "createFileAuto: " + "create parent file failed !");
        			return false;
            	}
        	} catch (Exception e) {
        		log(TAG_SELF, e);
            	return false;
        	}
        }  
		
        try {
        	return naviteFile.createNewFile();
        } catch (Exception e) {
        	log(TAG_SELF, e);
        	return false;
        }
	}
	
	/**
	 * Libgdx coordinate origin is (left, bottom).
	 * we want cover it to (left, top). we table is use (left, top).
	 * 
	 // 
	 // /|\
	 //  |
	 //  |
	 //  | 
	 //  |---------------->
	 *  (0,0)
	 *      
	 *      ||
	 *      ||
	 *      ||
	 *      ||
	 *     \||/
	 *      \/
	 *  
	 *  (0,0)
	 *   |----------------->
	 *   |
	 *   |
	 *   |
	 *   |
	 *  \|/
	 *      
	 * @param x: origin coordinate.
	 * @return after covert coordinate.
	 */
	public final static float coordinateLeftBottomToLeftTopX(float x) {
		return x;
	}
	
	public final static float coordinateLeftBottomToLeftTopY(float y, float h) {
		return h - y;
	}
	
	public final static float coordinateLeftTopToLeftBottomX(float x) {
		return x;
	}
	
	public final static float coordinateLeftTopToLeftBottomY(float y, float h) {
		return h - y;
	}
	
}
