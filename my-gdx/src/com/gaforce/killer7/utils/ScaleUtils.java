package com.gaforce.killer7.utils;

import com.badlogic.gdx.math.Vector2;

public class ScaleUtils {
	
	public final static String TAG = "ScaleUtils: ";
	
	public final static int SCALE_FIX_XY = 0;
	public final static int SCALE_FIX_X = 1;
	public final static int SCALE_FIX_Y = 2;
	
	
	public final static boolean scale(int orgWidth, int orgHeight, 
			int dstWidth, int dstHeight, Vector2 result) {
		return scale(orgWidth, orgHeight, dstWidth, dstHeight, result, SCALE_FIX_XY);
	}
	
	public final static boolean scale(int orgWidth, int orgHeight, 
			int dstWidth, int dstHeight, Vector2 result, int mode) {
		if (null == result) {
			K7Utils.log(TAG + "scale: " + "result is null.");
			return false;
		}
		
		if (orgWidth <= 0 || orgHeight <= 0 || 
				dstWidth <= 0 || dstHeight <= 0) {
			K7Utils.log(TAG + "scale: " + "paramter invaild.");
			return false;
		}
		
		switch (mode) {
		case SCALE_FIX_X:
			scaleFixX(orgWidth, orgHeight, dstWidth, dstHeight, result);
			break;
			
		case SCALE_FIX_Y:
			scaleFixY(orgWidth, orgHeight, dstWidth, dstHeight, result);
			break;
			
		case SCALE_FIX_XY:
		default:
			scaleFixXY(orgWidth, orgHeight, dstWidth, dstHeight, result);
			break;
		}
		
		return true;
	}
	
	private final static void scaleFixXY(int orgWidth, int orgHeight, 
			int dstWidth, int dstHeight, Vector2 result) {
		float aspectOrg = (float)orgWidth / (float)orgHeight;
		float aspectDst = (float)dstWidth / (float)dstHeight;
		
		if (K7Utils.floatEqual(aspectOrg, aspectDst)) {
			result.x = dstWidth;
			result.y = dstHeight;
		} else if (aspectDst > aspectOrg) {
			result.y = dstHeight;
			result.x = dstHeight * aspectOrg;
		} else {
			result.x = dstWidth;
			result.y = dstWidth / aspectOrg;
		}
	}
	
	private final static void scaleFixX(int orgWidth, int orgHeight, 
			int dstWidth, int dstHeight, Vector2 result) {
		result.x = dstWidth;
		result.y = orgHeight;
	}
	
	private final static void scaleFixY(int orgWidth, int orgHeight, 
			int dstWidth, int dstHeight, Vector2 result) {
		result.x = orgWidth;
		result.y = dstWidth;
	}
}
