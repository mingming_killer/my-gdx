package com.gaforce.killer7.ui;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Align;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.TableLayout;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Json.Serializer;
import com.gaforce.killer7.resolver.ResolutionFileResolverSerializer;
import com.gaforce.killer7.resource.StringValues;
import com.gaforce.killer7.ui.ImageButton.ImageButtonStyle;
import com.gaforce.killer7.ui.ToggleButton.ToggleButtonStyle;
import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * Read, write and serialize {@link Json} format object.
 * </p>
 * 
 * @author mingming
 */
public class SerializerFactory {
	
	public final static String TAG = "K7.SerializerFactory: ";
	
	private final static String DEFAULT_NAMESPACE = "com.badlogic.gdx.scenes.scene2d.";
	private final static boolean DEBUG_LAYOUT = false;
	
	private static Json mJson = null;
	private static JsonReader mJsonReader = null;
	
	private static Skin mSkin = null;
	private static String mDefaultStyle = "default";
	
	private static StringValues mStringValues = null;
	private static AssetManager mAssetMgr = null;
	
	
	/**
	 * Initialize SerializerFacotry Base element.
	 * After call this initialize, you can serialize some Non-UI object,
	 * like some configure, strings etc.
	 */
	public static void initializeBaseElement() {
		mJson = new Json();
		mJson.setTypeName(null);
		mJson.setUsePrototypes(false);
			
		mJsonReader = new JsonReader();
		
		setSerializer(ResolutionFileResolver.class, 
				ResolutionFileResolverSerializer.getResolutionFileResolverSerializer());
	}
	
	/**
	 * Initialize SerializerFacotry UI element.
	 * Must call initializeBaseElement before.
	 * After call this initialize, you can serialize all support object.
	 * 
	 * @param skin: object of {@link Skin}
	 * @param defStyle: default skin style name.
	 * @param assetMgr: object of {@link AssetManager}, 
	 * 		serialize some object need asset resources.
	 * @param strings: object of {@link StringValues}, 
	 * 		serialize some object need multi-language string support.
	 */
	public static void initializeUIElement(Skin skin, String defStyle, 
			AssetManager assetMgr, StringValues strings) {
		mSkin = skin;
		mDefaultStyle = defStyle;
		mAssetMgr = assetMgr;
		mStringValues = strings;
		
		// register K7 internal Serializer.
		// TODO: if have new Serializer, please register before use.
		setSerializer(TextButton.class, getTextButtonSerializer());
		setSerializer(com.badlogic.gdx.scenes.scene2d.ui.ImageButton.class, getImageButtonSerializer());
		setSerializer(ToggleButton.class, getToggleButtonSerializer());
		setSerializer(Image.class, getImageSerializer());
		setSerializer(Label.class, getLabelSerializer());
		setSerializer(ImageButton.class, getK7ImageButtonSerializer());
	}
	
	/**
	 * Release SerializerFacotry hold resources.
	 * Note: after call this method you can't serialize any object,
	 * 		until call {@link #initializeBaseElement} and {@link #initializeUIElement} again.
	 */
	public static void destroy() {
		mJson = null;
		mJsonReader = null;
		mSkin = null;
		mAssetMgr = null;
		mStringValues = null;
	}
	
	/**
	 * Register your custom {@link Serializer} to SerializerFactory.
	 * If you implement any custom {@link Serializer}, remember call this method to
	 * let SerializerFactory support your object.
	 * 
	 * @param <T>: object type.
	 * @param type: object class type.
	 * @param serializer: object of {@link Serializer}.
	 */
	public static <T> void setSerializer(Class<T> type, Serializer<T> serializer) {
		if (null == mJson) {
			K7Utils.log(TAG + "setSerializer: " +
					"call initSerializer() first !!");
			return;
		}
		
		mJson.setSerializer(type, serializer);
	}
	
	/**
	 * Deploy UI layout to {@link TableLayout} though UI file.
	 * 
	 * @param layout: object of TableLayout
	 * @param file: UI file.
	 * @return true means success or false.
	 */
	public static boolean deploy(TableLayout layout, FileHandle file) {
		ObjectMap<String, Object> params = null;
		ArrayList<Actor> actors = null;
		
		if (null == layout || null == file) {
			K7Utils.log(TAG + "deploy: " + 
					"parameters null !");
			return false;
		}
		
		try {
			// get ui parameter from configure file.
			params = SerializerFactory.readObject(file);
			if (null == params) {
				throw new GdxRuntimeException("read ui file error !");
			}
			
			// create all actors who declare in UI file
			actors = serializeActors(params);
			
			if (null != actors) {
				String layoutDesc = null;
				
				// register actors to scene
				for (int i = 0; i < actors.size(); i++) {
					Actor actor = actors.get(i);
					try {
						layout.register(actor);
					} catch (Exception ignored) {
						
					}
				}
				
				// get layout description
				try {
					layoutDesc = getString(params, SerializerElementDef.LAYOUT, null);
				} catch (Exception e) {
					layoutDesc = null;
					throw e;
				}
				
				// parse layout description
				if (null != layoutDesc) {
					try {
						layout.parse(layoutDesc);
						//layout.layout();
						layout.invalidate();
					} catch (Exception e) {
						throw e;
					}
				}
			}
			
		} catch (Exception e) {
			K7Utils.log(TAG + "deploy: ", e);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Read JSON file object.
	 * Note: if the file is encrypt by {@link SecureUtils}, must decrypt!
	 * 
	 * @param file: JSON {@link FileHandle} object.
	 * @return success JSON {@link ObjectMap}, or null.
	 */
	@SuppressWarnings("unchecked")
	public static ObjectMap<String, Object> readObject(FileHandle file) {
		if (null == mJson) {
			K7Utils.log(TAG + "readObject: " +
					"call initSerializer() first !");
			return null;
		}
			
		if (null == file) {
			K7Utils.log(TAG + "readObject: " +
				"parameter null !");
			return null;
		}
		
		try {
			// use UTF-8 to support complex language like Chinese.
			Reader reader = new InputStreamReader(file.read(), "UTF-8");
			//Reader reader = new InputStreamReader(file.read());
			
			ObjectMap<String, Object> object = 
				(ObjectMap<String, Object>) mJsonReader.parse(reader);
			return object;
			
		} catch (Exception e) {
			K7Utils.log(TAG + "readObject: ", e);
			return null;
		}
	}
	
	/**
	 * Read {@link Serializable} object from file.
	 * Note: if the reader of the file is encrypt by {@link SecureUtils}, must decrypt!
	 * 
	 * @param <T>: object type.
	 * @param reader: 
	 * @param classType: 
	 * @return
	 */
	public static <T> T readSerializable(Reader reader, Class<T> classType) {
		if (null == mJson || null == mJsonReader || null == reader) {
			K7Utils.log(TAG + "readSerializable: " + 
					"paramter is null !");
			return null;
		}
		
		try {
			Object data = mJsonReader.parse(reader);
			if (null != data) {
				return (T) mJson.readValue(classType, data);
			} else {
				K7Utils.log(TAG + "readSerializable: " + 
						"parse parameter failed !");
				return null;
			}
			
		} catch (Exception e) {
			K7Utils.log(TAG + "readSerializable: ", e);
			return null;
		}
	}
	
	/**
	 * Read {@link Serializable} object form file.
	 * Note: if the file is encrypt by {@link SecureUtils}, must decrypt!
	 * 
	 * @param <T>: object type.
	 * @param file: 
	 * @param classType: 
	 * @return
	 */
	public static <T> T readSerializable(FileHandle file, Class<T> classType) {
		if (null == mJson || null == mJsonReader || 
				null == file || !file.exists()) {
			K7Utils.log(TAG + "readSerializable: " + 
					"paramter is null !");
			return null;
		}
		
		// use UTF-8 to support complex language like Chinese.
		Reader reader;
		try {
			reader = new InputStreamReader(file.read(), "UTF-8");
			//reader = file.reader();
		} catch (Exception e) {
			K7Utils.log(TAG, e);
			reader = file.reader();
		}
		
		return readSerializable(reader, classType);
	}
	
	/**
	 * Write {@link Serializable} object to file.
	 * 
	 * @param <T>: 
	 * @param object: 
	 * @param file: 
	 * @param classType: 
	 * @return
	 */
	public static <T> boolean writeSerializable(Object object, FileHandle file, Class<T> classType) {
		if (null == mJson || null == object || null == file) {
			K7Utils.log(TAG + "writeSerializable: " + 
					"paramter is null !");
			return false;
		}
		
		try {
			if (!K7Utils.createFileAuto(file)) {
				return false;
			}
			
			mJson.toJson(object, classType, file);
			return true;
		} catch (Exception e) {
			K7Utils.log(TAG + "writeSerializable: ", e);
			return false;
		}
	}
	
	/**
	 * Serialize many {@link Actor}.
	 * 
	 * @param data: 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<Actor> serializeActors(ObjectMap<String, Object> data) {
		if (null == data) {
			return null;
		}
		
		ArrayList<Actor> actors = new ArrayList<Actor> ();
		
		// get class name space.
		String defNamespace = (String) data.get(SerializerElementDef.NAMESPACE);
		if (defNamespace == null) {
			defNamespace = DEFAULT_NAMESPACE;
		}
		
		Array<Object> array = getObjectArray(data, SerializerElementDef.ACTORS);
		if (null == array) {
			return null;
		}
		
		// serialize actors form configure file.
		Actor actor = null;
		for (Object obj : array) {
			// allow strings in JSON for comments
			if (!(obj instanceof ObjectMap)) { 
				continue;
			}
			
			// instance actor in the json file and add it to Stage.
			ObjectMap<String, Object> params = (ObjectMap<String, Object>) obj;
			actor = serializeActor(params, defNamespace);
			
			if (null != actor) {
				actors.add(actor);
			}
		}
		
		return actors;
	}
	
	/**
	 * Serialize {@link Actor} though JSON {@link ObjectMap}.
	 * 
	 * @param params: object of ObjectMap 
	 * @param defNamespace: actor class default NameSpace.
	 * @return success return actor or null.
	 */
	public static Actor serializeActor(ObjectMap<String, Object> params, String defNamespace) {
		if (null == mJson) {
			K7Utils.log(TAG +  "call initSerializer() first !!");
			return null;
		}
		
		try {
			Class<?> classType = null;
					
			if (null == defNamespace) {
				defNamespace = DEFAULT_NAMESPACE;
			}
			
			if (params.containsKey(SerializerElementDef.CLASS)) {
				// if package not specified, use this package
				String className = (String)params.get(SerializerElementDef.CLASS);
				if (className.indexOf('.') == -1) {
					className = defNamespace + className;
				}
				classType = Class.forName(className);
			}

			return (Actor) mJson.readValue(classType, params);
			
		} catch (Exception e) {
			K7Utils.log(TAG + "serializerActor: ", e);
			return null;
		}
	}
	
	public static int getInteger(ObjectMap<String, Object> data, String key, int defValue) {
		try {
			return asInteger(data.get(key), defValue);
		} catch (Exception e) {
			K7Utils.log(TAG + "getInteger: ", e);
			return defValue;
		}
	}
	
	public static long getLong(ObjectMap<String, Object> data, String key, long defValue) {
		try {
			return asLong(data.get(key), defValue);
		} catch (Exception e) {
			K7Utils.log(TAG + "getLong: ", e);
			return defValue;
		}
	}
	
	public static float getFloat(ObjectMap<String, Object> data, String key, float defValue) {
		try {
			return asFloat(data.get(key), defValue);
		} catch (Exception e) {
			K7Utils.log(TAG + "getFloat: ", e);
			return defValue;
		}
	}
	
	public static double getDouble(ObjectMap<String, Object> data, String key, double defValue) {
		try {
			return asDouble(data.get(key), defValue);
		} catch (Exception e) {
			K7Utils.log(TAG + "getDouble: ", e);
			return defValue;
		}
	}
	
	public static boolean getBoolean(ObjectMap<String, Object> data, String key, boolean defValue) {
		try {
			return asBoolean(data.get(key), defValue);
		} catch (Exception e) {
			K7Utils.log(TAG + "getBoolean: ", e);
			return defValue;
		}
	}
	
	public static String getString(ObjectMap<String, Object> data, String key, String defValue) {
		try {
			String str = (String)data.get(key);
			if (null == str) {
				str = defValue;
			}
			return str;
			
		} catch (Exception e) {
			K7Utils.log(TAG + "getString: ", e);
			return defValue;
		}
	}
	
	public static Object getObject(ObjectMap<String, Object> data, String key) {
		try {
			return data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getObject: ", e);
			return null;
		}
	}
	
	public static Scaling getScaling(ObjectMap<String, Object> data, Scaling defValue) {
		return getScaling(data, SerializerElementDef.SCALE_TYPE, defValue);
	}
	
	public static Scaling getScaling(ObjectMap<String, Object> data, String key, Scaling defValue) {
		try {
			return asScaling(data.get(key), defValue);
		} catch (Exception e) {
			K7Utils.log(TAG + "getScaling: ", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Array<Integer> getIntegerArray(ObjectMap<String, Object> data, String key) {
		try {
			return (Array<Integer>) data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getIntegerArray: ", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Array<Long> getLongArray(ObjectMap<String, Object> data, String key) {
		try {
			return (Array<Long>) data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getLongArray: ", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Array<Float> getFloatArray(ObjectMap<String, Object> data, String key) {
		try {
			return (Array<Float>) data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getFloatArray: ", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Array<Double> geDoubleArray(ObjectMap<String, Object> data, String key) {
		try {
			return (Array<Double>) data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getLongArray: ", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Array<String> getStringArray(ObjectMap<String, Object> data, String key) {
		try {
			return (Array<String>) data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getLongArray: ", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Array<Character> getCharArray(ObjectMap<String, Object> data, String key) {
		try {
			return (Array<Character>) data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getLongArray: ", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Array<Object> getObjectArray(ObjectMap<String, Object> data, String key) {
		try {
			return (Array<Object>) data.get(key);
		} catch (Exception e) {
			K7Utils.log(TAG + "getObjectArray: ", e);
			return null;
		}
	}
	
	public static float toRadians(float degrees) {
		return (float)(Math.PI / 180) * degrees;
	}
	
	public static float asFloat(Object obj, float defValue) {
		if (obj instanceof String) { 
			return Float.parseFloat((String)obj);
		} else if (obj instanceof Number){
			return ((Number)obj).floatValue();
		}
		return defValue;
	}
	
	public static float asFloat(Object obj) {
		return asFloat(obj, 0f);
	}
	
	public static double asDouble(Object obj, double defValue) {
		if (obj instanceof String) { 
			return Double.parseDouble((String)obj);
		} else if (obj instanceof Number){
			return ((Number)obj).doubleValue();
		}
		return defValue;
	}
	
	public static double asDouble(Object obj) {
		return asDouble(obj, 0.0);
	}
	
	public static int asInteger(Object obj, int defValue) {
		if (obj instanceof String) { 
			return Integer.parseInt((String)obj);
		} else if (obj instanceof Number){
			return ((Number)obj).intValue();
		}
		return defValue;
	}
	
	public static int asInteger(Object obj) {
		return asInteger(obj, 0);
	}
	
	public static long asLong(Object obj, long defValue) {
		if (obj instanceof String) { 
			return Long.parseLong((String)obj);
		} else if (obj instanceof Number){
			return ((Number)obj).longValue();
		}
		return defValue;
	}
	
	public static long asLong(Object obj) {
		return asLong(obj, 0);
	}
	
	public static boolean asBoolean(Object obj, boolean defValue) {
		if (obj instanceof String) { 
			return Boolean.parseBoolean((String)obj);
		} else if (obj instanceof Boolean) {
			return (Boolean)obj;
		}
		return defValue;
	}
	
	public static boolean asBoolean(Object obj) {
		return asBoolean(obj, false);
	}
	
	public static Scaling asScaling(Object obj, Scaling defValue) {
		if (obj instanceof String) {
			if (SerializerElementDef.SCALE_TYPE_FIT.equals(obj)) {
				return Scaling.fit;
			} else if (SerializerElementDef.SCALE_TYPE_FILL.equals(obj)) {
				return Scaling.fill;
			} else if (SerializerElementDef.SCALE_TYPE_FILLX.equals(obj)) {
				return Scaling.fillX;
			} else if (SerializerElementDef.SCALE_TYPE_FILLY.equals(obj)) {
				return Scaling.fillY;
			} else if (SerializerElementDef.SCALE_TYPE_STRETCH.equals(obj)) {
				return Scaling.stretch;
			} else if (SerializerElementDef.SCALE_TYPE_STRETCHX.equals(obj)) {
				return Scaling.stretchX;
			} else if (SerializerElementDef.SCALE_TYPE_STRETCHY.equals(obj)) {
				return Scaling.stretchY;
			} else if (SerializerElementDef.SCALE_TYPE_NONE.equals(obj)) {
				return Scaling.none;
			}
		}
		
		return defValue;
	}
	
	public static void correctIntegerArrayFromFloatArray(Array<Integer> array) {
		if (null == array) {
			return;
		}
		
		for (int i = 0; i < array.size; i++) {
			array.set(i, ((Number)array.get(i)).intValue());
		}
	}
	
	public static String assemblyCharToString(Array<Character> chars, String defValue) {
		if (null == chars) {
			return defValue;
		}
		
		char[] charBuffer = null;
		if (chars.size <= 0) {
			return "";
		} else {
			charBuffer = new char[chars.size];
			for (int i = 0; i < chars.size; i++) {
				charBuffer[i] = chars.get(i);
			}
		}
		
		try {
			return new String(charBuffer);
		} catch (Exception e) {
			K7Utils.log(TAG, e);
			return defValue;
		}
	}
	
	public static String assemblyString(Array<String> buffer, String defValue) {
		if (null == buffer) {
			return defValue;
		}
		
		String str = "";
		if (buffer.size <= 0) {
			return str;
		} else {
			for (int i = 0; i < buffer.size; i++) {
				str += buffer.get(i);
			}
		}
		
		return str;
	}
	
	
	//***************************************************************
	// K7 internal Serializer (include libgdx and K7 actors).
	//***************************************************************
	
	/**
	 * Support {@link SerializerElementDef}:
	 * 	
	 * 	{@link SerializerElementDef#CLASS}: class name.
	 * 	
	 * 	{@link SerializerElementDef#ACTOR_NAME}: actor name.
	 * 	
	 * 	{@link SerializerElementDef#STYLE}: style of {@link TextButtonStyle}.
	 * 	
	 * 	{@link SerializerElementDef#TEXT}: text of {@link TextButton}.
	 */
	private static Serializer<TextButton> getTextButtonSerializer() {
		return new Serializer<TextButton>() {
			private final static String DEFAULT_NAME = "TextButtonDefault";
			
			@SuppressWarnings("rawtypes")
			@Override
			public void write(Json json, TextButton object, Class type) {
				// we don't support write to this serializable object.
				return;
			}
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public TextButton read(Json json, Object jsonData, Class knownType) {
				if (jsonData == null) {
					return null;
				}
				
				TextButton self = null;
				ObjectMap<String, Object> params = null;
				String name, text, styleName;
				TextButtonStyle style;
				
				try {
					params = (ObjectMap) jsonData;
				
					name = SerializerFactory.getString(params, 
							SerializerElementDef.ACTOR_NAME, DEFAULT_NAME);
					
					// configure parameters.
					text = SerializerFactory.getString(
							params, SerializerElementDef.TEXT, DEFAULT_NAME);
					
					if (null != mStringValues) {
						text = mStringValues.getReferenceString(text, text);
					}
					
					styleName = SerializerFactory.getString(params, 
							SerializerElementDef.STYLE, mDefaultStyle);
					style = mSkin.getStyle(styleName, TextButtonStyle.class);
					
					self = new TextButton(text, style, name);
					
				} catch (Exception e) {
					K7Utils.log(TAG + "TextButtonSerializer-read: ", e);
					return null;
				}
				
				// show the table-layout debug rectangle
				if (DEBUG_LAYOUT) {
					self.getTableLayout().parse("" + 
							"debug " 
					);
				}
				
				return self;
			}
			
		};
	}
	
	/**
	 * Support {@link SerializerElementDef}:
	 * 	
	 * 	{@link SerializerElementDef#CLASS}: class name.
	 * 	
	 * 	{@link SerializerElementDef#ACTOR_NAME}: actor name.
	 * 	
	 * 	{@link SerializerElementDef#TEXTURE}: texture for image.
	 * 	
	 * 	{@link SerializerElementDef#POSITION}: {@link TextureRegion} for {@link Texture}.
	 * 
	 * 	{@link SerializerElementDef#SCALE_TYPE}: image scaling type.
	 * 
	 *  {@link SerializerElementDef#ALIGN}: image align type.
	 */
	private static Serializer<Image> getImageSerializer() {
		return new Serializer<Image> () {
			
			private final static String DEFAULT_NAME = "ImageDefault";
			
			@SuppressWarnings("rawtypes")
			@Override
			public void write(Json json, Image object, Class knownType) {
				// we don't support write to this serializable object.
				return;
			}
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public Image read(Json json, Object jsonData, Class type) {
				if (jsonData == null) {
					return null;
				}
				
				Image self = null;
				String name, file;
				int align;
				Scaling scaling;
				Array<Float> pos;
				Texture texture;
				TextureRegion region;
				ObjectMap<String, Object> params = null;
				
				try {
					params = (ObjectMap) jsonData;
					
					name = SerializerFactory.getString(params, 
							SerializerElementDef.ACTOR_NAME, DEFAULT_NAME);
					
					// configure parameters.
					file = SerializerFactory.getString(params, 
							SerializerElementDef.TEXTURE, null);
				
					pos = SerializerFactory.getFloatArray(
							params, SerializerElementDef.POSITION);
					
					texture = K7Utils.getAssetSafely(mAssetMgr, file, Texture.class, true, true);
					texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
					
					float tmp = 0;
					tmp = pos.get(0);
					int x = (int)tmp;
					tmp = pos.get(1);
					int y = (int)tmp;
					tmp = pos.get(2);
					int w = (int)tmp;
					tmp = pos.get(3);
					int h = (int)tmp;
					region = new TextureRegion(texture, x, y, w, h);
					
					scaling = SerializerFactory.getScaling(params, Scaling.fit);
					
					align = SerializerFactory.getInteger(params, 
							SerializerElementDef.ALIGN, Align.CENTER);
					
					self = new Image(region, scaling, align, name);
					
				} catch (Exception e) {
					K7Utils.log(TAG + "ImageSerializer-read: ", e);
					return null;
				}
				
				return self;
			}
			
		};
	}
	
	private static Serializer<Label> getLabelSerializer() {
		return new Serializer<Label> () {
			
			private final static String DEFAULT_NAME = "LabelDefault";

			@SuppressWarnings("rawtypes")
			@Override
			public void write(Json json, Label object, Class knownType) {
				// we don't support write to this serializable object.
				return;
			}

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public Label read(Json json, Object jsonData, Class type) {
				if (jsonData == null) {
					return null;
				}
				
				Label self = null;
				String name, text, styleName;
				ObjectMap<String, Object> params = null;
				LabelStyle style;
				
				try {
					params = (ObjectMap) jsonData;
					
					name = SerializerFactory.getString(params, 
							SerializerElementDef.ACTOR_NAME, DEFAULT_NAME);
					
					// configure parameters.
					text = SerializerFactory.getString(
							params, SerializerElementDef.TEXT, DEFAULT_NAME);
					
					if (null != mStringValues) {
						text = mStringValues.getReferenceString(text, text);
					}
					
					styleName = SerializerFactory.getString(params, 
							SerializerElementDef.STYLE, mDefaultStyle);
					style = mSkin.getStyle(styleName, LabelStyle.class);
					
					self = new Label(text, style, name);
							//mSkin.getStyle(mDefaultStyle, LabelStyle.class), 
							//name);
					
				} catch (Exception e) {
					K7Utils.log(TAG + "LabelSerializer-read: ", e);
					return null;
				}
				
				return self;
			}
			
		};
	}
	
	private static Serializer<com.badlogic.gdx.scenes.scene2d.ui.ImageButton> getImageButtonSerializer() {
		return new Serializer<com.badlogic.gdx.scenes.scene2d.ui.ImageButton>() {
			
			private final static String DEFAULT_NAME = "ImageButtonDefault";
			
			@SuppressWarnings("rawtypes")
			@Override
			public void write(Json json, com.badlogic.gdx.scenes.scene2d.ui.ImageButton button, Class type) {
				// we don't support write to this serializable object.
				return;
			}
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public com.badlogic.gdx.scenes.scene2d.ui.ImageButton read(Json json, Object jsonData, Class knownType) {
				if (jsonData == null) {
					return null;
				}
				
				com.badlogic.gdx.scenes.scene2d.ui.ImageButton self = null;
				String name, styleName;
				ObjectMap<String, Object> params = null;
				com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle style;
				
				try {
					params = (ObjectMap) jsonData;
					
					name = SerializerFactory.getString(params, 
							SerializerElementDef.ACTOR_NAME, DEFAULT_NAME);
					
					styleName = SerializerFactory.getString(params, 
							SerializerElementDef.STYLE, mDefaultStyle);
					style = mSkin.getStyle(styleName, com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle.class);
					
					self = new com.badlogic.gdx.scenes.scene2d.ui.ImageButton(style, name);
					
					Image image = self.getImage();
					
					if (null != image) {
						Scaling scaling = SerializerFactory.getScaling(params, Scaling.fit);
						int align = SerializerFactory.getInteger(params, 
								SerializerElementDef.ALIGN, Align.CENTER);
						
						image.setScaling(scaling);
						image.setAlign(align);
					}
					
				} catch (Exception e) {
					K7Utils.log(TAG + "ImageButtonSerializer-read: ", e);
					return null;
				}
				
				// show the table-layout debug rectangle
				if (DEBUG_LAYOUT) {
					self.getTableLayout().parse("" + 
							"debug " 
					);
				}
				
				return self;
			}
			
		};
	}
	
	private static Serializer<ToggleButton> getToggleButtonSerializer() {
		return new Serializer<ToggleButton> () {
			
			private final static String DEFAULT_NAME = "ToggleButtonDefault";

			@SuppressWarnings("rawtypes")
			@Override
			public void write(Json json, ToggleButton object, Class knownType) {
				// we don't support write to this serializable object.
				return;
			}

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public ToggleButton read(Json json, Object jsonData, Class type) {
				if (jsonData == null) {
					return null;
				}
				
				ToggleButton self = null;
				String name, styleName;
				ObjectMap<String, Object> params = null;
				ToggleButtonStyle style;
				
				try {
					params = (ObjectMap) jsonData;
					
					name = SerializerFactory.getString(params, 
							SerializerElementDef.ACTOR_NAME, DEFAULT_NAME);
					
					// configure parameters.
					styleName = SerializerFactory.getString(params, 
							SerializerElementDef.STYLE, mDefaultStyle);
					style = mSkin.getStyle(styleName, ToggleButtonStyle.class);
					
					self = new ToggleButton(style, name); 
							//mSkin.getStyle(mDefaultStyle, ToggleButtonStyle.class), 
							//name);
					
				} catch (Exception e) {
					K7Utils.log(TAG + "ToggleButtonSerializer-read: ", e);
					return null;
				}
				
				return self;
			}
			
		};
	}
	
	private static Serializer<ImageButton> getK7ImageButtonSerializer() {
		return new Serializer<ImageButton>() {
			
			private final static String DEFAULT_NAME = "ImgButtonDefault";
			
			@SuppressWarnings("rawtypes")
			@Override
			public void write(Json json, ImageButton button, Class type) {
				// we don't support write to this serializable object.
				return;
			}
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public ImageButton read(Json json, Object jsonData, Class knownType) {
				if (jsonData == null) {
					return null;
				}
				
				ImageButton self = null;
				String name, styleName;
				ObjectMap<String, Object> params = null;
				ImageButtonStyle style;
				
				try {
					params = (ObjectMap) jsonData;
					
					name = SerializerFactory.getString(params, 
							SerializerElementDef.ACTOR_NAME, DEFAULT_NAME);
					
					styleName = SerializerFactory.getString(params, 
							SerializerElementDef.STYLE, mDefaultStyle);
					style = mSkin.getStyle(styleName, ImageButtonStyle.class);
					
					self = new ImageButton(style, name);
							//mSkin.getStyle(mDefaultStyle, ImageButtonStyle.class), 
							//name);
					
					Image image = self.getImage();
					
					if (null != image) {
						Scaling scaling = SerializerFactory.getScaling(params, Scaling.fit);
						int align = SerializerFactory.getInteger(params, 
								SerializerElementDef.ALIGN, Align.CENTER);
						
						image.setScaling(scaling);
						image.setAlign(align);
					}
					
				} catch (Exception e) {
					K7Utils.log(TAG + "K7ImageButtonSerializer-read: ", e);
					return null;
				}
				
				// show the table-layout debug rectangle
				if (DEBUG_LAYOUT) {
					self.getTableLayout().parse("" + 
							"debug " 
					);
				}
				
				return self;
			}
			
		};
	}
	
	//***************************************************************
	//***************************************************************
	//***************************************************************
	
}
