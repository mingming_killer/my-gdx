package com.gaforce.killer7.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.gaforce.killer7.resource.Cyclable;

/**
 * 
 * <p>
 * This actor can make a alpha mask on screen.
 * </p>
 * 
 * @author mingming
 *
 */
public class ScreenMask extends Widget 
	implements Cyclable {
	
	private float mAlpha;
	private Color mColor;
	private Texture mTexture;
	
	
	// we forbid no name actor
	@SuppressWarnings("unused")
	private ScreenMask() {
		super();
	}
	
	public ScreenMask(String name) {
		this(Color.BLACK, name);
	}
	
	public ScreenMask(Color color, String name) {
		super(name);
		
		init(color);
	}
	
	private void init(Color color) {
		mColor = new Color();
		setColor(color.r, color.g, color.b);
		setFillParent(true);
	}
	
	public float getAlpha() {
		return mAlpha;
	}
	
	/**
	 * Change mask color, it will cost re-create texture and bind.
	 * So don't call it frequently.
	 * @param color
	 */
	public void setColor(float r, float g, float b) {
		mColor.set(r, g, b, 1);
		
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(r, g, b, 1);
		pixmap.fill();
		
		// release old texture first.
		if (null != mTexture) {
			mTexture.dispose();
		}
		
		// create a new texture.
		mTexture = new Texture(pixmap, false);
		//mTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		pixmap.dispose();
	}
	
	public void setAlpha(float alpha) {
		mAlpha = alpha;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		if (null == mTexture) {
			return;
		}
		
		Color tmp = batch.getColor();
		batch.setColor(tmp.r, tmp.g, tmp.b, mAlpha);
		batch.draw(mTexture, this.x, this.y, this.width, this.height, 
				0, 0, mTexture.getWidth(), mTexture.getHeight(), false, false);
		batch.setColor(tmp);
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		if (null != mTexture) {
			mTexture.dispose();
			mTexture = null;
		}
	}

	@Override
	public void resume() {
		setColor(mColor.r, mColor.g, mColor.b);
	}
	
	@Override
	public void destroy() {
		if (null != mTexture) {
			mTexture.dispose();
			mTexture = null;
		}
	}
	
}
