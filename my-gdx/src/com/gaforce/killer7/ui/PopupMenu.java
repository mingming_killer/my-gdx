package com.gaforce.killer7.ui;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;

/**
 * 
 * <p>
 * Modal Dialog.
 * </p>
 * 
 * @author mingming
 *
 */
public class PopupMenu {
	
	private final static int DEFAULT_WIDTH = 400;
	private final static int DEFAULT_HEIGHT = 300;
	
	private Window mWin = null;
	
	
	public PopupMenu(String title, FileHandle uiFile, WindowStyle winStyle, String name) {
		this(title, uiFile, winStyle, 
				0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT, name);
	}
	
	public PopupMenu(String title, FileHandle uiFile, WindowStyle winStyle, 
			int x, int y, int width, int height, String name) {
		if (null == uiFile || !uiFile.exists()) {
			return;
		}
		
		// create dialog.
		mWin = new Window(title, winStyle, name);
		
		// deploy dialog.
		if (null != mWin) {
			mWin.x = x;
			mWin.y = y;
			mWin.width = width;
			mWin.height = height;
			
			mWin.setModal(true);
			mWin.setMovable(false);
			
			SerializerFactory.deploy(mWin.getTableLayout(), uiFile);
		}
	}
	
	public void destroy() {
		if (null != mWin) {
			mWin.clearActions();
			mWin.clear();
			mWin.remove();
			mWin = null;
		}
	}
	
	public void removeMenu() {
		if (null != mWin) {
			mWin.clearActions();
			mWin.remove();
		}
	}
	
	public Window getWindow() {
		return mWin;
	}
	
	public void refreshUI(boolean hierarchy) {
		if (null == mWin) {
			return;
		}
		
		if (hierarchy) {
			mWin.invalidateHierarchy();
		} else {
			mWin.invalidate();
		}
	}
	
	public void setPosition(int x, int y) {
		if (null == mWin) {
			return;
		}
		
		setPosition(x, y, (int)mWin.width, (int)mWin.height);
	}
	
	public void setPosition(int x, int y, int w, int h) {
		if (null == mWin) {
			return;
		}
		
		// set position.
		mWin.x = x;
		mWin.y = y;
			
		if (w != mWin.width || h != mWin.height) {
			mWin.width = w;
			mWin.height = h;
			
			// change width and height must re-layout.
			refreshUI(false);
		}
	}
	
	public void setTitle(String title) {
		if (null == mWin || null == title) {
			return;
		}
		
		if (!title.equals(mWin.getTitle())) {
			mWin.setTitle(title);
			refreshUI(false);
		}
	}
	
	public void setAction(Action action) {
		if (null != mWin && null != action) {
			mWin.clearActions();
			mWin.action(action);
		}
	}
	
	public Actor findActor(String name) {
		if (null == name || null == mWin) {
			return null;
		}
		
		return mWin.findActor(name);
	}
	
}
