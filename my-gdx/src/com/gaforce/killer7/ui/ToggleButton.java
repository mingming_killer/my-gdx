package com.gaforce.killer7.ui;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.scenes.scene2d.actions.MoveTo;
import com.badlogic.gdx.scenes.scene2d.ui.Align;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Scaling;
import com.gaforce.killer7.interpolators.SystemInterpolator;

/**
 * 
 * <p>
 * Toggle Button, have on, off two swtich.
 * </p>
 * 
 * @author mingming
 *
 */
public class ToggleButton extends WidgetGroup {
	
	public final static String NAME_TOGGLE = "Toggle";
	
	protected final static float DEFAULT_DURATION = 0.5f;
	protected final static String DEFAULT_INTERPOLATOR_FACTOR = SystemInterpolator.Exp10Out;
	
	protected AnimationAction mAction = null;
	protected float mDuration;
	protected Interpolator mInterpolator;
	
	protected boolean mIsToggle;
	protected Image mActorToggle;
	protected ToggleButtonStyle mStyle;
	
	protected ClickListener mListener;
	
	
	public ToggleButton(String name) {
		this(null, name);
	}
	
	public ToggleButton(ToggleButtonStyle style, String name) {
		this(style, 
				DEFAULT_DURATION, SystemInterpolator.$(DEFAULT_INTERPOLATOR_FACTOR), 
				name);
	}
	
	public ToggleButton(ToggleButtonStyle style, 
			float duration, Interpolator interpolator, 
			String name) {
		super(name);
		
		mIsToggle = false;
		mStyle = style;
		
		mActorToggle = new Image((NinePatch)null, 
				Scaling.stretch, Align.CENTER, 
				name + NAME_TOGGLE);
		this.addActor(mActorToggle);
		
		mDuration = duration;
		mInterpolator = interpolator;
		
		mListener = null;
		
		toggle(false);
	}
	
	public void toggle(boolean on) {
		mIsToggle = on;
		
		if (null == mActorToggle || null == mStyle) {
			return;
		}
		
		// first clear old animation action.
		mActorToggle.clearActions();
		
		if (on) {
			mActorToggle.setPatch(mStyle.mImgOn);
			
			// right is on
			mAction = MoveTo.$(0 + (this.width - mActorToggle.width), 
					mActorToggle.y, mDuration);
			if (null != mInterpolator) {
				mAction.setInterpolator(mInterpolator);
			}
			mActorToggle.action(mAction);
		} else {
			mActorToggle.setPatch(mStyle.mImgOff);
			
			// left is off
			mAction = MoveTo.$(0, mActorToggle.y, mDuration);
			if (null != mInterpolator) {
				mAction.setInterpolator(mInterpolator);
			}
			mActorToggle.action(mAction);
		}
	}
	
	public void setStyle(ToggleButtonStyle style) {
		if (null == style || style.equals(mStyle)) {
			return;
		}
		
		mStyle = style;
		refreshUI(false);
	}
	
	public void setClickListener(ClickListener listener) {
		mListener = listener;
	}
	
	public void setAnimationProperty(float duration, Interpolator interpolator) {
		mDuration = duration;
		mInterpolator = interpolator;
	}
	
	public void setToggle(boolean on) {		
		if (null == mActorToggle || null == mStyle) {
			return;
		}
		
		// first clear old animation action.
		mActorToggle.clearActions();
		
		if (on) {
			mActorToggle.setPatch(mStyle.mImgOn);
		} else {
			mActorToggle.setPatch(mStyle.mImgOff);
		}
		
		mIsToggle = on;
		this.invalidate();
	}
	
	public boolean isToggle() {
		return mIsToggle;
	}
	
	public float getAnimationDuration() {
		return mDuration;
	}
	
	public Interpolator getAnimationInterpolator() {
		return mInterpolator;
	}
	
	public void refreshUI(boolean hierarchy) {
		if (hierarchy) {
			this.invalidateHierarchy();
		} else {
			this.invalidate();
		}
	}
	
	@Override
	public float getMinWidth() {
		if (null != mStyle && null != mStyle.mImgBk) {
			return mStyle.mImgBk.getTotalWidth();
		} else {
			return super.getMinWidth();
		}
	}
	
	@Override
	public float getMinHeight() {
		if (null != mStyle && null != mStyle.mImgBk) {
			return mStyle.mImgBk.getTotalHeight();
		} else {
			return super.getMinHeight();
		}
	}
	
	@Override
	public float getPrefWidth() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public float getPrefHeight() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override 
	public void layout() {
		NinePatch imgPatch = mActorToggle.getPatch();
		if (null == imgPatch || imgPatch.getTotalWidth() <= 0 || 
				imgPatch.getTotalHeight() <= 0) {
			return;
		}
		
		// we just support horizontal.
		float aspect = imgPatch.getTotalWidth() / imgPatch.getTotalHeight();
		float width = this.height * aspect;
		
		mActorToggle.width = width;
		mActorToggle.height = this.height;
		
		// notice: child actor is offset when draw.
		//mActorToggle.x = 0; //this.x;
		if (mIsToggle) {
			mActorToggle.x = 0 + (this.width - width);
		} else {
			mActorToggle.x = 0;
		}
		
		mActorToggle.y = 0; //this.y
		
		// let child actor layout.
		mActorToggle.layout();
	}
	
	@Override
	public boolean touchDown(float x, float y, int pointer) {
		boolean ret = super.touchDown(x, y, pointer);
		
		//Actor actor = this.hit(x, y);
		//if (null == actor || actor == this || 
		//		!(actor instanceof BaseItem)) {
		//	return false;
		//}
		
		// whole button receive touch event.
		mIsToggle = !mIsToggle;
		toggle(mIsToggle);
		
		if (null != mListener) {
			mListener.click(this, x, y);
		}
		
		return ret;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		// first let actor layout correct.
		this.validate();
		
		// draw background
		if (null != mStyle && null != mStyle.mImgBk) {
			mStyle.mImgBk.draw(batch, this.x, this.y, this.width, this.height);
		}
		
		// draw child(Image toggle).
		super.draw(batch, parentAlpha);
	}
	
	
	static class ToggleButtonStyle {
		// necessary
		public NinePatch mImgBk;
		public NinePatch mImgOn;
		public NinePatch mImgOff;
		
		public ToggleButtonStyle() {
			mImgBk = null;
			mImgOn = null;
			mImgOff = null;
		}
		
		public ToggleButtonStyle(NinePatch background, NinePatch on, NinePatch off) {
			mImgBk = background;
			mImgOn = on;
			mImgOff = off;
		}
		
	}

}
