package com.gaforce.killer7.ui;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.scenes.scene2d.Layout;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveTo;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateInterpolator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.Table;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.TableLayout;
import com.badlogic.gdx.utils.ObjectMap;
import com.gaforce.killer7.main.MainLoop;
import com.gaforce.killer7.message.MsgDef;
import com.gaforce.killer7.resource.Cyclable;
import com.gaforce.killer7.statemachine.Event;
import com.gaforce.killer7.statemachine.State;
import com.gaforce.killer7.statemachine.StateMachine;
import com.gaforce.killer7.statemachine.Transition;
import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * Scene include some {@link Actor}. {@link Actor} employed(created) 
 * by it, act on it. Scene can configure by a {@link JSON} format file.
 * And scene is the minimum unit of {@link StateMachine}.
 * 
 * It include {{@link #enterAnimation()} and {{@link #leaveAnimation()}.
 * </p>
 * 
 * @author mingming
 * 
 */
public abstract class Scene extends State
	implements Layout, OnActionCompleted, Cyclable {
	
	public final static String TAG = "K7.Scene: ";
	
	public final static float SCENE_ENTER_DURATION = 0.3f;
	public final static float SCENE_LEAVE_DURATION = 0.3f;
	public final static float INTERPOLATOR_FACTOR = 1.0f;
	
	protected static int mDefaultWidth = 480;
	protected static int mDefaultHeight = 800;
	
	protected static Skin mDefaultSkin = null;
	
	protected boolean mIsCurrentAct = false;

	protected FileHandle mUIFile = null;
	protected String mLayoutDesc = null;
	
	protected Stage mStage;
	protected Table mTable;
	protected MainLoop mLoop = null;
	
	protected ArrayList<Actor> mAnimationActors;
	protected HashMap<String, Actor> mActors;
	protected HashMap<String, Rectangle> mActorRects;
	
	
	// you must provider a name to instance object
	@SuppressWarnings("unused")
	private Scene() {
		super();
		
		mStage = null;
		init();
	}
	
	@SuppressWarnings("unused")
	private Scene(Stage stage) {
		super();
		
		mStage = stage;
		init();
	}
	
	public Scene(Stage stage, String name, MainLoop loop) {
		super(name);
		
		mStage = stage;
		mLoop = loop;
		init();
	}
	
	@SuppressWarnings("unused")
	private Scene(Stage stage, StateMachine stateMachine) {
        super(stateMachine, "");
        
        mStage = stage;
        init();
    }
    
	@SuppressWarnings("unused")
	private Scene(Stage stage, State parentState) {
        super(parentState, "");
        
        mStage = stage;
        init();
    }
    
    public Scene(Stage stage, StateMachine stateMachine, String name) {
        super(stateMachine, name);
        
        mStage = stage;
        init();
    }
    
    public Scene(Stage stage, State parentState, String name) {
        super(parentState, name);
        
        mStage = stage;
        init();
    }
    
    public Scene(Stage stage, StateMachine stateMachine, 
    		FileHandle uiFile, String name, MainLoop loop) {
        super(stateMachine, name);
        
        mStage = stage;
        mLoop = loop;
        mUIFile = uiFile;
        init();
    }
    
    private void init() {
    	if (null == mDefaultSkin) {
    		throw new IllegalStateException("Scene Table-Layout need a default skin.");
    	}
    	
    	mTable = new Table(mDefaultSkin, new TableLayout(), getName());
		mAnimationActors = new ArrayList<Actor> ();
		mActors = new HashMap<String, Actor> ();
		mActorRects = new HashMap<String, Rectangle> ();
	}
    
    public static void setDefaultSkin(Skin skin) {
    	mDefaultSkin = skin;
    }
    
    public static void cleanDefaultSkin() {
    	mDefaultSkin = null;
    }
	
    /**
     * Get {@link Stage} object.
     * 
     * @return
     */
	public Stage getStage() {
		return mStage;
	}
	
	/**
	 * Set Scene size.
	 * 
	 * @param w: 
	 * @param h: 
	 */
	public void adjustSize(int width, int height) {
		mTable.width = width;
		mTable.height = height;
		
		invalidate();
	}
	
	/**
	 * Set Scene default size.
	 * 
	 * @param w: 
	 * @param h: 
	 */
	public static void setDefaultSize(int w, int h) {
		mDefaultWidth = w;
		mDefaultHeight = h;
	}
	
	/**
	 * Add has load scene actors to stage, 
	 * let show begin.
	 * 
	 * @return true mean success or false.
	 */
	public boolean beginAct() {
		if (null == mStage) {
			return false;
		}
		
		if (null != mTable.name) {
			if (null == mStage.findActor(mTable.name)) {
				mStage.addActor(mTable);
			}
		}
		
		return true;
	}
	
	/**
	 * When show done, will remove actors from stage.
	 */
	public void finishAct() {
		if (null == mStage) {
			return;
		}
		
		mStage.removeActor(mTable);
	}
	
	/**
	 * Override it to implement your scene change language function.
	 */
	public void reloadLang() {
		
	}
	
	@Override
	public void layout() {
		mTable.layout();
	}

	@Override
	public void invalidate() {
		mTable.invalidate();
	}

	@Override
	public float getMinWidth() {
		return mTable.getMinWidth();
	}

	@Override
	public float getMinHeight() {
		return mTable.getMinHeight();
	}

	@Override
	public float getPrefWidth() {
		return mTable.getPrefWidth();
	}

	@Override
	public float getPrefHeight() {
		return mTable.getPrefHeight();
	}
	
	@Override
	public float getMaxWidth() {
		return mTable.getMaxWidth();
	}
	
	@Override
	public float getMaxHeight() {
		return mTable.getMaxHeight();
	}
	
	@Override
	public void invalidateHierarchy() {
		mTable.invalidateHierarchy();
	}

	@Override
	public void validate() {
		mTable.validate();
	}

	@Override
	public void pack() {
		mTable.pack();
	}

	@Override
	public void setFillParent(boolean fillParent) {
		mTable.setFillParent(fillParent);
	}
	
	@Override
	public void completed(Action action) {
		Actor actor = action.getTarget();
		
		// do animation actors
		if (mAnimationActors.contains(actor)) {
			//if (null != actor && null != mStage) {
			//	mStage.removeActor(actor);
			//}
			
			mAnimationActors.remove(actor);
			
			// all leave animation is finish.
			if (mAnimationActors.size() <= 0) {
				finishAct();
				
				if (null != mLoop) {
					mLoop.sendEmptyMessage(MsgDef.MSG_FW_ANIMATION_FINISH);
				}
			}
		}
	}
	
	@Override
	public void create() {
		deployScene();
	}
	
	@Override
	public void pause() {
		saveScene();
		
		finishAct();
		cleanScene();
	}

	@Override
	public void resume() {
		restoreScene();
	}
	
	@Override
	public void destroy() {
		finishAct();
		cleanScene();
	}
	
	/*@Override
	public boolean handleMessage(Message msg) {
		boolean interrupted = false;
		
		switch (msg.what) {
        case MsgDef.MSG_FW_SCENE_RESUME:
        	beginAct();
    		interrupted = true;
        	break;
        	
        default:
        	break;
        }
		
		return interrupted;
	}*/
	
	@Override
	protected void onEnter(Event event, Transition transition) {
		mIsCurrentAct = true;
		
		invalidate();
    	beginAct();
    	
    	enterAnimation();
    }
	
	@Override
    protected void onLeave(Event event, Transition transition) {
		mIsCurrentAct = false;
		
    	if (false == leaveAnimation()) {
    		finishAct();
    		
    		if (null != mLoop) {
    			K7Utils.log(TAG + "onLeave: " + "Secne send fw animation finish");
    			mLoop.sendEmptyMessage(MsgDef.MSG_FW_ANIMATION_FINISH);
    		}
    	}
    }
	
	/**
	 * Deploy scene from UI  {@link JSON} format file (set by Constructor).
	 * It will create {@link Actor} defined in {@link JSON} format file.
	 * And parse {@link TableLayout} and layout it.
	 * 
	 * @return true means success, or false.
	 */
	protected boolean deployScene() {
		// if actors already in place, not deploy again.
		// because it's cost many times to do it.
		if (checkActors()) {
			invalidate();
			return true;
		}
		
		ObjectMap<String, Object> params = null;
		ArrayList<Actor> actors = null;
		
		// get ui parameter from configure file.
		params = SerializerFactory.readObject(mUIFile);
		if (null == params) {
			return false;
		}
		
		// create all actors who declare in UI file
		actors = SerializerFactory.serializeActors(params);
		if (null != actors) {
			for (Actor actor : actors) {
				if (null != actor && null != actor.name) {
					mActors.put(actor.name, actor);
				}
			}
			
			actors.clear();
			actors = null;
			
			TableLayout layout = mTable.getTableLayout();
			
			// set table size
			mTable.x = 0;
			mTable.y = 0;
			mTable.width = mDefaultWidth;
			mTable.height = mDefaultHeight;
			
			// register actors to scene
			for (Actor actor : mActors.values()) {
				try {
					layout.register(actor);
				} catch (Exception ignored) {
					
				}
			}
			
			// get layout description
			try {
				mLayoutDesc = SerializerFactory.getString(
						params, SerializerElementDef.LAYOUT, null);
			} catch (Exception e) {
				K7Utils.log(TAG, e);
				mLayoutDesc = null;
			}
			
			// parse layout description
			if (null != mLayoutDesc) {
				try {
					layout.parse(mLayoutDesc);
					//layout.layout();
					layout.invalidate();
				} catch (Exception e) {
					K7Utils.log(TAG, e);
				}
			}
			
			// save the every actor finally rectangle.
			saveAllActorsRect();
		}
		
		return true;
	}
	
	/**
	 * Enter scene animation. Default is left MoveTo.
	 * You can override it for your custom scene animation (event disable it).
	 */
	protected boolean enterAnimation() {
		if (!checkActors()) {
			return false;
		}
		
		Rectangle rect = null;
		AnimationAction action = null;
		Interpolator interpolator = AccelerateInterpolator.$(INTERPOLATOR_FACTOR);
		
		/*for (Actor actor : mActors.values()) {
			rect = getActorRect(actor);
			if (null == rect) {
				continue;
			}
			action = MoveTo.$(rect.x, rect.y, SCENE_ENTER_DURATION);
			action.setInterpolator(interpolator);
			actor.x = 0 - rect.width;
			actor.y = rect.y;
			actor.action(action);
		}*/
		
		rect = getActorRect(mTable);
		if (null != rect) {
			action = MoveTo.$(rect.x, rect.y, SCENE_ENTER_DURATION);
			action.setInterpolator(interpolator);
			mTable.x = 0 - rect.width;
			mTable.y = rect.y;
			mTable.action(action);
		}
		
		return true;
	}
	
	/**
	 * Leave scene animation. Default is left MoveTo.
	 * You can override it for your custom scene animation (event disable it).
	 */
	protected boolean leaveAnimation() {
		if (null == mStage || !checkActors()) {
			return false;
		}
		
		Rectangle rect = null;
		AnimationAction action = null;
		int w = (int) mStage.width();
		Interpolator interpolator = AccelerateInterpolator.$(INTERPOLATOR_FACTOR);
		
		/*for (Actor actor : mActors.values()) {
			rect = getActorRect(actor);
			if (null == rect) {
				continue;
			}
			action = MoveTo.$(w + 2, rect.y, SCENE_ENTER_DURATION);
			action.setInterpolator(interpolator);
			actor.action(action);
			action.setCompletionListener(this);
		}*/
		
		mAnimationActors.clear();
		
		rect = getActorRect(mTable);
		if (null != rect) {
			action = MoveTo.$(w + 2, rect.y, SCENE_ENTER_DURATION);
			action.setInterpolator(interpolator);
			mTable.action(action);
			action.setCompletionListener(this);
			
			mAnimationActors.add(mTable);
		}
		
		return true;
	}
	
	/**
	 * Check actors whether is zero.
	 * This is use for check scene whether deployed.
	 * 
	 * @return
	 */
	protected boolean checkActors() {
		if (mActors.size() > 0) {
			return true;
		}
		
		return false;
	}

	/**
	 * Clean all actors of scene. 
	 * When exit scene will call it.
	 * Override it for clean your custom resource.
	 */
	protected void cleanScene() {
		mTable.clear();
		mAnimationActors.clear();
		mActors.clear();
		mActorRects.clear();
	}
	
	/**
	 * Save scene information.
	 * When {{@link #pause()} will call it.
	 * Override it for save your custom information, but remember call super.
	 */
	protected void saveScene() {
		// save actor position where they are act. 
		// saveAllActorsRect();
	}
	
	/**
	 * Restore scene.
	 * When {{@link #resume()} will call it.
	 * Override it for restore your custom information, but remember call super.
	 */
	protected void restoreScene() {
		deployScene();
		
		// if this is scene is current show, we show let it re-act.
		if (mIsCurrentAct) {
			beginAct();
		}
	}
	
	/**
	 * Save actor position information.
	 * Because default animation is MoveTo, so we need save the actors position.
	 * 
	 * @param actor: 
	 * @return
	 */
	protected int saveActorRect(Actor actor) {
		if (null == mActorRects || 
				null == actor || null == actor.name) {
			return -1;
		}
		
		Rectangle rect = new Rectangle(actor.x, actor.y, 
				actor.width, actor.height);
		mActorRects.put(actor.name, rect);
		
		return mActorRects.size();
	}
	
	/**
	 * Get actor position.
	 * 
	 * @param actor: 
	 * @return
	 */
	protected Rectangle getActorRect(Actor actor) {
		if (null == mActorRects || 
				null == actor || null == actor.name) {
			return null;
		}
		
		return mActorRects.get(actor.name);
	}
	
	/**
	 * Save all actors position.
	 * 
	 * @return
	 */
	protected int saveAllActorsRect() {
		int ret = -1;
		for (Actor actor : mActors.values()) {
			ret = saveActorRect(actor);
		}
		
		ret = saveActorRect(mTable);
		
		return ret;
	}
	
}
