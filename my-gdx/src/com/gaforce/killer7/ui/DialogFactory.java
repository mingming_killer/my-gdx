package com.gaforce.killer7.ui;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.gaforce.killer7.ui.Dialog.DialogListener;

/**
 * 
 * <p>
 * Provide dialog to display.
 * </p>
 * 
 * @author mingming
 *
 */
public class DialogFactory {
	
	private static Dialog mDlgCurrent = null;
	private static Dialog mDlgPositiveNegative = null;
	
	
	public static void initialize(WindowStyle style) {
		if (null == style) {
			mDlgPositiveNegative = null;
			return;
		}
		
		mDlgPositiveNegative = new Dialog("", style, 
				Dialog.STYLE_POSITIVE | Dialog.STYLE_NEGATIVE, null);
	}
	
	public static void destroy() {
		if (null != mDlgPositiveNegative) {
			mDlgPositiveNegative.destroy();
			mDlgPositiveNegative = null;
		}
	}
	
	public static Dialog dialog(String title, String msg, String positive, String negative, 
			int x, int y, int w, int h, DialogListener listener) {
		return dialog(title, msg, positive, negative, x, y, w, h, listener, null);
	}
	
	public static Dialog dialog(String title, String msg, String positive, String negative, 
			int x, int y, int w, int h, DialogListener listener, Action action) {
		setStyle(Dialog.STYLE_POSITIVE | Dialog.STYLE_NEGATIVE);
		
		setTitle(title);
		
		setMsg(msg);
		
		setPositiveText(positive);
		
		setNegativeText(negative);
		
		setPosition(x, y, w, h);
		
		setListener(listener);
		
		if (null != action && null != mDlgCurrent) {
			Window win = mDlgCurrent.getWindow();
			win.clearActions();
			win.action(action);
		}
		
		return getDialog();
	}
	
	public static void setStyle(int style) {
		if (0 != (style & Dialog.STYLE_POSITIVE) && 
				0 != (style & Dialog.STYLE_NEGATIVE)) {
			mDlgCurrent = mDlgPositiveNegative;
		} else {
			// now we don't support neutral.
			mDlgCurrent = mDlgPositiveNegative;
		}
	}
	
	public static void setPosition(int x, int y, int w, int h) {
		if (null == mDlgCurrent) {
			return;
		}
		
		mDlgCurrent.setPosition(x, y, w, h);
	}
	
	public static void setListener(DialogListener listener) {
		if (null == mDlgCurrent) {
			return;
		}
		
		mDlgCurrent.setListener(listener);
	}
	
	public static void setTitle(String title) {
		if (null == mDlgCurrent) {
			return;
		}
		
		mDlgCurrent.setTitle(title);
	}
	
	public static void setMsg(String msg) {
		if (null == mDlgCurrent) {
			return;
		}
		
		mDlgCurrent.setMsg(msg);
	}
	
	public static void setPositiveText(String text) {
		if (null == mDlgCurrent) {
			return;
		}
		
		mDlgCurrent.setPositiveText(text);
	}
	
	public static void setNeutralText(String text) {
		if (null == mDlgCurrent) {
			return;
		}
		
		mDlgCurrent.setNeutralText(text);
	}
	
	public static void setNegativeText(String text) {
		if (null == mDlgCurrent) {
			return;
		}
		
		mDlgCurrent.setNegativeText(text);
	}
	
	public static Dialog getDialog() {
		return mDlgCurrent;
	}
	
}
