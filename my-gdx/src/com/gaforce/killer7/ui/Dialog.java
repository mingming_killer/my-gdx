package com.gaforce.killer7.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Align;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;

/**
 * 
 * <p>
 * Modal Dialog.
 * </p>
 * 
 * @author mingming
 *
 */
public class Dialog implements ClickListener {
	
	public final static int STYLE_POSITIVE = 1 << 0;
	public final static int STYLE_NEUTRAL = 1 << 1;
	public final static int STYLE_NEGATIVE = 1 << 2;
	
	public final static int ID_POSITIVE = 0;
	public final static int ID_NEUTRAL = 1;
	public final static int ID_NEGATIVE = 2;
	
	public final static String NAME_DLG = "Dialog";
	public final static String NAME_LABEL_MSG = "DlgLabelMsg";
	public final static String NAME_BTN_POSITIVE = "DlgBtnPositive";
	public final static String NAME_BTN_NEUTRAL = "DlgBtnNeutral";
	public final static String NAME_BTN_NEGATIVE = "DlgBtnNegative";
	
	private final static String POSITIVE_NEGATIVE_UI_FILE = 
		"com/gaforce/killer7/res/dlg_positive_negative.json";
	
	private final static String POSITIVE_NEUTRAL_NEGATIVE_UI_FILE = 
		//"com/gaforce/killer7/res/dlg_positive_neutral_negative.json";
		"com/gaforce/killer7/res/dlg_positive_negative.json";
	
	private final static int DEFAULT_WIDTH = 400;
	private final static int DEFAULT_HEIGHT = 300;
	
	private static int COUNT = 0;
	
	
	@SuppressWarnings("unused")
	private int mNo = 0;
	private int mStyle = 0;
	
	private Window mWin = null;
	private TextButton mBtnPositive = null;
	private TextButton mBtnNeutral = null;
	private TextButton mBtnNegative = null;
	
	private DialogListener mListener = null;
	
	
	public interface DialogListener {
		public void onDlgSelected(int ret);
	}
	
	
	public Dialog(String title, WindowStyle winStyle, int dlgStyle, 
			DialogListener listener) {
		this(title, winStyle, dlgStyle, listener, 
				0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
	
	public Dialog(String title, WindowStyle winStyle, int dlgStyle, 
			DialogListener listener, 
			int x, int y, int width, int height) {
		// create dialog.
		mWin = new Window(title, winStyle, NAME_DLG + COUNT);
		mNo = COUNT;
		
		// name of window not the same.
		COUNT++;
		
		mStyle = dlgStyle;
		mListener = listener;
		
		// deploy dialog.
		if (null != mWin) {
			mWin.x = x;
			mWin.y = y;
			mWin.width = width;
			mWin.height = height;
			
			mWin.setModal(true);
			mWin.setMovable(false);
			
			String uiFile = POSITIVE_NEGATIVE_UI_FILE;
			if (0 != (dlgStyle & STYLE_POSITIVE) && 
					0 != (dlgStyle & STYLE_NEGATIVE)) {
				uiFile = POSITIVE_NEGATIVE_UI_FILE;
			} else if (0 != (dlgStyle & STYLE_POSITIVE) && 
					0 != (dlgStyle & STYLE_NEGATIVE) && 
					0 != (dlgStyle & STYLE_NEUTRAL)) {
				uiFile = POSITIVE_NEUTRAL_NEGATIVE_UI_FILE;
			}
			
			if (SerializerFactory.deploy(mWin.getTableLayout(), 
					Gdx.files.classpath(uiFile))) {
				mBtnPositive = (TextButton) mWin.findActor(NAME_BTN_POSITIVE);
				mBtnNeutral = (TextButton) mWin.findActor(NAME_BTN_NEUTRAL);
				mBtnNegative = (TextButton) mWin.findActor(NAME_BTN_NEGATIVE);
				
				if (null != mBtnPositive) {
					mBtnPositive.setClickListener(this);
				}
				
				if (null != mBtnNeutral) {
					mBtnNeutral.setClickListener(this);
				}
				
				if (null != mBtnNegative) {
					mBtnNegative.setClickListener(this);
				}
			}
		}
	}
	
	public void destroy() {
		if (null != mWin) {
			mWin.clearActions();
			mWin.clear();
			mWin = null;
		}
	}
	
	public void dismiss() {
		if (null != mWin) {
			mWin.remove();
		}
	}
	
	public Window getWindow() {
		return mWin;
	}
	
	public void refreshUI(boolean hierarchy) {
		if (null == mWin) {
			return;
		}
		
		if (hierarchy) {
			mWin.invalidateHierarchy();
		} else {
			mWin.invalidate();
		}
	}
	
	public void setPosition(int x, int y) {
		if (null == mWin) {
			return;
		}
		
		setPosition(x, y, (int)mWin.width, (int)mWin.height);
	}
	
	public void setPosition(int x, int y, int w, int h) {
		if (null == mWin) {
			return;
		}
		
		// set position.
		mWin.x = x;
		mWin.y = y;
			
		if (w != mWin.width || h != mWin.height) {
			mWin.width = w;
			mWin.height = h;
			
			// change width and height must re-layout.
			refreshUI(false);
		}
	}
	
	public void setListener(DialogListener listener) {
		mListener = listener;
	}
	
	public void setTitle(String title) {
		if (null == mWin || null == title) {
			return;
		}
		
		if (!title.equals(mWin.getTitle())) {
			mWin.setTitle(title);
			refreshUI(false);
		}
	}
	
	public void setMsg(String msg) {
		if (null == mWin || null == msg) {
			return;
		}
		
		Label labelMsg = (Label) mWin.findActor(NAME_LABEL_MSG);
		if (null != labelMsg) {
			if (!msg.equals(labelMsg.getText())) {
				labelMsg.setAlignment(Align.CENTER);
				labelMsg.setText(msg);
			}
		}
	}
	
	public void setPositiveText(String text) {
		if (null == mWin) {
			return;
		}
		
		if (null != mBtnPositive) {
			if (!text.equals(mBtnPositive.getText())) {
				mBtnPositive.setText(text);
			}
		}
	}
	
	public void setNeutralText(String text) {
		if (null == mWin) {
			return;
		}
		
		if (null != mBtnNeutral) {
			if (!text.equals(mBtnNeutral.getText())) {
				mBtnNeutral.setText(text);
			}
		}
	}
	
	public void setNegativeText(String text) {
		if (null == mWin) {
			return;
		}
		
		if (null != mBtnNegative) {
			if (!text.equals(mBtnNegative.getText())) {
				mBtnNegative.setText(text);
			}
		}
	}
	
	public int getStyle() {
		return mStyle;
	}

	@Override
	public void click(Actor actor, float x, float y) {
		if (null == mWin) {
			return;
		}
		
		if (null != mBtnPositive && actor.equals(mBtnPositive)) {
			if (null != mListener) {
				mWin.remove();
				mListener.onDlgSelected(ID_POSITIVE);
			}
		} else if (null != mBtnNeutral && actor.equals(mBtnNeutral)) {
			if (null != mListener) {
				mWin.remove();
				mListener.onDlgSelected(ID_NEUTRAL);
			}
		} else if (null != mBtnNegative && actor.equals(mBtnNegative)) {
			if (null != mListener) {
				mWin.remove();
				mListener.onDlgSelected(ID_NEGATIVE);
			}
		}
	}
	
}
