package com.gaforce.killer7.ui;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Align;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Scaling;
import com.esotericsoftware.tablelayout.Cell;
import com.gaforce.killer7.utils.ScaleUtils;

/**
 * 
 * <p>
 * Base {@link ImageButton}, copy code.
 * I just want to keep the image aspect ratio.
 * So it's not my code style. -_-||
 * </p>
 * 
 * @author mingming
 *
 */
public class ImageButton extends Button {
	protected final Image image;
	protected ImageButtonStyle style;
	
	protected Vector2 rect;
	
	
	public ImageButton(Skin skin) {
		this(skin.getStyle("default", ImageButtonStyle.class));
	}

	public ImageButton(ImageButtonStyle style) {
		this(style, null);
	}

	public ImageButton(ImageButtonStyle style, String name) {
		super(style, name);
		
		image = new Image();
		image.setScaling(Scaling.fit);
		
		add(image);
		setStyle(style);
		
		width = getPrefWidth();
		height = getPrefHeight();
		
		rect = new Vector2(width, height);
	}

	public ImageButton(TextureRegion region) {
		this(new ImageButtonStyle(null, null, null, 0f, 0f, 0f, 0f, region, null, null));
	}

	public ImageButton(TextureRegion regionUp, TextureRegion regionDown) {
		this(new ImageButtonStyle(null, null, null, 0f, 0f, 0f, 0f, regionUp, regionDown, null));
	}

	public ImageButton(TextureRegion regionUp, TextureRegion regionDown, TextureRegion regionChecked) {
		this(new ImageButtonStyle(null, null, null, 0f, 0f, 0f, 0f, regionUp, regionDown, regionChecked));
	}

	public ImageButton(NinePatch patch) {
		this(new ImageButtonStyle(null, null, null, 0f, 0f, 0f, 0f, patch, null, null));
	}

	public ImageButton(NinePatch patchUp, NinePatch patchDown) {
		this(new ImageButtonStyle(null, null, null, 0f, 0f, 0f, 0f, patchUp, patchDown, null));
	}

	public ImageButton(NinePatch patchUp, NinePatch patchDown, NinePatch patchChecked) {
		this(new ImageButtonStyle(null, null, null, 0f, 0f, 0f, 0f, patchUp, patchDown, patchChecked));
	}

	public void setStyle(ButtonStyle style) {
		if (!(style instanceof ImageButtonStyle)) throw new IllegalArgumentException("style must be an ImgButtonStyle.");
		super.setStyle(style);
		this.style = (ImageButtonStyle)style;
		if (image != null) updateImage();
	}

	public ImageButtonStyle getStyle() {
		return style;
	}

	private void updateImage() {
		if (isPressed && style.regionDown != null)
			image.setRegion(style.regionDown);
		else if (isPressed && style.patchDown != null)
			image.setPatch(style.patchDown);
		else if (this.isChecked() && style.regionChecked != null)
			image.setRegion(style.regionChecked);
		else if (this.isChecked() && style.patchChecked != null)
			image.setPatch(style.patchChecked);
		else if (style.regionUp != null)
			image.setRegion(style.regionUp);
		else if (style.patchUp != null)
			image.setPatch(style.patchUp);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		updateImage();
		super.draw(batch, parentAlpha);
	}

	public Image getImage() {
		return image;
	}

	@SuppressWarnings("rawtypes")
	public Cell getImageCell() {
		return getCell(image);
	}
	
	@Override
	public boolean touchDown (float x, float y, int pointer) {
		if (hit(x, y) == image) {
			return super.touchDown(x, y, pointer);
		}
		
		return false;
	}
	
	@Override
	public void touchUp (float x, float y, int pointer) {		
		if (hit(x, y) == image) {
			super.touchUp(x, y, pointer);
		} else {
			isPressed = false;
		}
	}
	
	@Override
	public void layout() {
		if (null == style) {
			return;
		}
		
		if (null != style.patchUp && null != image && 
				0 != style.patchUp.getTotalWidth() && 0 != style.patchUp.getTotalHeight()) {
			// we use FIX_XY scale to keep aspect ratio.
			ScaleUtils.scale((int)style.patchUp.getTotalWidth(), (int)style.patchUp.getTotalHeight(), 
					(int)this.width, (int)this.height, rect);
			
			if ((int)image.width != (int)rect.x || (int)image.height != (int)rect.y) {			
				// image aspect ratio don't same, we adjust it.
				image.width = rect.x;
				image.height = rect.y;
				
				image.layout();
				
				// adjust after scale image position.
				if (0 != (style.align & Align.CENTER)) {
					image.x = 0 + (this.width - image.width) / 2;
					image.y = 0 + (this.height - image.height) / 2;
				} else {
					if (0 != (style.align & Align.LEFT)) {
						// do noting
					} else if (0 != (style.align & Align.RIGHT)) {
						image.x = this.width - image.width;
					}
				
					if (0 != (style.align & Align.BOTTOM)) {
						// do noting
					} else if (0 != (style.align & Align.TOP)) {
						image.y = this.height - image.height;
					}
				}
			}
		}
		
		//super.layout();
	}
	
	
	static public class ImageButtonStyle extends com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle {
		/** Optional. */
		public int align;
		
		public ImageButtonStyle() {
			super();
			
			align = Align.CENTER;
		}

		public ImageButtonStyle(NinePatch down, NinePatch up, NinePatch checked, float pressedOffsetX, float pressedOffsetY, 
				float unpressedOffsetX, float unpressedOffsetY, TextureRegion regionUp, TextureRegion regionDown,
			TextureRegion regionChecked) {
			super(down, up, checked, pressedOffsetX, pressedOffsetY, 
					unpressedOffsetX, unpressedOffsetY, 
					regionUp, regionDown, regionChecked);
			
			align = Align.CENTER;
		}
		
		public ImageButtonStyle(NinePatch down, NinePatch up, NinePatch checked, float pressedOffsetX, float pressedOffsetY, 
				float unpressedOffsetX, float unpressedOffsetY, NinePatch patchUp, NinePatch patchDown, NinePatch patchChecked) {
			super(down, up, checked, pressedOffsetX, pressedOffsetY, 
					unpressedOffsetX, unpressedOffsetY, 
					patchUp, patchDown, patchChecked);
			
			align = Align.CENTER;
		}
		
		public ImageButtonStyle(NinePatch down, NinePatch up, NinePatch checked, float pressedOffsetX, float pressedOffsetY,
				float unpressedOffsetX, float unpressedOffsetY, TextureRegion regionUp, TextureRegion regionDown,
				TextureRegion regionChecked, int align) {
			super(down, up, checked, pressedOffsetX, pressedOffsetY, 
					unpressedOffsetX, unpressedOffsetY, 
					regionUp, regionDown, regionChecked);
				
			this.align = align;
		}
	}
}
