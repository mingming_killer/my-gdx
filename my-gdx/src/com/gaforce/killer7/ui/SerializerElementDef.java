package com.gaforce.killer7.ui;

/**
 * 
 * <p>
 * The JSON file specified field defined.
 * K7 internal support JSON element.
 * </p>
 * 
 * @author mingming
 */
public class SerializerElementDef {
		
	/**
	 * Class name.
	 * 
	 * type: String(full class name[be sure you set default NameSpace], or short class name)
	 * defined: 
	 * 	"class": "com.badlogic.gdx.scenes.scene2d.Actor",
	 *  "class": "Actor",
	 */
	public final static String CLASS = "class";
	
	/**
	 * Class default NameSpace.
	 * 
	 * type: String
	 * defined: 
	 * 	"namespace": "com.badlogic.gdx.scenes.scene2d.ui"
	 */
	public final static String NAMESPACE = "namespace";
	
	/**
	 * Libgdx actors.
	 * It can set some {@link SerializerFactory} support elements.
	 * Like: {@link #ACTOR_NAME} of {@link Actor}, {@link #CLASS} of {@link Object},  
	 * 		{@link #TEXTURE} of {@link Image}, {@link #TEXT} of {@link Label} etc.
	 * 
	 * type: JSON array. 
	 * defined: 
	 * 	"actors": [
	 * 		"actor1",
	 * 		{
	 *      	"class": "Label",
	 *      	"text": "Label 1",
	 *      	"name": "Label1"
	 *      },
	 *       
	 *      "actor2",
	 *      {
	 *      	... ...
	 *      }
	 * 	],
	 */
	public final static String ACTORS = "actors";
	
	/**
	 * Table-layout description.
	 * Specification: {@link http://code.google.com/p/table-layout/}.
	 * 
	 * type: String, support multi-lines.
	 * defined:
	 * 	"layout": "
	 * 		w:100% h:100%
	 * 		
	 * 		* align: left
	 * 		
	 * 		[Actor1]
	 * 			w:20% h:40%
	 * 		
	 * 		---
	 * 		[Actor2]
	 * 			w:20% h:40%
	 * 	"
	 */
	public final static String LAYOUT = "layout";
	
	/**
	 * Scale resolver.
	 * 
	 * type: JSON array
	 * defined:
	 * 	"scale_resolver": [
	 *		"320x480",
	 *		{
	 *			"width": "320",
	 *			"height": "480",
	 *			"scale_factor": "1.0"
	 *		},
	 *	
	 *		"480x800",
	 *		{
	 *			"width": "480",
	 *			"height": "800",
	 *			"scale_factor": "1.5"
	 *		},
	 *	]
	 */
	public final static String SCALE_RESOLVER = "scale_resolver";
	
	/**
	 * Resolution resolver.
	 * 
	 * typed: JSON array.
	 * defined:
	 * 	"resolution_resolver": [
	 * 		"480x320",
	 * 		{
	 *			"width": "320",
	 * 			"height": "480",
	 * 			"suffix": "normal"
	 *		},
	 * 		
	 * 		"480x800",
	 * 		{
	 * 			"width": "480",
	 * 			"height": "800",
	 *			"suffix": "HD"
	 * 		},
	 * 	]
	 */
	public final static String RESOLUTION_RESOLVER = "resolution_resolver";
	
	/**
	 * Name of actor
	 * 
	 * type: String
	 * defined: 
	 * 	"name": "Actor1",
	 */
	public final static String ACTOR_NAME = "name";
	
	// now use yet.
	public final static String DEFAULT_NAME = "ActorDefault";
	
	/**
	 * Style name of skin. 
	 * 
	 * type: String
	 * defined:
	 * 	"style": "default",
	 */
	public final static String STYLE = "style";
	
	// click listener of actor, now use yet.
	public final static String CLICK = "onClick";
	
	/**
	 * Text information, like Label, TextButton etc.
	 * 
	 * type: String
	 * defined:
	 * 	"text": "Text",
	 */
	public final static String TEXT = "text";
	
	/**
	 * Position information
	 * 
	 * type: JSON array(4 Float[x, y, w, h])
	 * defined:
	 * 	"position": [0, 0, 400, 285],
	 */
	public final static String POSITION = "position";
	
	/**
	 * Width
	 * 
	 * type: Float or String
	 * defined:
	 * 	"width": "320",
	 * 	"width": 320,
	 */
	public final static String WIDTH = "width";
	
	/**
	 * Height
	 * 
	 * type: Float or String
	 * defined:
	 * 	"height": "480",
	 * 	"height": 480,
	 */
	public final static String HEIGTH = "height";
	
	/**
	 * Reference {@link Texture} path. Support path resolver.
	 * Can with position information(convert to {@link TextureRegion}) use.
	 * 
	 * type: String
	 * defined:
	 * 	"texture": "data/texture/ui/ui.png",
	 * 	"position": [0, 0, 480, 800],
	 */
	public final static String TEXTURE = "texture";
	
	/**
	 * Scale factor, use by {@link BitmapFont} scale etc.
	 * 
	 * type: Float or String
	 * defined:
	 * 	"scale_factor": "1.0"
	 * 	"scale_factor": 1.0
	 */
	public final static String SCALE_FACTOR = "scale_factor";
	
	/**
	 * Path suffix use by some resolver.
	 * 
	 * type: String
	 * defined:
	 * 	"suffix": "HD",
	 */
	public final static String SUFFIX = "suffix";
	
	/**
	 * Scale type. libgdx scale type, Specification see {@link Scaling}.
	 * Use by {@link Image} etc.
	 * 
	 * type: String
	 * defined:
	 * 	"scale_type": "fit".
	 */
	public final static String SCALE_TYPE = "scale_type";
	public final static String SCALE_TYPE_FIT = "fit";
	public final static String SCALE_TYPE_FILL = "fill";
	public final static String SCALE_TYPE_FILLX = "fillX";
	public final static String SCALE_TYPE_FILLY = "fillY";
	public final static String SCALE_TYPE_STRETCH = "stretch";
	public final static String SCALE_TYPE_STRETCHX = "stretchX";
	public final static String SCALE_TYPE_STRETCHY = "stretchY";
	public final static String SCALE_TYPE_NONE = "none";
	
	/**
	 * Align type. libgdx scale type, Specification see {@link Align}.
	 * 
	 * type: Integer.
	 * defined: 
	 * 	"align": 1,  (Align.CENTER)
	 * 	"align": 18, (Align.Right | Align.Top )
	 */
	public final static String ALIGN = "align";
	
}
