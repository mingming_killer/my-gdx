package com.gaforce.killer7.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;
import com.badlogic.gdx.scenes.scene2d.actions.ActionResetingPool;

/**
 * 
 * <p>
 * This a base value property animation action.
 * </p>
 * 
 * @author mingming
 *
 */
public class ValueTo extends AnimationAction {
	
	private static final ActionResetingPool<ValueTo> mPool = 
		new ActionResetingPool<ValueTo>(4, 100) {
		@Override
		protected ValueTo newObject() {
			return new ValueTo();
		}
	};
	
	protected int mStartValue;
	protected int mEndValue;
	protected int mValue;
	protected ValueToChange mListener;
	
	
	public static interface ValueToChange {
		public void onValueTo(int value);
	}
	
	public static ValueTo $(int start, int end, float duration) {
		return ValueTo.$(start, end, duration, null);
	}
	
	public static ValueTo $(int start, int end, float duration, ValueToChange listener) {
		ValueTo action = mPool.obtain();
		action.mStartValue = start;
		action.mEndValue = end;
		action.mValue = start;
		action.mListener = listener;
		action.duration = duration;
		action.invDuration = 1 / duration;
		return action;
	}
	
	public int getValue() {
		return mValue;
	}
	
	public void setChangeListener(ValueToChange listener) {
		mListener = listener;
	}
	
	@Override
	public void setTarget(Actor actor) {
		// in this action, the target don't use.
		this.target = actor;
		
		this.taken = 0;
		this.done = false;
	}

	@Override
	public void act(float delta) {
		float alpha = createInterpolatedAlpha(delta);
		if (done) {
			mValue = mEndValue;
			if (null != mListener) {
				mListener.onValueTo(mValue);
			}
		} else {			
			mValue = mStartValue + (int)((mEndValue - mStartValue) * alpha);
			if (null != mListener) {
				mListener.onValueTo(mValue);
			}
		}
	}
	
	@Override
	public void finish () {
		super.finish();
		mPool.free(this);
	}

	@Override
	public Action copy() {
		ValueTo valueTo = $(mStartValue, mEndValue, duration);
		if (interpolator != null) valueTo.setInterpolator(interpolator.copy());
		return valueTo;
	}

}
