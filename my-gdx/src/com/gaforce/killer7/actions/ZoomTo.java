package com.gaforce.killer7.actions;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;
import com.badlogic.gdx.scenes.scene2d.Layout;
import com.badlogic.gdx.scenes.scene2d.actions.ActionResetingPool;

/**
 * 
 * <p>
 * Zoom out or in animation action.
 * </p>
 * 
 * @author mingming
 *
 */
public class ZoomTo extends AnimationAction {
	
	private static final ActionResetingPool<ZoomTo> mPool = 
		new ActionResetingPool<ZoomTo>(4, 100) {
		@Override
		protected ZoomTo newObject() {
			return new ZoomTo();
		}
	};
	
	protected Rectangle mRect;
	protected Rectangle mStartRect;
	protected Rectangle mDeltaRect;
	
	
	public static ZoomTo $(Rectangle rect, float duration) {
		ZoomTo action = mPool.obtain();
		action.mRect = new Rectangle(rect);
		action.mStartRect = new Rectangle(rect);
		action.mDeltaRect = new Rectangle(rect);
		action.duration = duration;
		action.invDuration = 1 / duration;
		return action;
	}
	
	public static ZoomTo $(float x, float y, float w, float h, float duration) {
		ZoomTo action = mPool.obtain();
		action.mRect = new Rectangle(x, y, w, h);
		action.mStartRect = new Rectangle(x, y, w, h);
		action.mDeltaRect = new Rectangle(x, y, w, h);
		action.duration = duration;
		action.invDuration = 1 / duration;
		return action;
	}
	
	@Override
	public void setTarget(Actor actor) {
		this.target = actor;

		mStartRect.x = target.x;
		mStartRect.y = target.y;
		mStartRect.width = target.width;
		mStartRect.height = target.height;
		
		mDeltaRect.x = mRect.x - target.x;
		mDeltaRect.y = mRect.y - target.y;
		mDeltaRect.width = mRect.width - target.width;
		mDeltaRect.height = mRect.height - target.height;
		
		this.taken = 0;
		this.done = false;
	}

	@Override
	public void act(float delta) {
		float alpha = createInterpolatedAlpha(delta);
		if (done) {
			target.x = mRect.x;
			target.y = mRect.y;
			target.width = mRect.width;
			target.height = mRect.height;
		} else {
			target.x = mStartRect.x + mDeltaRect.x * alpha;
			target.y = mStartRect.y + mDeltaRect.y * alpha;
			target.width = mStartRect.width + mDeltaRect.width * alpha;
			target.height = mStartRect.height + mDeltaRect.height * alpha;
			
			// change actor size, must re-compute children size.
			if (target instanceof Layout) {
				((Layout) target).invalidate();
				//((Layout) target).layout();
			}
		}
	}
	
	@Override
	public void finish () {
		super.finish();
		mPool.free(this);
	}

	@Override
	public Action copy() {
		ZoomTo zoomTo = $(mRect, duration);
		if (interpolator != null) zoomTo.setInterpolator(interpolator.copy());
		return zoomTo;
	}

}
