package com.gaforce.killer7.statemachine;

import java.util.Iterator;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.gaforce.killer7.main.MainLoop;
import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * StateMachine ... ...
 * </p>
 * 
 * @author mingming
 * 
 */
public class StateMachine {
	
	private final static int STATEMACHINE_IDLE = 1;
	private final static int STATEMACHINE_RUNNING = 2;
	private final static int STATEMACHINE_PROCESSING = 3;
	@SuppressWarnings("unused")
	private final static int STATEMACHINE_FINISHE = 4;
	
	private State mRootState;
	private State mCurrentState;
	
	private int mStatus;
	private int mMsg = 0x0999;
    private MainLoop mLoop = null;
	private Vector<Event> mEvents;
	
	
    public StateMachine(MainLoop loop, int msg) {
    	if (null != loop) {
    		mLoop = loop;
    	} else {
    		throw new NullPointerException();
    	}

    	mMsg = msg;
        mStatus = STATEMACHINE_IDLE;
        mEvents = new Vector<Event> ();
        mRootState = new State();
    }
    
    // we forbid default construct.
    @SuppressWarnings("unused")
    private StateMachine() {
    	
    }
	
    public void setInitialState(State state) {
        //assert(STATEMACHINE_IDLE == mStatus);
    	if (STATEMACHINE_IDLE == mStatus) {
    		mRootState.setInitialState(state);
    	} else {
    		Gdx.app.log(K7Utils.TAG, "StateMachine: setInitialState: "  + 
    				"you can't set initial state when state machine is running !!");
    	}
    }
    
    public void start() {
        //assert (mStatus == STATEMACHINE_IDLE);
    	if (STATEMACHINE_IDLE == mStatus) {
    		mStatus = STATEMACHINE_RUNNING;
        	mCurrentState = mRootState.enter(null, null, true);
    	} else {
    		Gdx.app.log(K7Utils.TAG, "StateMachine: start: " + 
    				"state machine is already run !!");
    	}
    }
    
	public State getCurrentState() {
        //assert(mStatus == STATEMACHINE_RUNNING || mStatus == STATEMACHINE_PROCESSING);
		if (STATEMACHINE_RUNNING == mStatus || 
				STATEMACHINE_PROCESSING == mStatus) {
			return mCurrentState;
		} else {
			Gdx.app.log(K7Utils.TAG, "StateMachine: getCurrentState: " + 
					"you should not call getCurrentState() when state machine " + 
					"running or processing !!");
			return null;
		}
    }

    public void addState(State state) {
        mRootState.addState(state);
        state.mParentState = mRootState;
    }
    
    public void processEvent(final Event event) {
    	int i;
    	
        State oldState = null; 
        State newState = null;
        Transition trans = null;
        
        Iterator<State> iterator;
        Vector<State> current2TransHost = new Vector<State> ();
        Vector<State> target2Ancestor = new Vector<State> ();

        for (oldState = mCurrentState; /* null */; 
        	oldState = oldState.getParentState()) {
        	
            if (oldState.getDepth() <= 0) {
                return;
            }
            
            /** 
             * can't leave right now, 
             * since don't know whether there is a proper transition 
             * with a not-null target 
             */
            current2TransHost.add(oldState);
            trans = oldState.eventTest(event);
            if (null != trans) {
                break;
            }
        }

        Gdx.app.log(K7Utils.TAG, "StateMachine: processEvent: " + 
        		"find transition: " + trans.getName());
        newState = trans.getTarget();
        assert null != newState:"new state was null";

        iterator = current2TransHost.iterator();
        while (iterator.hasNext()) {
            iterator.next().leave(event, trans);
        }

        for (i = oldState.getDepth() - newState.getDepth(); i > 1; i--) {
            oldState = oldState.getParentState();
            oldState.leave(event, trans);
        }

        for (i = newState.getDepth() - oldState.getDepth(); i >= 0; i--) {
            // insert the new state to begin of container.
            target2Ancestor.insertElementAt(newState, 0);
            newState = newState.getParentState();
        }
        
        assert(oldState.getDepth() == newState.getDepth() + 1);

        while (oldState.getParentState() != newState) {
            oldState = oldState.getParentState();
            oldState.leave(event, trans);

            target2Ancestor.insertElementAt(newState, 0);
            newState = newState.getParentState();
        }

        if (0 == target2Ancestor.size()) {
            oldState.getParentState().leave(event, trans);
            target2Ancestor.insertElementAt(newState, 0);
        }

        trans.action(event);

        iterator = target2Ancestor.iterator();
        while (iterator.hasNext()) {
            State node = iterator.next();
            if (node == trans.getTarget()) {
                mCurrentState = node.enter(event, trans, true);
            } else {
                node.enter(event, trans, false);
            }
        }
        
        for (i = newState.getDepth(); i > 0; 
        	i--, newState = newState.getParentState()) {
            newState.resetTransitions();
        }
    }
    
    /**
     * Post a {@link Event} to message queue.
     * Note: the handler is not in GLThread !!
     * @param event
     */
	public void postEvent(Event event) {
        mEvents.add(event);
        // TOOD:SendNotifyMessage(m_hwnd, MSG_USER_STATEMACHINE_EVENT, -1, -1);
        mLoop.sendEmptyMessage(mMsg);
    }
	
	/**
	 * Send {@link Event} to state machine, this is a synchronous
	 * method, so don't do someting which can block the thread.
	 * And this is in GLThread !!
	 * @param event
	 */
	public void sendEvent(Event event) {
		mEvents.add(event);
		onEvent();
	}
	
    public void onEvent() {
        if (STATEMACHINE_PROCESSING == mStatus) {
            return;
        }
        
        mStatus = STATEMACHINE_PROCESSING;
        while (mEvents.size() > 0) {
            Event event = mEvents.get(0);
            mEvents.remove(0);
            processEvent(event);
            event = null;
        }
        
        mStatus = STATEMACHINE_RUNNING;
    }
}
