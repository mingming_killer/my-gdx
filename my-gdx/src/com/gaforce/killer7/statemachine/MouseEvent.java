package com.gaforce.killer7.statemachine;


public class MouseEvent extends Event {
	public enum Type {
		ButtonPressed,
		ButtonReleased
	};
	
	@SuppressWarnings("unused")
	private int m_x;
	@SuppressWarnings("unused")
	private int m_y;
	private int m_buttons;
	private Type m_type;
	
	public static final int LeftButton = 0x01;
	public static final int RightButton = 0x02;
	
	public MouseEvent(Type type, int x, int y, int buttons) {
		super(Event.MouseEvent);
		m_type = type; 
		m_x = x; 
		m_y = y;
		m_buttons = buttons;
	}
	
	/*void point(Point pt) { 
		if (null != pt) {
			pt.x = m_x; pt.y = m_y; 
		}
	}*/
	
	int buttons() { return m_buttons; }
	
	Type type() {
		return m_type;
	}
}
