package com.gaforce.killer7.statemachine;

public class SignalTransition extends Transition {
	
    private Object mSender;
    private int mSignal;
    
    
    public SignalTransition(State source, State target, String name) {
        super(source, target, name);
    }

    public SignalTransition(final Object sender, String signal,
    		State source, State target, String name) {
        super(source, target, name);
        setSignal(sender, signal);
    }
    
    public SignalTransition(final Object sender, int signal,
    		State source, State target, String name) {
        super(source, target, name);
        setSignal(sender, signal);
    }

    public void setSignal(final Object sender, String signal) {
        mSender = sender;
        mSignal = signal.hashCode();
    }
    
    public void setSignal(final Object sender, int signal) {
    	mSender = sender;
        mSignal = signal;
    }

    protected boolean eventTest(final Event event) {
        SignalEvent signalEvent;
        if (event.getEventType() != Event.SignalEvent) {
            return false;
        }

        signalEvent = (SignalEvent) event;
        return (signalEvent.getSender() == mSender && signalEvent.getSignal() == mSignal);
    }
}
