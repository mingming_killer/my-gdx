package com.gaforce.killer7.statemachine;

import java.util.Iterator;
import java.util.Vector;

public class EventTransition extends Transition {
	
    protected int mButtonType;
    protected int mButtons;
    protected int mKeyType;
    protected Vector<Integer> mKeys;
    
    private int mEventMask;

    public EventTransition(int eventMask, State source, State target, String name) {
        super(source, target, name); 
        mEventMask = eventMask;
    }
    
    public final void setButtonType(int type) {
        mButtonType = type;	
    }

    public final void setButtons(int buttons) {
        mButtons = buttons;	
    }

    public final void setKeyType(int type) {
        mKeyType = type;	
    }

    public final void setKey(int scancode) {
        mKeys.add(scancode);	
    }
    
    protected boolean eventTest(Event event) {
        assert null != event : "event was nil.";
        if (0 == (event.getEventType() & mEventMask)) {
            return false;
        }
        
        switch (event.getEventType()) {
            case Event.KeyEvent:
                return testKey((KeyEvent )event);
                
            case Event.MouseEvent:
                /*if (((MouseEvent)event).eventType() == 2) {
                  printf("%s:%d, type=%d\n", __FUNCTION__, event.type(), ((MouseEvent)event).type());
                  }*/
                return testMouse((MouseEvent)event);
                
            case Event.MoveEvent:
                return testMove((MoveEvent)event);
                
            case Event.TimerEvent:
                return testTimer((TimerEvent)event);
                
            default:
                return false;
        }
    }

    protected boolean testKey(KeyEvent event) {
        Iterator<Integer> i = mKeys.iterator();
        while (i.hasNext()) {
            if (0 != (event.type().ordinal() & mKeyType) && (event.key() == i.next())) {
                return true;
            }
        }
        return false;
    }

    protected boolean testMouse(MouseEvent event) {
        if ((event.type().ordinal() & mButtonType) != 0 && (event.buttons() & mButtons) != 0) {
            return true;
        } else {
            return false;
        }
    }

    protected boolean testMove(MoveEvent e) { 
    	return true; 
    }
    
    protected boolean testTimer(TimerEvent e) { 
    	return false; 
    }
}
