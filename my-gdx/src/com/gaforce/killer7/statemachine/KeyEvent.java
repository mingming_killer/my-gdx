package com.gaforce.killer7.statemachine;

public class KeyEvent extends Event {
	private Type m_type;
	
	public enum Type {
		KeyPressed,
		KeyReleased
	};
	
	private int m_key;
	
	public KeyEvent(Type type, int key) {
		super(Event.KeyEvent);
		m_type = type; 
		m_key = key;
	}
	
	public int key() { return m_key; }
	public Type type() {
		return m_type;
	}
}
