package com.gaforce.killer7.statemachine;

/**
 * 
 * <p>
 * SignalEvent ... ...
 * </p>
 * 
 * @author mingming
 */
public class SignalEvent extends Event {
	
	private Object mSender;
	private int mSignal;
	
	
	public SignalEvent(final Object sender, final String signal) {
		super(Event.SignalEvent);
		mSender = sender;
		mSignal = signal.hashCode(); /*Str2Key(signal)*/;
	}
	
	public SignalEvent(final Object sender, final int signal) {
		super(Event.SignalEvent);
		mSender = sender;
		mSignal = signal;
	}
	
	public Object getSender() { 
		return mSender; 
	}
	
	public int getSignal() { 
		return mSignal; 
	}
}
