package com.gaforce.killer7.statemachine;

import java.util.Map;
import java.util.Hashtable;


/**
 * 
 * <p>
 * Transition ... ...
 * </p>
 * 
 * @author mingming
 */
public class Transition {
	
	protected String mName;
	private State mSource;
	private State mTarget;
	private Map<TransitionSubscriber, Object> mActions;
	
	private static Object mParam = new Object();
	
	
	protected Transition(State source, State target) {
        this(source, target, "");
    }

	protected Transition(State source, State target, String name) {
        mName = name;
        mSource = source;
        mTarget = target;
        mActions = new Hashtable<TransitionSubscriber, Object> ();
        
        if (null != mSource) {
            mSource.addTransition(this);
        } else {
            throw new NullPointerException();
        }
	}
	
	public State getSource() { 
		return mSource; 
	}
	
	public State getTarget() { 
		return mTarget; 
	}
	
	public String getName() { 
		return mName; 
	}
	
	public void subscribe(TransitionSubscriber subscriber, Object param) {
	    if (null != subscriber && (!mActions.containsKey(subscriber))) {
	    	// in order to avoid NullPointerException.
	    	if (null == param) {
	    		param = mParam;
	    	}
            mActions.put(subscriber, param);
        }
	}
	
	public void reset() {
		
	}
	
	protected boolean eventTest(final Event event) { 
		return true; 
	}
	
	/* TODO: no use, so comment it
	    virtual void onTransition();
	 */

    /* package */ void action(Event event) {
        for (Map.Entry<TransitionSubscriber, Object> entry : mActions.entrySet()) {
            entry.getKey().onTrigger(this, event, entry.getValue());
        }
    }
}
