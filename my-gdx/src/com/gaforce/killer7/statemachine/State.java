package com.gaforce.killer7.statemachine;

import java.util.Iterator;
import java.util.Vector;

import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * Unit of {@link StateMachine}.
 * </p>
 * 
 * @author mingming
 */
public class State {
	
    private String mName;
    private Vector<State> mChildStates = new Vector<State>();
    private Vector<Transition> mTransitions = new Vector<Transition>();
    
    /* package */ State mParentState;
    private State mInitalState;
    private StateMachine mStateMachine;
    
    private int mDepth;
    private boolean mEntered;
    
    
    public State() {
        mName = "root";
    }
    
    public State(final String name) {
    	mName = name;
    }
    
    public State(StateMachine stateMachine) {
        this(stateMachine, "");
    }
    
    public State(State parentState) {
        this(parentState, "");
    }
    
    public State(StateMachine stateMachine, final String name) {
        mName = name;
        mStateMachine = stateMachine;
        mDepth = 1;
        
        if (null != mStateMachine) {
            mStateMachine.addState(this);
        } else {
            throw new NullPointerException();
        }
    }

    public State(State parentState, final String name) {
        mName = name;
        mParentState = parentState;
        
        if (null != mParentState) {
            mDepth = mParentState.getDepth() + 1;
            mParentState.addState(this);
        } else {
            throw new NullPointerException();
        }
    }

    public final String getName() { 
    	return mName; 
    }
    
    public final State getParentState() { 
    	return mParentState; 
    }

    public final StateMachine getStateMachine() {
        if (null != mStateMachine) {
            return mStateMachine;
        } else {
            return getParentState().getStateMachine();
        }
    }
    
    public final int getDepth() { 
    	return mDepth; 
    }

    public final void setInitialState(State state) {
        Iterator<State> i = mChildStates.iterator();
        while (i.hasNext()) {
            if (i.next() == state) {
                mInitalState = state;
                return;
            }
        }
    }

    public final void addTransition(Transition transition) {
        if (null != transition && (!mTransitions.contains(transition))) {
            mTransitions.add(transition);
        }
    }
    
    protected void onEnter(Event event, Transition transition) {
    	
    }

    protected void onLeave(Event event, Transition transition) {
    	
    }
    
    /* package */ void addState(State childState) {
        if (null != childState && (!mChildStates.contains(childState))) {
            mChildStates.add(childState);
        }
    }
    
    /* package */ Transition eventTest(final Event event) {
        Iterator<Transition> i = mTransitions.iterator();
        Transition trans;
        
        while (i.hasNext()) {
            trans = i.next();
            if (trans.eventTest(event)) {
                if (null != trans.getTarget()) {
                    return trans;
                } else {
                    /** Don't return if it is a targetless transition, 
                     * and do not reset other transitions */
                    trans.action(event);
                }
            }
        }
        return null;
    }
    
    /* package */ State enter(final Event event, 
    		final Transition transition, boolean autojump) {
        if (mEntered) {
        	String tranName;
        	if (null != transition) {
        		tranName = transition.getName();
        	} else {
        		tranName = "null";
        	}
           K7Utils.log("K7.State: enter: " +  
                    "renter state: " + getName() + "transition = " + tranName);
        }
        
        // don't case reenter.
        //assert !mEntered : "State.enter() was reenter.";
        mEntered = true;
        
        onEnter(event, transition);
        
        resetTransitions();

        if (autojump) {
            if (null != mInitalState) {
                return mInitalState.enter(event, transition, true);
            } else {
                assert(0 == mChildStates.size());
                return this;
            }
        } else {
            return this;
        }
    }
    
    /* package */ void leave(final Event event, final Transition transition) {
        if (! mEntered) {
            K7Utils.log("K7.State: leave: " +  
                    "renter state" + getName() + "transition=" + transition.getName());
        }
        
        assert(mEntered);
        mEntered = false;

        onLeave(event, transition);
    }

    /* package */ void resetTransitions() {
        Iterator<Transition> i = mTransitions.iterator();

        while (i.hasNext()) {
            i.next().reset();
        }
    }
    
}
