package com.gaforce.killer7.statemachine;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;


/**
 * 
 * <p>
 * Event ... ...
 * </p>
 * 
 * @author mingming
 */
public class Event {
	
	public final static int KeyEvent = 0x01;
	public final static int MouseEvent = 0x02;
	public final static int MoveEvent = 0x03;
	public final static int TimerEvent = 0x04;
	public final static int SignalEvent = 0x05;
	
	private int mType;
	private Vector<Object> mParams;
	//typedef vector<any> EventParameters;
	
	
	protected Event(int type) {
		mType = type;
	}
	
	public final int getEventType() { 
		return mType; 
	}
	
	public void appendParameter(Object param) {
		mParams.add(param);
	}
	
	public final Enumeration<Object> getParameters() {
		return mParams.elements();
	}
	
	public final Iterator<Object> getIterator() {
		return mParams.iterator();
	}
	
}
