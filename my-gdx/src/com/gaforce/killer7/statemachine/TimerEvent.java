package com.gaforce.killer7.statemachine;

public class TimerEvent extends Event {
	private int m_id;
	private int m_tickCount;
	
	public TimerEvent(int id, int tickCount) {
		super(Event.TimerEvent);
		m_id = (id);
		m_tickCount = (tickCount);
	}
	
	public int id() { return m_id; }
	public int tickCount() { return m_tickCount; }
}
