package com.gaforce.killer7.statemachine;

public interface TransitionSubscriber {
	public void onTrigger(Transition transition, final Event event, Object param);
}
