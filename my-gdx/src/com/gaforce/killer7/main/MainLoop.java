package com.gaforce.killer7.main;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationListener;
import com.gaforce.killer7.message.Handler;
import com.gaforce.killer7.message.Message;
import com.gaforce.killer7.message.Handler.UIThread;
import com.gaforce.killer7.message.HandlerFactory;

/**
 * 
 * <p>
 * GLThread main loop
 * </p>
 * 
 * @author mingming
 *
 */
public class MainLoop implements ApplicationListener, 
	UIThread, Handler.Callback {
	
	protected Handler mMainHandler = null;
	protected ArrayList<Handler.Callback> mCallbacks = null;
	protected ArrayList<Handler> mHandlers = null;
	
	
	public MainLoop() {
		mHandlers = new ArrayList<Handler> ();
		mCallbacks = new ArrayList<Handler.Callback> ();
    }
	
	@Override
	public void create() {
		// TODO Auto-generated method stub
		HandlerFactory.initialize(null);
		
		mMainHandler = HandlerFactory.getHandler(this, this);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void render() {
		// process the UI thread message.
		for (Handler handler : mHandlers) {
			synchronized(handler) {
				handler.dispatchUIMessage();
			}
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		if (null != mCallbacks) {
			mCallbacks.clear();
		}
		
		if (null != mHandlers) {
			mHandlers.clear();
		}
		
		mMainHandler = null;
		
		HandlerFactory.destroy();
	}
	
	@Override
	public void registerUIHandler(Handler handler) {
		if (null == handler) {
			return;
		}
		
		if (!mHandlers.contains(handler)) {
			mHandlers.add(handler);
		}
	}
	
	@Override
	public void unregisterUIHandler(Handler handler) {
		if (null == handler) {
			return;
		}
		
		mHandlers.remove(handler);
	}

	@Override
	public boolean handleMessage(Message msg) {
		// deliver message to register handler callback.
        if (deliverMessage(msg)) {
        	return true;
        }
        
        // default message handler callback.
        return defaultMessageHandler(msg);
	}
	
	public void registerHandlerCallback(Handler.Callback callback) {
		if (null == callback) {
			return;
		}
		
		if (!mCallbacks.contains(callback)) {
			mCallbacks.add(callback);
		}
	}
	
	public void unregisterHandlerCallback(Handler.Callback callback) {
		if (null == callback) {
			return;
		}
		
		mCallbacks.remove(callback);
	}
	
	/**
	 * Just a wrap of {@link Handler#sendMessage(Message)}.
	 * 
	 * @param msg see {@link Handler#sendMessage(Message)}.
	 * @return see {@link Handler#sendMessage(Message)}.
	 */
	public boolean sendMessage(Message msg) {
		checkMainHandler();
		return mMainHandler.sendMessage(msg);
	}
	
	/**
	 * Just a wrap of {@link Handler#sendEmptyMessage(int)}}.
	 * 
	 * @param what see {@link Handler#sendEmptyMessage(int)}.
	 * @return see {@link Handler#sendEmptyMessage(int)}.
	 */
	public boolean sendEmptyMessage(int what) {
		checkMainHandler();
		return mMainHandler.sendEmptyMessage(what);
	}
	
	protected boolean defaultMessageHandler(Message msg) {
		return false;
	}
	
	private boolean deliverMessage(Message msg) {
		if (null == mCallbacks || null == msg) {
			return false;
		}
		
		boolean ret = false;
		for (Handler.Callback callback : mCallbacks) {
			ret = callback.handleMessage(msg);
			if (true == ret) {
				return true;
			}
		}
		
		return false;
	}
	
	private void checkMainHandler() {
		if (null == mMainHandler) {
			mMainHandler = HandlerFactory.getHandler(this, this);
		}
	}
}
