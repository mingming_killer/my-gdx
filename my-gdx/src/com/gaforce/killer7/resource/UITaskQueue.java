package com.gaforce.killer7.resource;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * 
 * <p>
 * UI task queue ... ...
 * </p>
 * 
 * @author mingming
 *
 */
public class UITaskQueue {
	
	// we use ArrayList to implement simple FIFO Queue.
	// java's Queue is too trouble. -_-||
	protected ArrayList<UITaskUnit> mQueue;
	
	
	public interface UITask {
		/**
		 * 
		 * @param params
		 * @return true means the task already complete, 
		 *     false means not complete yet.
		 */
		public boolean doUITask(Object params);
	}
	
	public interface UITaskListener {
		public void uiTaskComplete(UITask task);
		
		public void waitUITaskScreen(SpriteBatch batch);
	}
	
	public static class UITaskUnit {
		public UITask mTask;
		public Object mParams;
		public UITaskListener mListener; 
		
		public UITaskUnit() {
			mTask = null;
			mParams = null;
		}
		
		public UITaskUnit(UITask task, Object params, UITaskListener listener) {
			mTask = task;
			mParams = params;
			mListener = listener;
		}
	}
	
	
	public UITaskQueue() {
		mQueue = new ArrayList<UITaskUnit> ();
	}
	
	public void destory() {
		if (null != mQueue) {
			mQueue.clear();
			mQueue = null;
		}
	}
	
	public boolean commitTask(UITaskUnit unit) {
		if (null == mQueue || null == unit) {
			return false;
		}
		
		// if queue already have task, delete it first.
		if (mQueue.contains(unit)) {
			mQueue.remove(unit);
		}
		
		// add it to the queue trail.
		mQueue.add(unit);
		
		return true;
	}
	
	public boolean cancelTask(UITaskUnit unit) {
		if (null == mQueue || null == unit) {
			return false;
		}
		
		return mQueue.remove(unit);
	}
	
	public UITaskUnit peek() {
		if (null == mQueue || mQueue.size() <= 0) {
			return null;
		}
		
		// get head element of the queue.
		return mQueue.get(0);
	}
	
	public UITaskUnit poll() {
		UITaskUnit unit = peek();
		
		if (null != unit) {
			mQueue.remove(unit);
			return unit;
		} else {
			return null;
		}
	}
	
	public boolean checkTask(SpriteBatch batch) {
		UITaskUnit unit = peek();
		
		if (null == unit) {
			// there don't have any task.
			return true;
		} else {
			if (null != unit.mTask) {
				// show waiting screen.
				if (null != unit.mListener) {
					unit.mListener.waitUITaskScreen(batch);
				}
				
				// do task.
				if (unit.mTask.doUITask(unit.mParams)) {
					if (null != unit.mListener) {
						unit.mListener.uiTaskComplete(unit.mTask);
					}
				
					poll();
				}
			} else {
				poll();
			}
		}
		
		return false;
	}
	
}
