package com.gaforce.killer7.resource;

/**
 * 
 * <p>
 * When in {@link ApplicationListener#create()}, Object who need outside resources 
 * should load it. And in {@link ApplicationListener#dispose()} it should destroy 
 * it loaded resources, there is no more time to need it.
 * When in {@link ApplicationListener#pause()} , Object who hold resources 
 * like {@link Texture} should release it to save mobile device memory cost. 
 * And in {@link ApplicationListener#resume()} it should reload it's need resources.
 * 
 * </p>
 * 
 * @author mingming
 */
public interface Cyclable {
	
	/**
	 * Call it When in {@link ApplicationListener#create()}
	 */
	public void create();
	
	/**
	 * Call it When in {@link ApplicationListener#pause()}
	 */
	public void pause();
	
	/** 
	 * Call it When in {@link ApplicationListener#resume()}
	 */
	public void resume();
	
	/**
	 * Call it When in {@link ApplicationListener#dispose()}
	 */
	public void destroy();
	
}
