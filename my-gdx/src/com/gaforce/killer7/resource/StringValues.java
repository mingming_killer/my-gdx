package com.gaforce.killer7.resource;

import java.util.HashMap;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.OrderedMap;
import com.gaforce.killer7.utils.K7Utils;

/**
 * 
 * <p>
 * Hold string values, implement mulit-language support.
 * </p>
 * 
 * @author mingming
 *
 */
public class StringValues implements Serializable {
	
	public final static String TAG = "K7.StringValues: ";
	
	public final static String LANG = "str_lang";
	public final static String LANG_UNKNOW = "unknow";

	public final static String STR_MISS = "miss";
	
	private final static String REF_EXPR = "@string/(.)+";
	private final static int REF_KEY_START = 8;  // length of "@string/" is 8. 
	
	private HashMap<String, String> mStringMap;
	
	
	public StringValues() {
		init();
	}
	
	private void init() {
		mStringMap = new HashMap<String, String> ();
	}
	
	public void destroy() {
		if (null != mStringMap) {
			mStringMap.clear();
			mStringMap = null;
		}
	}
	
	public boolean loadString() {
		return false;
	}
	
	public boolean unloadString() {
		return false;
	}
	
	public String getLang() {
		if (null == mStringMap) {
			return LANG_UNKNOW;
		}
		
		String lang = mStringMap.get(LANG);
		if (null == lang) {
			return LANG_UNKNOW;
		} else {
			return lang;
		}
	}
	
	public String getString(String key) {
		return getString(key, STR_MISS);
	}
	
	public String getString(String key, String def) {
		if (null == mStringMap) {
			return def;
		}
		
		String str = mStringMap.get(key);
		if (null == str) {
			return def;
		} else {
			return str;
		}
	}
	
	public String getRecferenceString(String refKey) {
		return getReferenceString(refKey, STR_MISS);
	}
	
	public String getReferenceString(String refKey, String def) {
		if (null == mStringMap) {
			return def;
		}
		
		if (!refKey.matches(REF_EXPR)) {
			return def;
		}
		
		String key = refKey.substring(REF_KEY_START);
		return getString(key, def);
	}

	@Override
	public void write(Json json) {
		// we don't support write to this serializable object.
		return;
	}

	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData) {
		if (jsonData == null) {
			return;
		}
		
		String str = null;
		
		try {
			// get all string values.
			for (String key : jsonData.keys()) {
				if (null == key || key.equals("")) {
					continue;
				}
				
				str = (String) jsonData.get(key);
				mStringMap.put(key, str);
			}
			
		} catch (Exception e) {
			K7Utils.log(TAG + "read: ", e);
		}
	}
	
}
