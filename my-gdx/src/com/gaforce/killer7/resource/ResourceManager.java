package com.gaforce.killer7.resource;

import java.util.ArrayList;

import com.gaforce.killer7.resource.Cyclable;

/**
 * 
 * <p>
 * Manage {@link Cyclable} object who {@link ResourceManager#registerCyclable(Cyclable)}
 * to it. When {@link ApplicationListener#create()}, {@link ApplicationListener#pause()},
 * {@link ApplicationListener#resume()}, {@link ApplicationListener#dispose()}, 
 * it will call all register object {@link Cyclable#load()}, {@link Cyclable#release()}, 
 * {@link Cyclable#reload()}, {@link Cyclable#destroy()}.
 * </p>
 * 
 * @author mingming
 */
public class ResourceManager implements Cyclable {
	
	private static ResourceManager mInstance = null;
	private static ArrayList<Cyclable> mResourceCyclableList = null;
	
	
	/**
	 * Get singleton object instance.
	 * @return singleton instance.
	 */
	public static ResourceManager getInstance() {
		if (null == mInstance) {
			mInstance = new ResourceManager();
		}
		
		return mInstance;
	}
	
	// we forbid create instance from outside.
	private ResourceManager() {
		mResourceCyclableList = new ArrayList<Cyclable> ();
	}
	
	/**
	 * Register cyclable object to ResourceManager. 
	 * When methods of ApplicationListener called, 
	 * it will call cyclable corresponding method.
	 * @param cyclable register cyclable object.
	 */
	public void registerCyclable(Cyclable cyclable) {
		if (null == cyclable) {
			return;
		}
		
		if (null == mResourceCyclableList) {
			mResourceCyclableList = new ArrayList<Cyclable> ();
		}
		
		if (!mResourceCyclableList.contains(cyclable)) {
			mResourceCyclableList.add(cyclable);
		}
	}
	
	/**
	 * Delete registered cyclable object in ResourceManager.
	 * @param cyclable unregister cyclable object.
	 */
	public void unregisterCyclable(Cyclable cyclable) {
		if (null == mResourceCyclableList || null == cyclable) {
			return;
		}
		
		mResourceCyclableList.remove(cyclable);
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		if (null == mResourceCyclableList) {
			return;
		}
		
		for (Cyclable cyclable : mResourceCyclableList) {
			cyclable.create();
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		if (null == mResourceCyclableList) {
			return;
		}
		
		for (Cyclable cyclable : mResourceCyclableList) {
			cyclable.pause();
		}
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		if (null == mResourceCyclableList) {
			return;
		}
		
		for (Cyclable cyclable : mResourceCyclableList) {
			cyclable.resume();
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		if (null != mResourceCyclableList) {
			for (Cyclable cyclable : mResourceCyclableList) {
				cyclable.destroy();
			}
		}
		
		mResourceCyclableList.clear();
		mResourceCyclableList = null;
		
		mInstance = null;
	}

}
